unit Unit_Faktoren;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, Unit_TGameBoard;

type
  TFaktoren = record
    faktor_rechengenauigkeit: integer;
    faktor_zukunftsschritte: integer;
    faktor_basis: integer;
    faktor_exponentmulti: integer;

    faktor_rateOWNmovability: single;
    faktor_rateOWNmovability_unbeweglich: integer;

    faktor_ratemovability: single;
    faktor_ratemovability_unbeweglich: integer;

    faktor_fisch: integer;


    faktor_fischverhaeltnis_exponent: single;
    faktor_fischverhaeltnis_multiplikator: integer;

    faktor_schlechtesfeld_1: integer;
    faktor_schlechtesfeld_1_gefahr: integer;
    faktor_schlechtesfeld_2: integer;
  end;

  TForm_Faktoren = class(TForm)
    Label1: TLabel;
    SE_rechengenauigkeit: TSpinEdit;
    Label2: TLabel;
    SE_zukunftsschritte: TSpinEdit;
    Label3: TLabel;
    SE_basis: TSpinEdit;
    Label4: TLabel;
    SE_exponentmulti: TSpinEdit;
    Label5: TLabel;
    Label6: TLabel;
    SE_rateOWNmovability_unbeweglich: TSpinEdit;
    Ed_rateOWNmovability: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    SE_ratemovability_unbeweglich: TSpinEdit;
    Ed_ratemovability: TEdit;
    Label9: TLabel;
    SE_fisch: TSpinEdit;
    Label10: TLabel;
    Label11: TLabel;
    SE_fischverhaeltnis_multiplikator: TSpinEdit;
    Ed_fischverhaeltnis_exponent: TEdit;
    Label12: TLabel;
    SE_schlechtesfeld_1: TSpinEdit;
    Label13: TLabel;
    SE_schlechtesfeld_1_gefahr: TSpinEdit;
    Label14: TLabel;
    SE_schlechtesfeld_2: TSpinEdit;
    Btn_Load: TButton;
    Btn_Cancel: TButton;
    Btn_OK: TButton;
    Btn_Send: TButton;
    Btn_Repeat: TButton;
    RG_Client: TRadioGroup;
    Timer_Load: TTimer;
    Btn_RawOutput: TButton;
    procedure Btn_RawOutputClick(Sender: TObject);
    procedure Timer_LoadTimer(Sender: TObject);
    procedure RG_ClientClick(Sender: TObject);
    procedure EditChange(Sender: TObject);
    procedure Btn_RepeatClick(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure Btn_SendClick(Sender: TObject);
    procedure Btn_LoadClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    procedure SetValues; //schreibt in globae Variable Faktoren
    procedure GetValues; //liest aus globaler Variable Faktoren
  end;

  procedure LoadFaktoren(Client: TBit); //L�dt in globale Variable Faktoren
  procedure SendFaktoren(Client: TBit);
  procedure FaktConvertRawToVal; //FaktRawStr  ==>  Faktoren

var
  Form_Faktoren: TForm_Faktoren;
  Faktoren: TFaktoren;
  FaktRawStr: array[0..13] of string;
  FaktCounter: -1..13 = -1; //Wenn > -1, dann wird die Clientausgabe in FaktRawStr[FaktCounter] eingelesen

implementation

{$R *.dfm}

uses Unit_Server_Main, DebugForm;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_Faktoren.FormShow(Sender: TObject);
begin
  RG_Client.Items.Clear;
  if Board.Player[0].CPUControlled = true then
    RG_Client.Items.Add(Board.Player[0].Name);
  if Board.Player[1].CPUControlled = true then
    RG_Client.Items.Add(Board.Player[1].Name);
  if RG_Client.Items.Count = 0 then
    ShowMessage('Keinen aktiven Client gefunden!')
  else begin
    RG_Client.ItemIndex:=0;
    Btn_LoadClick(self);
  end;
end;

//------------------------------------------------------------------------------

procedure LoadFaktoren(Client: TBit); //L�dt in globale Variable Faktoren
begin
  FaktCounter := 0; //Jetzt wird Input erwartet
  //Sendeanfrage schicken
  Board.Player[Client].SendToClient('facraw');
  Form_Faktoren.Timer_Load.Tag := -1;
  Form_Faktoren.Timer_LoadTimer(Form_Faktoren);
  Form_Faktoren.Timer_Load.Enabled := true;
end;

//------------------------------------------------------------------------------

procedure FaktConvertRawToVal;
var i: Integer;
begin
  for i:=0 to 13 do
  begin
    if Copy(FaktRawStr[i],1,6) = 'debug ' then
      Delete(FaktRawStr[i],1,6);
  end;

  Faktoren.faktor_rechengenauigkeit := StrToIntDef(FaktRawStr[0], 0);
  Faktoren.faktor_zukunftsschritte := StrToIntDef(FaktRawStr[1], 0);
  Faktoren.faktor_basis := StrToIntDef(FaktRawStr[2], 0);
  Faktoren.faktor_exponentmulti := StrToIntDef(FaktRawStr[3], 0);

  Faktoren.faktor_rateOWNmovability := StrToFloatDef(FaktRawStr[4], 0);
  Faktoren.faktor_rateOWNmovability_unbeweglich := StrToIntDef(FaktRawStr[5], 0);

  Faktoren.faktor_ratemovability := StrToFloatDef(FaktRawStr[6], 0);
  Faktoren.faktor_ratemovability_unbeweglich := StrToIntDef(FaktRawStr[7], 0);

  Faktoren.faktor_fisch := StrToIntDef(FaktRawStr[8], 0);


  Faktoren.faktor_fischverhaeltnis_exponent := StrToFloatDef(FaktRawStr[9], 0);
  Faktoren.faktor_fischverhaeltnis_multiplikator := StrToIntDef(FaktRawStr[10], 0);

  Faktoren.faktor_schlechtesfeld_1 := StrToIntDef(FaktRawStr[11], 0);
  Faktoren.faktor_schlechtesfeld_1_gefahr := StrToIntDef(FaktRawStr[12], 0);
  Faktoren.faktor_schlechtesfeld_2 := StrToIntDef(FaktRawStr[13], 0);
end;

//------------------------------------------------------------------------------

procedure SendeFaktor(Client: TBit; S: String);
const KeyStr = 'fac ';
begin
  Board.Player[Client].SendToClient(KeyStr + S);
end;
//------------------------------------------------------------------------------
procedure SendFaktoren(Client: TBit);
begin
  //Aus Variable Faktoren senden
  SendeFaktor(Client, 'faktor_rechengenauigkeit = ' + IntToStr(Faktoren.faktor_rechengenauigkeit));
  SendeFaktor(Client, 'faktor_zukunftsschritte = ' + IntToStr(Faktoren.faktor_zukunftsschritte));
  SendeFaktor(Client, 'faktor_basis = ' + IntToStr(Faktoren.faktor_basis));
  SendeFaktor(Client, 'faktor_exponentmulti = ' + IntToStr(Faktoren.faktor_exponentmulti));

  SendeFaktor(Client, 'faktor_rateOWNmovability_unbeweglich = ' + IntToStr(Faktoren.faktor_rateOWNmovability_unbeweglich));
  SendeFaktor(Client, 'faktor_rateOWNmovability = ' + FloatToStr(Faktoren.faktor_rateOWNmovability));

  SendeFaktor(Client, 'faktor_ratemovability_unbeweglich = ' + IntToStr(Faktoren.faktor_ratemovability_unbeweglich));
  SendeFaktor(Client, 'faktor_ratemovability = ' + FloatToStr(Faktoren.faktor_ratemovability));

  SendeFaktor(Client, 'faktor_fisch = ' + IntToStr(Faktoren.faktor_fisch));


  SendeFaktor(Client, 'faktor_fischverhaeltnis_multiplikator = ' + IntToStr(Faktoren.faktor_fischverhaeltnis_multiplikator));
  SendeFaktor(Client, 'faktor_fischverhaeltnis_exponent = ' + FloatToStr(Faktoren.faktor_fischverhaeltnis_exponent));

  SendeFaktor(Client, 'faktor_schlechtesfeld_1 = ' + IntToStr(Faktoren.faktor_schlechtesfeld_1));
  SendeFaktor(Client, 'faktor_schlechtesfeld_1_gefahr = ' + IntToStr(Faktoren.faktor_schlechtesfeld_1_gefahr));
  SendeFaktor(Client, 'faktor_schlechtesfeld_2 = ' + IntToStr(Faktoren.faktor_schlechtesfeld_2));
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.SetValues; //schreibt in globale Variable Faktoren
begin
Faktoren.faktor_rechengenauigkeit := SE_rechengenauigkeit.Value;
Faktoren.faktor_zukunftsschritte := SE_zukunftsschritte.Value ;
Faktoren.faktor_basis := SE_basis.Value ;
Faktoren.faktor_exponentmulti := SE_exponentmulti.Value ;

Faktoren.faktor_rateOWNmovability_unbeweglich := SE_rateOWNmovability_unbeweglich.Value ;
Faktoren.faktor_rateOWNmovability := StrToFloat(Ed_rateOWNmovability.Text);

Faktoren.faktor_ratemovability_unbeweglich := SE_ratemovability_unbeweglich.Value ;
Faktoren.faktor_ratemovability := StrToFloat(Ed_ratemovability.Text);

Faktoren.faktor_fisch := SE_fisch.Value ;


Faktoren.faktor_fischverhaeltnis_multiplikator := SE_fischverhaeltnis_multiplikator.Value;
Faktoren.faktor_fischverhaeltnis_exponent := StrToFloat(Ed_fischverhaeltnis_exponent.Text);

Faktoren.faktor_schlechtesfeld_1 := SE_schlechtesfeld_1.Value;
Faktoren.faktor_schlechtesfeld_1_gefahr := SE_schlechtesfeld_1_gefahr.Value;
Faktoren.faktor_schlechtesfeld_2 := SE_schlechtesfeld_2.Value;
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.GetValues;
begin
  //Aus Faktoren ins Formular
  SE_rechengenauigkeit.Value := Faktoren.faktor_rechengenauigkeit;
  SE_zukunftsschritte.Value := Faktoren.faktor_zukunftsschritte;
  SE_basis.Value := Faktoren.faktor_basis;
  SE_exponentmulti.Value := Faktoren.faktor_exponentmulti;

  SE_rateOWNmovability_unbeweglich.Value := Faktoren.faktor_rateOWNmovability_unbeweglich;
  Ed_rateOWNmovability.Text := FloatToStr(Faktoren.faktor_rateOWNmovability);

  SE_ratemovability_unbeweglich.Value := Faktoren.faktor_ratemovability_unbeweglich;
  Ed_ratemovability.Text := FloatToStr(Faktoren.faktor_ratemovability);

  SE_fisch.Value := Faktoren.faktor_fisch;


  SE_fischverhaeltnis_multiplikator.Value := Faktoren.faktor_fischverhaeltnis_multiplikator;
  Ed_fischverhaeltnis_exponent.Text := FloatToStr(Faktoren.faktor_fischverhaeltnis_exponent);

  SE_schlechtesfeld_1.Value := Faktoren.faktor_schlechtesfeld_1;
  SE_schlechtesfeld_1_gefahr.Value := Faktoren.faktor_schlechtesfeld_1_gefahr;
  SE_schlechtesfeld_2.Value := Faktoren.faktor_schlechtesfeld_2;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



procedure TForm_Faktoren.Btn_LoadClick(Sender: TObject);
begin
  if RG_Client.ItemIndex > -1 then
    LoadFaktoren(RG_Client.ItemIndex);
  GetValues;
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.Btn_SendClick(Sender: TObject);
begin
  if RG_Client.ItemIndex > -1 then
  begin
    SetValues;
    SendFaktoren(RG_Client.ItemIndex);
  end;
  if Sender = Btn_OK then
    close;
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.Btn_CancelClick(Sender: TObject);
begin
  close;
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.Btn_RepeatClick(Sender: TObject);
begin
  if Form_Server.MM_Undo.Enabled = true then
    Form_Server.MM_UndoClick(self);
  if Form_Server.MM_Undo.Enabled = true then
    if Board.CurrentPlayer <> RG_Client.ItemIndex then
      Form_Server.MM_UndoClick(self);
  SendFaktoren(RG_Client.ItemIndex);
  Board.Player[RG_Client.ItemIndex].SendToClient(1);
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.EditChange(Sender: TObject);
var i: Single;
begin
  if TryStrToFloat(Ed_rateOWNmovability.Text, i) = true then
    Ed_rateOWNmovability.Color := clWindow
  else
    Ed_rateOWNmovability.Color := clRed;

  if TryStrToFloat(Ed_ratemovability.Text, i) = true then
    Ed_ratemovability.Color := clWindow
  else
    Ed_ratemovability.Color := clRed;

  if TryStrToFloat(Ed_fischverhaeltnis_exponent.Text, i) = true then
    Ed_fischverhaeltnis_exponent.Color := clWindow
  else
    Ed_fischverhaeltnis_exponent.Color := clRed;
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.RG_ClientClick(Sender: TObject);
begin
  Btn_LoadClick(self);
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.Timer_LoadTimer(Sender: TObject);
const max = 10; //Intervalle
begin
  Timer_Load.Tag := Timer_Load.Tag + 1;

  if (Timer_Load.Tag = max)then
  begin
    FaktCounter := -1;
    FaktConvertRawToVal; //Rohdaten in Werte in Faktoren umwandeln
    Timer_Load.Enabled := false;
    Timer_Load.Tag := 0;
  end;
end;

//------------------------------------------------------------------------------

procedure TForm_Faktoren.Btn_RawOutputClick(Sender: TObject);
var i: integer;
begin
  for i:=0 to 13 do
  begin
    SendDebug('FaktRawStr[' + IntToStr(i) + ']: ' + FaktRawStr[i]);
  end;
end;

end.
