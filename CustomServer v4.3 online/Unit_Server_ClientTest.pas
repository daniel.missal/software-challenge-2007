unit Unit_Server_ClientTest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, Unit_TGameBoard_Client;

type
  TFieldInt = array[0..10,0..7] of Integer;

  TForm_ClientTest = class(TForm)
    GB_Fields: TGroupBox;
    Btn_GetNextFieldInDirection: TButton;
    GB_Parameters: TGroupBox;
    La_Direction_Caption: TLabel;
    La_Position: TLabel;
    La_Position_X1: TLabel;
    La_Position_Y1: TLabel;
    SE_Direction: TSpinEdit;
    CB_StringGrid: TCheckBox;
    SE_Position_X1: TSpinEdit;
    SE_Position_Y1: TSpinEdit;
    La_Direction: TLabel;
    GB_Output: TGroupBox;
    Ed_Output: TEdit;
    Btn_StringGridSetBack: TButton;
    Pa_clMark: TPanel;
    ColorDialog_clMark: TColorDialog;
    La_clMark: TLabel;
    Btn_GetAllTargets: TButton;
    Btn_GetTargetsInDirection: TButton;
    Btn_GetAllNeighborFields: TButton;
    Btn_GetFieldsInRadius: TButton;
    SE_IntPar: TSpinEdit;
    La_IntPar: TLabel;
    Btn_GetAllFish: TButton;
    Btn_GetAllReachableFieldsOfFish: TButton;
    Btn_GetSumOfReachableFish: TButton;
    RG_Methods: TRadioGroup;
    GB_Fish: TGroupBox;
    GB_Move: TGroupBox;
    SE_Position_Y2: TSpinEdit;
    SE_Position_X2: TSpinEdit;
    La_Position_Y2: TLabel;
    La_Position_X2: TLabel;
    Btn_CheckPath: TButton;
    Btn_GetPath: TButton;
    Btn_GetBestFish: TButton;
    Btn_GetBestFishRandomField: TButton;
    Btn_GetFishInLine: TButton;
    GB_quality: TGroupBox;
    Btn_RateFieldsAlle: TButton;
    Btn_RateEnemysMovability: TButton;
    Btn_GetAllFishAlle: TButton;
    Btn_RateEnemysMovabilityAlle: TButton;
    Btn_RateField: TButton;
    Btn_ClientRefresh: TButton;
    procedure Btn_ClientRefreshClick(Sender: TObject);
    procedure Btn_RateFieldClick(Sender: TObject);
    procedure Btn_RateEnemysMovabilityAlleClick(Sender: TObject);
    procedure Btn_GetAllFishAlleClick(Sender: TObject);
    procedure Btn_RateEnemysMovabilityClick(Sender: TObject);
    procedure Btn_RateFieldsAlleClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_GetFishInLineClick(Sender: TObject);
    procedure Btn_GetBestFishRandomFieldClick(Sender: TObject);
    procedure Btn_GetBestFishClick(Sender: TObject);
    procedure Btn_GetPathClick(Sender: TObject);
    procedure Btn_CheckPathClick(Sender: TObject);
    procedure Btn_GetSumOfReachableFishClick(Sender: TObject);
    procedure SE_IntParChange(Sender: TObject);
    procedure Btn_GetAllReachableFieldsOfFishClick(Sender: TObject);
    procedure Btn_GetAllFishClick(Sender: TObject);
    procedure Btn_GetFieldsInRadiusClick(Sender: TObject);
    procedure Btn_GetAllNeighborFieldsClick(Sender: TObject);
    procedure Btn_GetTargetsInDirectionClick(Sender: TObject);
    procedure Btn_GetAllTargetsClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Btn_StringGridSetBackClick(Sender: TObject);
    procedure Btn_GetNextFieldInDirectionClick(Sender: TObject);
    procedure SE_DirectionChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PositionChange(Sender:TObject);
    procedure PanelColor(Sender: TObject);
    procedure OrderElements (Sender: TObject);  //Passt Formgr��e an und ordnet GroupBoxes
  private
    { Private-Deklarationen }
    //Mit Overload kann man Methoden gleichen Namens verwenden, wenn die Parameter
    //(und R�ckgabetypen) unterschiedlich sind. Beim Aufruf der Methode (hier Output) wird
    //von Delphi automatisch �berpr�ft, welche Art von Paramter angegeben wird, und ordnet
    //den Aufruf dann der entsprechenden Methode zu.
    //Eine Methode ist entweder eine Funktion oder Prozedur in einer Klasse
    procedure Output(Punkte: Unit_TGameBoard_Client.TPointArray; MarkPosition: Boolean; Sender: TObject); overload;
    procedure Output(Punkt: TPoint; const MarkPosition: Boolean; const Sender: TObject); overload;
    procedure Output(OutputInt: Integer; const Sender: TObject); overload;
    procedure Output(OutputString: AnsiString; const Sender: TObject); overload;
    procedure Output(Feldwerte: TFieldInt; const Mark: Boolean); overload;
                                            //schreibt ins StringGrid, wenn Mark=TRUE wird Hexagonalfeld eingef�rbt
  public
    { Public-Deklarationen }
    procedure RefreshClientBoard;
    procedure ChangeStringGrid (Sender: TObject);
  end;

var
  Form_ClientTest: TForm_ClientTest;
  clMark: TColor;
  ClientBoard: TGameBoardClient;
  Pos: TPoint; //In Parameters ausgew�hltes Feld
  Pos2: TPoint; //Zweiter Positionsparamter
  Dir: TDirection; //In Parameters ausgew�hlte Richtung
  int: Integer; //IntegerParameter

  //test
  Targets: TPointArray;

implementation

uses Unit_TGameBoard, Unit_Server_Main, Unit_CustomProc;

{$R *.dfm}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//#######################################
//###############  ALLGEMEIN  ###########
//#######################################

procedure TForm_ClientTest.FormCreate(Sender: TObject);
begin
  self.Caption:='Client '+Unit_TGameBoard_Client.Version;
  //ClientBoard erstellen
  ClientBoard:=TGameBoardClient.Create;
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.FormShow(Sender: TObject);
begin
  RefreshClientBoard;
  OrderElements (self);
  clMark:=Pa_clMark.Color;
  //Position
  IF CB_StringGrid.Checked=TRUE
    THEN begin
      SE_Position_X1.Value:=Form_Server.SG_GameBoard.Col-1;
      SE_Position_Y1.Value:=Form_Server.SG_GameBoard.Row-1;
    end;
  //Richtung
  SE_DirectionChange(self);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.OrderElements (Sender: TObject);
const Abstand: byte = 3;
var ActiveGBHeight: integer;
begin
  //Formulare
  IF Sender<>RG_Methods
    THEN begin
      Form_Server.Left:=(screen.Width div 2) - ((Form_Server.Width+self.Width) div 2);
      IF Form_Server.Left+Form_Server.Width+self.Width > screen.Width
        THEN Form_Server.Left:=screen.Width - Form_Server.Width - self.Width;
      self.Left:=Form_Server.Left + Form_Server.Width;
      self.Top:=Form_Server.Top;
    end;

  //Methoden
  GB_Fields.Visible:=FALSE;
    GB_Fields.Top:=RG_Methods.Top+RG_Methods.Height + Abstand;
  GB_Fish.Visible:=FALSE;
    GB_Fish.Top:=GB_Fields.Top;
  GB_Move.Visible:=FALSE;
    GB_Move.Top:=GB_Fields.Top;
  GB_Quality.Visible:=FALSE;
    GB_Quality.Top:=GB_Fields.Top;
  CASE RG_Methods.ItemIndex of
    0: begin //Felder
         GB_Fields.Visible:=TRUE;
         ActiveGBHeight:=GB_Fields.Height;
       end;
    1: begin //Fische
         GB_Fish.Visible:=TRUE;
         ActiveGBHeight:=GB_Fish.Height;
       end;
    2: begin //Bewegung
         GB_Move.Visible:=TRUE;
         ActiveGBHeight:=GB_Move.Height;
       end;
    3: begin //Bewertungen
         GB_Quality.Visible:=TRUE;
         ActiveGBHeight:=GB_Quality.Height;
       end;
  end;
  //Andere Groupboxes
  GB_Parameters.Top:=GB_Fields.Top+ActiveGBHeight+ Abstand;
  GB_Output.Top:=GB_Parameters.Top+GB_Parameters.Height+ Abstand;
  self.ClientHeight:=GB_Output.Top+GB_Output.Height+ Abstand+1;
end;
//------------------------------------------------------------------------------

procedure TForm_ClientTest.FormActivate(Sender: TObject);
begin
  RefreshClientBoard;
  Unit_Server_Main.Board.RefreshPaintBox;
  Unit_Server_Main.Board.MarkField(Pos.X,Pos.Y,Unit_Server_Main.clMarkPos);
end;
//------------------------------------------------------------------------------
procedure TForm_ClientTest.RefreshClientBoard;
var x,y: byte;
    S: String;
begin
  ClientBoard.DecodeBoard(Unit_Server_Main.Board.EncodeBoard (0));
  //StringGrid
  FOR x:=0 to 10 do
    FOR y:=0 to 7 do
      begin
        S:='';
        IF ClientBoard.Field[x,y].Full=FALSE
          THEN begin
            S:=IntToStr(ClientBoard.Field[x,y].Fish);
          end
          ELSE begin
            S:='P'+IntToStr(ClientBoard.Field[x,y].Player)+','+IntToStr(ClientBoard.Field[x,y].PinguinNummer);
          end;

        IF ClientBoard.Field[x,y].valid=FALSE THEN S:='';
        Form_Server.SG_GameBoard.Cells[x+1,y+1]:=S;
      end;
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_ClientRefreshClick(Sender: TObject);
begin
  RefreshClientBoard;;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//#######################################
//#######  ALLGEMEINE PARAMETER  ########
//#######################################

procedure TForm_ClientTest.SE_DirectionChange(Sender: TObject);
begin
  CASE SE_Direction.Value of
    0: La_Direction.Caption:='oben';
    1: La_Direction.Caption:='oben rechts';
    2: La_Direction.Caption:='rechts';
    3: La_Direction.Caption:='unten';
    4: La_Direction.Caption:='unten links';
    5: La_Direction.Caption:='links';
  end;
  Dir:=SE_Direction.Value;
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.ChangeStringGrid (Sender: TObject);
begin
  Positionchange(Sender);
end;

procedure TForm_ClientTest.PositionChange(Sender:TObject);
begin
  Pos.X:=SE_Position_X1.Value;
  Pos.Y:=SE_Position_Y1.Value;
  Pos2.X:=SE_Position_X2.Value;
  Pos2.Y:=SE_Position_Y2.Value;

  IF (CB_StringGrid.Checked=TRUE) OR (Sender=Form_Server.SG_GameBoard)
    THEN Board.RefreshPaintBox;

  IF (Sender=SE_Position_X1) OR (Sender=SE_Position_Y1) OR
     (Sender=SE_Position_X2) OR (Sender=SE_Position_Y2)
    THEN begin
      IF CB_StringGrid.Checked=TRUE
        THEN begin
          Form_Server.SG_GameBoard.Col:=SE_Position_X1.Value+1;
          Form_Server.SG_GameBoard.Row:=SE_Position_Y1.Value+1;
        end;
    end;

  IF Board.Field[Pos2.X,Pos2.Y].OnGameBoard=TRUE
    THEN begin
      Board.MarkField(Pos2.X,Pos2.Y,clMarkAlt);
    end;
  IF Board.Field[Pos.X,Pos.Y].OnGameBoard=TRUE
    THEN begin
      Board.MarkField(Pos.X,Pos.Y,clMarkPos);
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.SE_IntParChange(Sender: TObject);
begin
  Int:=SE_IntPar.Value;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//#######################################
//############## AUSGABE  ##############
//#######################################

procedure TForm_ClientTest.Btn_StringGridSetBackClick(Sender: TObject);
begin
  Unit_Server_Main.Board.DrawQuad;
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.PanelColor(Sender:TObject);
begin
  IF Sender=Pa_clMark
    THEN begin
      ColorDialog_clMark.Color:=Pa_clMark.Color;
      ColorDialog_clMark.Color:=Pa_clMark.Color;
      ColorDialog_clMark.Execute;
    end;
  Pa_clMark.Color:=ColorDialog_clMark.Color;
  clMark:=Pa_clMark.Color;
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Output(Punkt: TPoint; const MarkPosition: Boolean; const Sender: TObject);
begin
  //Editfeld
  Ed_Output.Text:='('+IntToStr(Punkt.X)+'|'+IntToStr(Punkt.Y)+')';
  //Feld
  Unit_Server_Main.Board.RefreshPaintBox;
  Unit_Server_Main.Board.MarkField(Punkt.X,Punkt.Y,clMark);
  IF MarkPosition=TRUE THEN Unit_Server_Main.Board.MarkField(Pos.X,Pos.Y,Unit_Server_Main.clMarkPos);
end;
//------------------------------------------------------------------------------
procedure TForm_ClientTest.Output(Punkte: Unit_TGameBoard_Client.TPointArray; MarkPosition: Boolean; Sender: TObject);
var i:integer;
begin
  Ed_Output.Text:='';
  Unit_Server_Main.Board.RefreshPaintBox;
  FOR i:=0 to High(Punkte) do
    begin
      Ed_Output.Text:=Ed_Output.Text+'('+IntToStr(Punkte[i].X)+'|'+IntToStr(Punkte[i].Y)+'), ';
      Unit_Server_Main.Board.MarkField(Punkte[i].X,Punkte[i].Y,clMark);
    end;
  IF MarkPosition=TRUE THEN Unit_Server_Main.Board.MarkField(Pos.X,Pos.Y,Unit_Server_Main.clMarkPos);
end;
//------------------------------------------------------------------------------
procedure TForm_ClientTest.Output(OutputInt: Integer; const Sender: TObject);
begin
  Ed_OutPut.Text:=IntToStr(OutputInt);
end;
//------------------------------------------------------------------------------
procedure TForm_ClientTest.Output(OutputString: AnsiString; const Sender: TObject);
begin
  Ed_OutPut.Text:=OutputString;
end;
//------------------------------------------------------------------------------
procedure TForm_ClientTest.Output(Feldwerte: TFieldInt; const Mark: Boolean);
var x,y: byte;
    Max,Min, Dif: Integer; //H�chste und niedrigste Werte im Array, Dif die Differenz dazwischen
    R,G,B, Hue: Double;
begin
  Max:=0; Min:=2147483647;
  //StringGrid
  FOR x:=0 to 10 do
    FOR y:=0 to 7 do
      begin
        IF Feldwerte[x,y]>Max THEN Max:=Feldwerte[x,y];
        IF Feldwerte[x,y]<Min THEN Min:=Feldwerte[x,y];
        With Form_Server.SG_GameBoard do
          begin
            IF ClientBoard.Field[x,y].valid = TRUE
              THEN Cells[x+1,y+1]:=IntToStr(Feldwerte[x,y])
              ELSE Cells[x+1,y+1]:=' ~ ';
            IF ClientBoard.Field[x,y].OnGameBoard = FALSE
              THEN Cells[x+1,y+1]:=' ';
          end;
      end;
  //Farben
  IF Mark=TRUE
    THEN begin
      Dif:=Max-Min;
      FOR x:=0 to 10 do
        FOR y:=0 to 7 do
          begin
            Hue:=round( 255 - (Feldwerte[x,y]/Dif) * 255);
            HSVtoRGB(Hue,150,240,R,G,B);
            StringGridColors[x,y]:=RGB(round(R),round(G),round(B));
            IF ClientBoard.Field[x,y].valid=FALSE
              THEN StringGridColors[x,y]:=clValidFalse;
            IF ClientBoard.Field[x,y].OnGameBoard=FALSE
              THEN StringGridColors[x,y]:=clOnGameBoardFalse;
          end;
    end;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//#######################################
//############## METHODEN  ##############
//#######################################



procedure TForm_ClientTest.Btn_GetNextFieldInDirectionClick(Sender: TObject);
var Feld: TPoint;
begin
  Feld:=ClientBoard.GetNextFieldInDirection(Pos.X,Pos.Y,Dir);
  Output(Feld,TRUE, Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetAllTargetsClick(Sender: TObject);
begin
  Output(ClientBoard.GetAllTargets(Pos.X,Pos.Y),TRUE,Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetTargetsInDirectionClick(Sender: TObject);
begin
  OutPut(ClientBoard.GetTargetsInDirection(Pos.X,Pos.Y,Dir),TRUE,Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetAllNeighborFieldsClick(Sender: TObject);
begin
//  OutPut(ClientBoard.GetAllNeighborFields(Pos.X,Pos.Y),TRUE,Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetFieldsInRadiusClick(Sender: TObject);
begin
  IF (int>=1) AND (int<5) //test
    THEN OutPut(ClientBoard.GetFieldsInRadius(Pos.X,Pos.Y,int),TRUE,Sender)
    ELSE ShowMessage('IntegerParameter hat unzul�ssigen Wert!');
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetAllFishClick(Sender: TObject);
begin
  Output(ClientBoard.GetAllFish(Pos.X,Pos.Y),Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetAllFishAlleClick(Sender: TObject);
var x,y:byte;
    Feldwert: TFieldInt;
begin
  FOR x:=0 to 10 do
    FOR y:=0 to 7 do
      Feldwert[x,y]:=Clientboard.GetAllFish(x,y);
  Output(Feldwert,TRUE);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetAllReachableFieldsOfFishClick(
  Sender: TObject);
begin
  IF int<1 THEN SE_IntPar.Value:=1;
  IF int>3 THEN SE_IntPar.Value:=3;
  OutPut(ClientBoard.GetAllReachableFieldsOfFish(Pos.X,Pos.Y,Int),TRUE,Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetSumOfReachableFishClick(Sender: TObject);
begin
  IF int<1 THEN SE_IntPar.Value:=1;
  IF int>3 THEN SE_IntPar.Value:=3;
  Output(ClientBoard.GetSumOfReachableFish(Pos.X,Pos.Y,int),Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_CheckPathClick(Sender: TObject);
begin
  IF ClientBoard.CheckPath(Pos.X,Pos.Y,Pos2.X,Pos2.Y)=TRUE
    THEN Output('TRUE',Sender)
    ELSE Output('FALSE',Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetPathClick(Sender: TObject);
begin
//  Output(ClientBoard.GetPath(Pos.X,Pos.Y,Pos2.X,Pos2.Y),TRUE,Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetBestFishClick(Sender: TObject);
begin
//  Output(ClientBoard.GetBestFish(Pos.X,Pos.Y),Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetBestFishRandomFieldClick(Sender: TObject);
begin
//  Output(ClientBoard.GetBestFishRandomField(Pos.X,Pos.Y),TRUE,Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_GetFishInLineClick(Sender: TObject);
begin
  IF Int<1
    THEN SE_IntPar.Value:=1;
//  Output(ClientBoard.GetFishInLine(Pos.X,Pos.Y,Int),TRUE,Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form_Server.Left:=(screen.Width div 2) - ((Form_Server.Width) div 2);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_RateFieldsAlleClick(Sender: TObject);
var x,y: byte;
    Feldwert: TFieldInt;
begin
  IF Int<0
    THEN SE_IntPar.Value:=0;
  IF Int>4
    THEN SE_IntPar.Value:=4;

  FOR x:=0 to 10 do
    FOR y:=0 to 7 do
      Feldwert[x,y]:=ClientBoard.AutoRateFields(x,y,Int);

  Output(Feldwert,TRUE);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_RateFieldClick(Sender: TObject);
begin
  IF Int<0
    THEN SE_IntPar.Value:=0;
  IF Int>4
    THEN SE_IntPar.Value:=4;

  Output(ClientBoard.AutoRateFields(Pos.X,Pos.Y,Int), Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_RateEnemysMovabilityClick(Sender: TObject);
var i: Integer;
begin
//  Output(ClientBoard.RateEnemysMovability(Pos.X,Pos.Y,3,3,10), self);
end;

//------------------------------------------------------------------------------

procedure TForm_ClientTest.Btn_RateEnemysMovabilityAlleClick(Sender: TObject);
var x,y: byte;
    i: Integer;
    Feld: TFieldInt;
begin
  FOR x:=0 to 10 do
    FOR y:=0 to 7 do
      begin
//        i:=ClientBoard.RateEnemysMovability(Pos.X,Pos.Y,3,3,10);
        Form_Server.SG_GameBoard.Cells[x+1,y+1]:= IntToStr(i);
      end;
//  Output(Feld);
end;

//------------------------------------------------------------------------------




end.
