unit Unit_TestSets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_TGameBoard, StdCtrls;

type
  TForm_TestSets = class(TForm)
    La_GameSum: TLabel;
    La_Games: TLabel;
    La_Wait: TLabel;
    La_AfterGame: TLabel;
    La_StartPlayer: TLabel;
    La_SavePath: TLabel;
    Ed_SavePath: TEdit;
    GroupBox: TGroupBox;
    Btn_OK: TButton;
    procedure Btn_OKClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FSets: TTestSettings;
    procedure SetSets(TS: TTestSettings);
  public
    { Public-Deklarationen }
    property Sets: TTestSettings read FSets write SetSets;
  end;

var
  Form_TestSets: TForm_TestSets;

implementation

{$R *.dfm}

procedure TForm_TestSets.SetSets(TS: TTestSettings);
var S:String;
begin

  //Anzahl der Spiele
  La_Games.Caption:='Anzahl der Spiele pro Begegnung: '+IntToStr(TS.AmountOfGames);
  La_GameSum.Caption:='Spiele insgesamt: '+IntToStr( TS.AmountOfGames * (TS.High0+1) * (TS.High1+1) );

  //Verzögerter Zug
  IF TS.WaitEachTurn=0
    THEN S:='aus'
    ELSE S:=IntToStr(TS.WaitEachTurn)+' ms';
  La_Wait.Caption:='Verzögerter Zug: '+S;

  //Nach jedem Spiel 
  CASE TS.AfterEachGame of
    0: S:='Clients neustarten';
    1: S:='Clients zurücksetzen';
    2: S:='gar nichts';
  end;
  La_AfterGame.Caption:='Nach jedem Spiel: '+S;

  //Startspieler
  CASE TS.StartPlayer of
    0: S:='Spieler 0';
    1: S:='Spieler 1';
    2: S:='Zufall';
  end;
  La_StartPlayer.Caption:='Startspieler: '+S;

  //Speicherpfad
  IF TS.SaveGames=FALSE
    THEN Ed_SavePath.Text:='deaktiviert'
    ELSE Ed_SavePath.Text:=TS.SavePath;
end;

procedure TForm_TestSets.Btn_OKClick(Sender: TObject);
begin
  close;
end;

end.
