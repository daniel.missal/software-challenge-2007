object Form_ClientTest: TForm_ClientTest
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'TGameBoard_Client'
  ClientHeight = 668
  ClientWidth = 218
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GB_Parameters: TGroupBox
    Left = 0
    Top = 400
    Width = 209
    Height = 145
    Caption = 'Allgemeine Parameter'
    TabOrder = 1
    object La_Direction_Caption: TLabel
      Left = 8
      Top = 96
      Width = 46
      Height = 13
      Caption = 'Richtung:'
    end
    object La_Position: TLabel
      Left = 8
      Top = 16
      Width = 48
      Height = 13
      Caption = 'Position:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object La_Position_X1: TLabel
      Left = 24
      Top = 48
      Width = 14
      Height = 16
      Caption = 'X:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object La_Position_Y1: TLabel
      Left = 24
      Top = 72
      Width = 14
      Height = 16
      Caption = 'Y:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object La_Direction: TLabel
      Left = 56
      Top = 115
      Width = 59
      Height = 13
      Caption = 'La_Direction'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object La_IntPar: TLabel
      Left = 112
      Top = 96
      Width = 84
      Height = 13
      Caption = 'IntegerParamter:'
    end
    object La_Position_Y2: TLabel
      Left = 112
      Top = 72
      Width = 19
      Height = 16
      Caption = 'Y2:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object La_Position_X2: TLabel
      Left = 112
      Top = 48
      Width = 20
      Height = 16
      Caption = 'X2:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object SE_Direction: TSpinEdit
      Left = 16
      Top = 112
      Width = 33
      Height = 22
      MaxValue = 5
      MinValue = 0
      TabOrder = 0
      Value = 0
      OnChange = SE_DirectionChange
    end
    object CB_StringGrid: TCheckBox
      Left = 24
      Top = 32
      Width = 73
      Height = 17
      Caption = 'StringGrid'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object SE_Position_X1: TSpinEdit
      Left = 48
      Top = 48
      Width = 41
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MaxValue = 10
      MinValue = 0
      ParentFont = False
      TabOrder = 2
      Value = 0
      OnChange = PositionChange
    end
    object SE_Position_Y1: TSpinEdit
      Left = 48
      Top = 72
      Width = 41
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MaxValue = 7
      MinValue = 0
      ParentFont = False
      TabOrder = 3
      Value = 0
      OnChange = PositionChange
    end
    object SE_IntPar: TSpinEdit
      Left = 152
      Top = 112
      Width = 49
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 4
      Value = 0
      OnChange = SE_IntParChange
    end
    object SE_Position_Y2: TSpinEdit
      Left = 136
      Top = 72
      Width = 41
      Height = 22
      MaxValue = 7
      MinValue = 0
      TabOrder = 5
      Value = 0
      OnChange = PositionChange
    end
    object SE_Position_X2: TSpinEdit
      Left = 136
      Top = 48
      Width = 41
      Height = 22
      MaxValue = 10
      MinValue = 0
      TabOrder = 6
      Value = 0
      OnChange = PositionChange
    end
  end
  object GB_Output: TGroupBox
    Left = 0
    Top = 552
    Width = 209
    Height = 105
    Caption = 'Ausgabe'
    TabOrder = 2
    object La_clMark: TLabel
      Left = 40
      Top = 56
      Width = 84
      Height = 13
      Caption = 'Markierungsfarbe'
    end
    object Ed_Output: TEdit
      Left = 8
      Top = 24
      Width = 193
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object Pa_clMark: TPanel
      Left = 8
      Top = 48
      Width = 25
      Height = 25
      Color = clYellow
      TabOrder = 1
      OnClick = PanelColor
    end
  end
  object RG_Methods: TRadioGroup
    Left = 0
    Top = 0
    Width = 105
    Height = 73
    Caption = 'Methoden'
    ItemIndex = 0
    Items.Strings = (
      'Felder'
      'Fische'
      'Bewegung'
      'Bewertungen')
    TabOrder = 3
    OnClick = OrderElements
  end
  object GB_Fish: TGroupBox
    Left = 0
    Top = 256
    Width = 209
    Height = 105
    Caption = 'Methoden - Fische'
    TabOrder = 4
    object Btn_GetAllFish: TButton
      Left = 8
      Top = 16
      Width = 193
      Height = 17
      Hint = 'function GetAllFish(const x,y: byte): integer;'
      Caption = 'GetAllFish (Einzeln)'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = Btn_GetAllFishClick
    end
    object Btn_GetAllFishAlle: TButton
      Left = 8
      Top = 32
      Width = 193
      Height = 17
      Hint = 'function GetAllFish(const x,y: byte): integer;'
      Caption = 'GetAllFish (Alle)'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = Btn_GetAllFishAlleClick
    end
    object Btn_GetSumOfReachableFish: TButton
      Left = 8
      Top = 48
      Width = 193
      Height = 17
      Hint = 'function GetSumOfReachableFish(X,Y: byte; Fish: TFish): Integer;'
      Caption = 'GetSumOfReachableFish'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = Btn_GetSumOfReachableFishClick
    end
    object Btn_GetAllReachableFieldsOfFish: TButton
      Left = 8
      Top = 64
      Width = 193
      Height = 17
      Hint = 
        'function GetAllReachableFieldsOfFish(X,Y: byte; Fish: TFish): TP' +
        'ointArray;'
      Caption = 'GetAllReachableFieldsOfFish'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = Btn_GetAllReachableFieldsOfFishClick
    end
    object Btn_GetFishInLine: TButton
      Left = 8
      Top = 80
      Width = 193
      Height = 17
      Hint = 'function GetFishInLine(x,y,Firstfish:byte): TpointArray;'
      Caption = 'GetFishInLine'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = Btn_GetFishInLineClick
    end
  end
  object GB_Move: TGroupBox
    Left = 0
    Top = 184
    Width = 209
    Height = 73
    Caption = 'Methoden - Bewegung'
    TabOrder = 5
    object Btn_CheckPath: TButton
      Left = 8
      Top = 16
      Width = 193
      Height = 17
      Hint = 'function CheckPath(X1,Y1,X2,Y2: Byte): Boolean;'
      Caption = 'CheckPath'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = Btn_CheckPathClick
    end
    object Btn_GetPath: TButton
      Left = 8
      Top = 32
      Width = 193
      Height = 17
      Hint = 'function GetPath(X1,Y1,X2,Y2: Byte): TPointArray;'
      Caption = 'GetPath'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = Btn_GetPathClick
    end
    object Btn_GetBestFishRandomField: TButton
      Left = 8
      Top = 48
      Width = 193
      Height = 17
      Hint = 'function GetAllReachableFieldsOfFish(Fish: TFish): TPointArray;'
      Caption = 'GetBestFishRandomField'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = Btn_GetBestFishRandomFieldClick
    end
  end
  object GB_quality: TGroupBox
    Left = 0
    Top = 352
    Width = 209
    Height = 89
    Caption = 'Methoden - Bewertungen'
    TabOrder = 6
    object Btn_RateField: TButton
      Left = 8
      Top = 16
      Width = 193
      Height = 17
      Hint = 'function AutoRateFields(x,y,durchlauf: byte):integer;'
      Caption = 'RateField (Einzeln)'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = Btn_RateFieldClick
    end
    object Btn_RateFieldsAlle: TButton
      Left = 8
      Top = 32
      Width = 193
      Height = 17
      Hint = 'function AutoRateFields(x,y,durchlauf: byte):integer;'
      Caption = 'RateFields (Alle)'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = Btn_RateFieldsAlleClick
    end
    object Btn_RateEnemysMovability: TButton
      Left = 8
      Top = 48
      Width = 193
      Height = 17
      Hint = 
        'function RateEnemysMovability (x,y,gewFelder, gewFische, gewUnbe' +
        'weglich: byte): Integer;'
      Caption = 'RateEnemysMovability (Einzeln)'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = Btn_RateEnemysMovabilityClick
    end
    object Btn_RateEnemysMovabilityAlle: TButton
      Left = 8
      Top = 64
      Width = 193
      Height = 17
      Hint = 
        'function RateEnemysMovability (x,y,gewFelder, gewFische, gewUnbe' +
        'weglich: byte): Integer;'
      Caption = 'RateEnemysMovability (Alle)'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = Btn_RateEnemysMovabilityAlleClick
    end
  end
  object Btn_StringGridSetBack: TButton
    Left = 112
    Top = 8
    Width = 97
    Height = 33
    Caption = 'StringGrid zur'#252'cksetzen'
    TabOrder = 7
    WordWrap = True
    OnClick = Btn_StringGridSetBackClick
  end
  object GB_Fields: TGroupBox
    Left = 0
    Top = 64
    Width = 209
    Height = 121
    Caption = 'Methoden - Felder'
    TabOrder = 0
    object Btn_GetNextFieldInDirection: TButton
      Left = 8
      Top = 16
      Width = 193
      Height = 17
      Hint = 
        'function GetNextFieldInDirection(x,y: byte;Direction: TDirection' +
        '): TPoint;'
      Caption = 'GetNextFieldInDirection'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = Btn_GetNextFieldInDirectionClick
    end
    object Btn_GetAllTargets: TButton
      Left = 8
      Top = 32
      Width = 193
      Height = 17
      Hint = 'function GetAllTargets(const x,y:byte): TPointArray;'
      Caption = 'GetAllTargets'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = Btn_GetAllTargetsClick
    end
    object Btn_GetTargetsInDirection: TButton
      Left = 8
      Top = 48
      Width = 193
      Height = 17
      Hint = 
        'function GetTargetsInDirection(const x,y: byte; Direction: TDire' +
        'ction): TPointArray;'
      Caption = 'GetTargetsInDirection'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = Btn_GetTargetsInDirectionClick
    end
    object Btn_GetAllNeighborFields: TButton
      Left = 8
      Top = 64
      Width = 193
      Height = 17
      Hint = 'function GetAllNeighborFields(x,y: byte): TPointArray;'
      Caption = 'GetAllNeighborFields'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = Btn_GetAllNeighborFieldsClick
    end
    object Btn_GetFieldsInRadius: TButton
      Left = 8
      Top = 80
      Width = 193
      Height = 17
      Hint = 
        'function GetFieldsInRadius(x,y: byte; Radius: byte): TPointArray' +
        ';'
      Caption = 'GetFieldsInRadius'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = Btn_GetFieldsInRadiusClick
    end
    object Btn_GetBestFish: TButton
      Left = 8
      Top = 96
      Width = 193
      Height = 17
      Hint = 'function GetBestFish(const X,Y: byte): TFish;'
      Caption = 'GetBestFish'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = Btn_GetBestFishClick
    end
  end
  object Btn_ClientRefresh: TButton
    Left = 112
    Top = 40
    Width = 97
    Height = 25
    Caption = 'Refresh Client'
    TabOrder = 8
    OnClick = Btn_ClientRefreshClick
  end
  object ColorDialog_clMark: TColorDialog
    OnClose = PanelColor
    Options = [cdFullOpen]
    Left = 8
    Top = 520
  end
end
