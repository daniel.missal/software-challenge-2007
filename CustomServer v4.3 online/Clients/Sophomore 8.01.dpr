PROGRAM Sophomore;
{$AppType CONSOLE}

{ USES LCLIntf,Math,SysUtils; }
USES Math,SysUtils,Windows;

VAR Client_Name,Client_Version,Client_Author : STRING;

PROCEDURE dm_Client_Information;
BEGIN
     Client_Name    := 'Sophomore';
     Client_Version := '8.01';
     Client_Author  := 'Daniel Missal';
END;

{******************************************************************************}
{*** Inhalts�bersicht *********************************************************}
{******************************************************************************}
{                                                                              }
{=== 1 Deklarationen zur Informationsverarbeitung =============================}
{----- 1.1 Speicher -----------------------------------------------------------}
{----- 1.2 Prozeduren und Funktionen ------------------------------------------}
{=== 2 Deklarationen zur KI ===================================================}
{----- 2.1 Speicher -----------------------------------------------------------}
{----- 2.2 Prozeduren und Funktionen ------------------------------------------}
{=== 3 Hauptprozedur ==========================================================}
{                                                                              }
{******************************************************************************}



{=== 1 Deklarationen zur Informationsverarbeitung =============================}


{----- 1.1 Speicher -----------------------------------------------------------}


TYPE Type_Feld  = RECORD
                   E   : INTEGER;
                   FrN : INTEGER;
                   P   : INTEGER;
                   S   : INTEGER;
END;
{ 'E'    bedeutet 'Eigentum'.                          }
{ 'FrN'  bedeutet 'Figur_Nummer'.                      }
{ 'P'    bedeutet 'Punkte'.                            }
{ 'S'    bedeutet 'Status'.                            }
TYPE Type_Figur = RECORD
                   x            : INTEGER;
                   y            : INTEGER;
END;

VAR  Informationen,LZG,LZS,Zug : ANSISTRING;
{ 'LZG'  bedeutet 'Letzter_Zug_Gegner'.                }
{ 'LZS'  bedeutet 'Letzter_Zug_Selbst'.                }
VAR  Farbe   : ARRAY[0..2]            OF STRING;
VAR  ZDG,ZDS : ARRAY[1..56]           OF CARDINAL;
VAR  SFd     : ARRAY[1..2,0..7,0..10] OF Type_Feld;
VAR  SFr     : ARRAY[1..2,1..2,1..4]  OF Type_Figur;
{ 'ZDG'  bedeutet 'Zugdauer_Gegner'.                   }
{ 'ZDS'  bedeutet 'Zugdauer_Selbst'.                   }
{ 'SFd'  bedeutet 'Stellungsfeld'.                     }
{ 'SFr'  bedeutet 'Stellungsfigur'.                    }
VAR  Emulator,SN                                                   : BOOLEAN;
{ 'SN'   bedeutet 'Selbst_Nachziehend'.                }
VAR  Gegner,HN,Nicht_vorhanden,PG,PS,Selbst,Unbekannt,Vorhanden,ZN : INTEGER;
{ 'HN'   bedeutet 'Halbzug_Nummer'.                    }
{ 'PG'   bedeutet 'Punktzahl_Gegner'.                  }
{ 'PS'   bedeutet 'Punktzahl_Selbst'.                  }
{ 'ZN'   bedeutet 'Zug_Nummer'.                        }
VAR  GZD,GZDG,GZDS,ZZBG,ZZBS                                       : INT64;
{ 'GZD'  bedeutet 'Gr��te_Zugdauer'.                   }
{ 'GZDG' bedeutet 'Gesamtzugdauer_Gegner'.             }
{ 'GZDS' bedeutet 'Gesamtzugdauer_Selbst'.             }
{ 'ZZBG' bedeutet 'Zugzeitbeginn_Gegner'.              }
{ 'ZZBS' bedeutet 'Zugzeitbeginn_Selbst'.              }


{----- 1.2 Prozeduren und Funktionen ------------------------------------------}


PROCEDURE Log(Nachricht : ANSISTRING);
{ 'Log'  bedeutet 'Protokollierung'.                   }
BEGIN
     // WRITELN(ERROUTPUT,PCHAR('debug '+Nachricht));
     FLUSH(ERROUTPUT);
END;


PROCEDURE dm_Initialisierung;
FORWARD;


PROCEDURE dm_Emulation;
FORWARD;


PROCEDURE dm_Zugsendung;
BEGIN
     WRITELN(PCHAR(Zug));
     FLUSH(OUTPUT);
     QueryPerformanceCounter(ZZBG);
     ZDS[ZN] := ZZBG-ZZBS+1;
     GZDS := GZDS+ZDS[ZN];
     IF (ZDS[ZN] > GZD) THEN GZD := ZDS[ZN];
     LZS := Zug;
     HN  := HN+2;
     ZN  := ZN+1;
     IF ((ZN = 3) AND (Selbst = 1)) THEN dm_Emulation;
     dm_Initialisierung;
END;


FUNCTION EFd(y,x : INTEGER) : BOOLEAN;
{ 'EFd'  bedeutet 'Existierendes_Feld'.                }
BEGIN
     EFd := TRUE;
     IF ((y < 0) OR (y > 7) OR (x < 0) OR (x > 10)) THEN EFd := FALSE;
END;


FUNCTION dm_Registrierte_Figur(Spieler,y,x : INTEGER) : BOOLEAN;
VAR i : INTEGER;
BEGIN
     dm_Registrierte_Figur := FALSE;
     FOR i := 1 TO 4 DO
     BEGIN
          IF ((SFr[1,Spieler,i].x = x) AND (SFr[1,Spieler,i].y = y))
          THEN dm_Registrierte_Figur := TRUE;
     END;
END;


FUNCTION RS_x(Richtung,Entfernung : INTEGER) : INTEGER;
{ 'RS'   bedeutet 'Richtungssummand'.                  }
VAR Delphi_Integer : INTEGER;
BEGIN
     IF (Richtung = 1) THEN Delphi_Integer :=  1;
     IF (Richtung = 2) THEN Delphi_Integer :=  0;
     IF (Richtung = 3) THEN Delphi_Integer := -1;
     IF (Richtung = 4) THEN Delphi_Integer := -1;
     IF (Richtung = 5) THEN Delphi_Integer :=  0;
     IF (Richtung = 6) THEN Delphi_Integer :=  1;
     RS_x := Delphi_Integer*Entfernung;
END;


FUNCTION RS_y(Richtung,Entfernung : INTEGER) : INTEGER;
VAR Delphi_Integer : INTEGER;
BEGIN
     IF (Richtung = 1) THEN Delphi_Integer :=  0;
     IF (Richtung = 2) THEN Delphi_Integer :=  1;
     IF (Richtung = 3) THEN Delphi_Integer :=  1;
     IF (Richtung = 4) THEN Delphi_Integer :=  0;
     IF (Richtung = 5) THEN Delphi_Integer := -1;
     IF (Richtung = 6) THEN Delphi_Integer := -1;
     RS_y := Delphi_Integer*Entfernung;
END;


FUNCTION VDZZ : INTEGER;
{ 'VDZZ' bedeutet 'Verbleibende_Durchschnittszugzeit'. }
VAR Delphi_Integer : INT64;
BEGIN
     IF (ZN > 25) THEN RESULT := -10
     ELSE BEGIN
          QueryPerformanceCounter(Delphi_Integer);
          RESULT := (60000000-GZDS-Delphi_Integer+ZZBS-2) DIV (31-ZN) -1;
          IF (RESULT > 5000000) THEN RESULT := 5000000;
     END;
END;


FUNCTION VGZ  : CARDINAL;
{ 'VGZ'  bedeutet 'Verbleibende_Gesamtzeit'.           }
VAR Delphi_Integer : INT64;
BEGIN
     QueryPerformanceCounter(Delphi_Integer);
     RESULT := 60000000-GZDS-Delphi_Integer+ZZBS-2;
END;


FUNCTION VZZ  : CARDINAL;
{ 'VZZ'  bedeutet 'Verbleibende_Zugzeit'.              }
VAR Delphi_Integer : INT64;
BEGIN
     QueryPerformanceCounter(Delphi_Integer);
     RESULT := 5000000-Delphi_Integer+ZZBS-1;
     IF (VGZ < RESULT) THEN RESULT := VGZ;
END;


FUNCTION ZA_x(Zugstring : STRING) : INTEGER;
{ 'ZA'   bedeutet 'Zuganfang'.                         }
BEGIN
     ZA_x := StrToInt(Zugstring[3]);
     IF ((Zugstring[3] = '1') AND (Zugstring[4] = '0')) THEN ZA_x := 10;
END;


FUNCTION ZA_y(Zugstring : STRING) : INTEGER;
BEGIN
     ZA_y := StrToInt(Zugstring[1]);
END;


FUNCTION ZE_x(Zugstring : STRING) : INTEGER;
{ 'ZE'   bedeutet 'Zugende'.                           }
BEGIN
     IF ((Zugstring[LENGTH(Zugstring)] = '0')
         AND
         (Zugstring[LENGTH(Zugstring)-2] = '0')) THEN
     BEGIN
          ZE_x := StrToInt(Zugstring[3]);
          IF ((Zugstring[3] = '1') AND (Zugstring[4] = '0')) THEN ZE_x := 10;
     END
     ELSE BEGIN
          ZE_x := StrToInt(Zugstring[LENGTH(Zugstring)]);
          IF ((Zugstring[LENGTH(Zugstring)-1] = '1')
              AND
              (Zugstring[LENGTH(Zugstring)] = '0'))
          THEN ZE_x := 10;
     END;
END;


FUNCTION ZE_y(Zugstring : STRING) : INTEGER;
BEGIN
     IF ((Zugstring[LENGTH(Zugstring)] = '0')
         AND
         (Zugstring[LENGTH(Zugstring)-2] = '0'))
     THEN ZE_y := StrToInt(Zugstring[1])
     ELSE BEGIN
          IF ((Zugstring[LENGTH(Zugstring)-1] = '1')
              AND
              (Zugstring[LENGTH(Zugstring)] = '0'))
          THEN ZE_y := StrToInt(Zugstring[LENGTH(Zugstring)-3])
          ELSE ZE_y := StrToInt(Zugstring[LENGTH(Zugstring)-2]);
     END;
END;


PROCEDURE dm_Erste_Definitionen;
BEGIN
     Farbe[0]        := 'Unbekannt';
     Farbe[1]        := 'Blau';
     Farbe[2]        := 'Rot';
     Emulator        := FALSE;
     GZD             := 0;
     GZDG            := 0;
     GZDS            := 0;
     Nicht_vorhanden := 0;
     PG              := 0;
     PS              := 0;
     Unbekannt       := 0;
     Vorhanden       := 1;
     ZN              := 1;
     QueryPerformanceCounter(ZZBG);
END;


PROCEDURE dm_Feldzuweisungen;
VAR i,x,y : INTEGER;
VAR k : STRING;
BEGIN
     i := 1;
     x := 0;
     y := 0;
     REPEAT
           i := i+2;
           k := Informationen[i];
           IF (Informationen[i+1] <> ' ') THEN
           BEGIN
                k := Informationen[i]+Informationen[i+1];
                i := i+1;
           END;
           SFd[1,y,x].E := StrToInt(k) MOD 4;
           IF ((Emulator = TRUE) AND (StrToInt(k) MOD 4 > 0))
           THEN SFd[1,y,x].E := 3-(StrToInt(k) MOD 4);
           SFd[1,y,x].P := StrToInt(k) DIV 4;
           IF (StrToInt(k) = 0) THEN SFd[1,y,x].S := Nicht_vorhanden
           ELSE SFd[1,y,x].S := Vorhanden;
           IF (x = 10) THEN y := y+1;
           x := (x+1) MOD 11;
     UNTIL (i+2 > LENGTH(Informationen));
END;


PROCEDURE dm_Farbenermittlung_im_Nachzug;
VAR i : INTEGER;
BEGIN
     Gegner := Unbekannt;
     Selbst := Unbekannt;
     SN     := FALSE;
     i      := 1;
     REPEAT
           i := i+2;
           IF (StrToInt(Informationen[i]) = 1) THEN
           BEGIN
                i := i+1;
                IF (ABS(StrToInt(Informationen[i])-2) MOD 4 > 0) THEN
                BEGIN
                     Gegner := ABS(StrToInt(Informationen[i])-2) MOD 4;
                     Selbst := 3-Gegner;
                     SN     := TRUE;
                END;
           END
           ELSE BEGIN
                IF (StrToInt(Informationen[i]) MOD 4 > 0) THEN
                BEGIN
                     Gegner := StrToInt(Informationen[i]) MOD 4;
                     Selbst := 3-Gegner;
                     SN     := TRUE;
                END;
           END;
     UNTIL (i+2 > LENGTH(Informationen));
     Log('Farbe Selbst: '+Farbe[Selbst]);
     Log('Farbe Gegner: '+Farbe[Gegner]);
     IF (Gegner > Unbekannt) THEN HN := 2 ELSE HN := 1;
END;


PROCEDURE dm_Farbenermittlung_im_Anzug;
BEGIN
     Selbst := SFd[1,ZE_y(LZS),ZE_x(LZS)].E;
     Gegner := 3-Selbst;
     Log('Farbe Selbst: '+Farbe[Selbst]);
     Log('Farbe Gegner: '+Farbe[Gegner]);
END;


PROCEDURE dm_Figurenregistrierung;
VAR i,x,y : INTEGER;
BEGIN
     IF (SN) THEN
     BEGIN
          IF (ZN = 1) THEN
          BEGIN
               x                           := -1;
               y                           := -1;
               REPEAT
                     x := (x+1) MOD 11;
                     IF (x = 0) THEN y := y+1;
                     IF ((SFd[1,y,x].E = Gegner)
                         AND
                         (dm_Registrierte_Figur(Gegner,y,x) = FALSE)) THEN
                     BEGIN
                          LZG := IntToStr(y)+' '+IntToStr(x)+' 0 0';
                          Log('Letzter_Zug_Gegner: '+LZG);
                          SFr[1,Gegner,ZN].x := x;
                          SFr[1,Gegner,ZN].y := y;
                          SFd[1,y,x].FrN     := ZN;
                          BREAK;
                     END;
               UNTIL ((y=7) AND (x=10));
          END;
          IF ((ZN > 1) AND (ZN < 5)) THEN
          BEGIN
               SFr[1,Selbst,ZN-1].x           := ZE_x(LZS);
               SFr[1,Selbst,ZN-1].y           := ZE_y(LZS);
               SFd[1,ZE_y(LZS),ZE_x(LZS)].FrN := ZN-1;
               x                           := -1;
               y                           := -1;
               REPEAT
                     x := (x+1) MOD 11;
                     IF (x = 0) THEN y := y+1;
                     IF ((SFd[1,y,x].E = Gegner)
                         AND
                         (dm_Registrierte_Figur(Gegner,y,x) = FALSE)) THEN
                     BEGIN
                          LZG := IntToStr(y)+' '+IntToStr(x)+' 0 0';
                          Log('Letzter_Zug_Gegner: '+LZG);
                          SFr[1,Gegner,ZN].x := x;
                          SFr[1,Gegner,ZN].y := y;
                          SFd[1,y,x].FrN     := ZN;
                          BREAK;
                     END;
               UNTIL ((y=7) AND (x=10));
          END;
          IF (ZN = 5) THEN
          BEGIN
               SFr[1,Selbst,ZN-1].x           := ZE_x(LZS);
               SFr[1,Selbst,ZN-1].y           := ZE_y(LZS);
               SFd[1,ZE_y(LZS),ZE_x(LZS)].FrN := ZN-1;
               x := -1;
               y := -1;
               REPEAT
                     x := (x+1) MOD 11;
                     IF (x = 0) THEN y := y+1;
                     IF ((SFd[1,y,x].E = Gegner)
                         AND
                         (dm_Registrierte_Figur(Gegner,y,x) = FALSE)) THEN
                     BEGIN
                          FOR i := 1 TO 4 DO
                          BEGIN
                               IF (SFd[1,SFr[1,Gegner,i].y,SFr[1,Gegner,i].x].E
                                   =
                                   0) THEN
                               BEGIN
                                    LZG := IntToStr(SFr[1,Gegner,i].y)
                                           +' '+
                                           IntToStr(SFr[1,Gegner,i].x)
                                           +' '+
                                           IntToStr(y)
                                           +' '+
                                           IntToStr(x);
                                    SFr[1,Gegner,i].x              := x;
                                    SFr[1,Gegner,i].y              := y;
                                    SFd[1,y,x].FrN                 := i;
                                    SFd[1,ZA_y(LZG),ZA_x(LZG)].FrN := 0;
                                    BREAK;
                               END;
                          END;
                          BREAK;
                     END;
               UNTIL ((y=7) AND (x=10))
          END;
          IF (ZN > 5) THEN
          BEGIN
               SFr[1,Selbst,SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN].x := ZE_x(LZS);
               SFr[1,Selbst,SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN].y := ZE_y(LZS);
               SFd[1,ZE_y(LZS),ZE_x(LZS)].FrN := SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN;
               SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN := 0;
               x := -1;
               y := -1;
               REPEAT
                     x := (x+1) MOD 11;
                     IF (x = 0) THEN y := y+1;
                     IF ((SFd[1,y,x].E = Gegner)
                         AND
                         (dm_Registrierte_Figur(Gegner,y,x) = FALSE)) THEN
                     BEGIN
                          FOR i := 1 TO 4 DO
                          BEGIN
                               IF (SFd[1,SFr[1,Gegner,i].y,SFr[1,Gegner,i].x].E
                                   =
                                   0) THEN
                               BEGIN
                                    LZG := IntToStr(SFr[1,Gegner,i].y)
                                           +' '+
                                           IntToStr(SFr[1,Gegner,i].x)
                                           +' '+
                                           IntToStr(y)
                                           +' '+
                                           IntToStr(x);
                                    SFr[1,Gegner,i].x              := x;
                                    SFr[1,Gegner,i].y              := y;
                                    SFd[1,y,x].FrN                 := i;
                                    SFd[1,ZA_y(LZG),ZA_x(LZG)].FrN := 0;
                                    BREAK;
                               END;
                          END;
                          BREAK;
                     END;
               UNTIL ((y=7) AND (x=10));
          END;
     END ELSE
     BEGIN
          IF ((ZN > 1) AND (ZN < 6)) THEN
          BEGIN
               SFr[1,Selbst,ZN-1].x           := ZE_x(LZS);
               SFr[1,Selbst,ZN-1].y           := ZE_y(LZS);
               SFd[1,ZE_y(LZS),ZE_x(LZS)].FrN := ZN-1;
               x                           := -1;
               y                           := -1;
               REPEAT
                     x := (x+1) MOD 11;
                     IF (x = 0) THEN y := y+1;
                     IF ((SFd[1,y,x].E = Gegner)
                         AND
                         (dm_Registrierte_Figur(Gegner,y,x) = FALSE)) THEN
                     BEGIN
                          LZG := IntToStr(y)+' '+IntToStr(x)+' 0 0';
                          Log('Letzter_Zug_Gegner: '+LZG);
                          SFr[1,Gegner,ZN-1].x := x;
                          SFr[1,Gegner,ZN-1].y := y;
                          SFd[1,y,x].FrN       := ZN-1;
                          BREAK;
                     END;
               UNTIL ((y=7) AND (x=10));
          END;
          IF (ZN > 5) THEN
          BEGIN
               SFr[1,Selbst,SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN].x := ZE_x(LZS);
               SFr[1,Selbst,SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN].y := ZE_y(LZS);
               SFd[1,ZE_y(LZS),ZE_x(LZS)].FrN := SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN;
               SFd[1,ZA_y(LZS),ZA_x(LZS)].FrN := 0;
               x := -1;
               y := -1;
               REPEAT
                     x := (x+1) MOD 11;
                     IF (x = 0) THEN y := y+1;
                     IF ((SFd[1,y,x].E = Gegner)
                         AND
                         (dm_Registrierte_Figur(Gegner,y,x) = FALSE)) THEN
                     BEGIN
                          FOR i := 1 TO 4 DO
                          BEGIN
                               IF (SFd[1,SFr[1,Gegner,i].y,SFr[1,Gegner,i].x].E
                                   =
                                   0) THEN
                               BEGIN
                                    LZG := IntToStr(SFr[1,Gegner,i].y)
                                           +' '+
                                           IntToStr(SFr[1,Gegner,i].x)
                                           +' '+
                                           IntToStr(y)
                                           +' '+
                                           IntToStr(x);
                                    SFr[1,Gegner,i].x              := x;
                                    SFr[1,Gegner,i].y              := y;
                                    SFd[1,y,x].FrN                 := i;
                                    SFd[1,ZA_y(LZG),ZA_x(LZG)].FrN := 0;
                                    BREAK;
                               END;
                          END;
                          BREAK;
                     END;
               UNTIL ((y=7) AND (x=10));
          END;
     END;
END;


PROCEDURE dm_Punktzaehlung;
BEGIN
     IF (ZN > 1) THEN
     BEGIN
          PG := PG+SFd[1,ZE_y(LZG),ZE_x(LZG)].P;
          PS := PS+SFd[1,ZE_y(LZS),ZE_x(LZS)].P;
          Log('Punkte Selbst: '+IntToStr(PS));
          Log('Punkte Gegner: '+IntToStr(PG));
     END;
END;


{} PROCEDURE dm_Stellungsinformationen;
VAR i : INTEGER;
BEGIN
     IF ZN > 1 THEN
     BEGIN
          IF (SN) THEN Log('- Selbst nachziehend -')
          ELSE         Log('- Selbst anziehend -');
          Log('GZD:  '+IntToStr(GZD));
          Log('GZDG: '+IntToStr(GZDG));
          Log('GZDS: '+IntToStr(GZDS));
          Log('VZZ:  '+IntToStr(VZZ));
          Log('VGZ:  '+IntToStr(VGZ));
          Log('VDZZ: '+IntToStr(VDZZ));
          FOR i:=1 TO 4 DO
          BEGIN
               Log('Selbst '+IntToStr(i)+': '+IntToStr(SFr[1,Selbst,i].y)+' '
                   +IntToStr(SFr[1,Selbst,i].x));
          END;
          FOR i:=1 TO 4 DO
          BEGIN
               Log('Gegner '+IntToStr(i)+': '+IntToStr(SFr[1,Gegner,i].y)+' '
                   +IntToStr(SFr[1,Gegner,i].x));
          END;
     END;
END;


PROCEDURE dm_Emulation;
VAR Wechselspeicher : ARRAY[1..4,1..2] OF INTEGER;
VAR i               : INTEGER;
BEGIN
     Emulator := TRUE;
     Selbst := 2;
     Gegner := 1;
     FOR i:= 1 TO 4 DO
     BEGIN
          Wechselspeicher[i,1] := SFr[1,1,i].y;
          Wechselspeicher[i,2] := SFr[1,1,i].x;
          SFr[1,1,i] := SFr[1,2,i];
          SFr[1,2,i].y := Wechselspeicher[i,1];
          SFr[1,2,i].x := Wechselspeicher[i,2];
     END;
     
END;


PROCEDURE dm_Informationsverarbeitung;
BEGIN
     Log('Jetzt ist man selbst am Zug: '+IntToStr(ZN)+ ' --------------------');
     dm_Feldzuweisungen;
     IF (ZN = 1) THEN dm_Farbenermittlung_im_Nachzug;
     IF ((ZN = 2) AND (SN = FALSE))
     THEN dm_Farbenermittlung_im_Anzug;
     dm_Figurenregistrierung;
     dm_Punktzaehlung;
     dm_Stellungsinformationen;
END;


{=== 2 Deklarationen zur KI ===================================================}


{----- 2.1 Speicher -----------------------------------------------------------}


TYPE Type_bf_Zug                     = RECORD
                                        ETZ_PD  : INTEGER;
                                        ETZ_FdD : INTEGER;
                                        FdD     : INTEGER;
                                        FrN     : INTEGER;
                                        TZ_E    : INTEGER;
                                        TZ_F    : INTEGER;
                                        TZ_R    : INTEGER;
                                        TZ_S    : INTEGER;
                                        TZ_UFr  : INTEGER;
                                        PD      : INTEGER;
                                        VS      : INTEGER;
                                        ZA_x    : INTEGER;
                                        ZA_y    : INTEGER;
                                        ZE_x    : INTEGER;
                                        ZE_y    : INTEGER;
END;
{ 'bf'   bedeutet 'brute_force'.                       }
{ 'ETZ'  bedeutet 'Extremer_tieferer_Zug'.             }
{ 'PD'   bedeutet 'Punktdifferenz'.                    }
{ 'FdD'  bedeutet 'Felddifferenz'.                     }
{ 'TZ'   bedeutet 'Tieferer_Zug'  .                    }
{ 'E'    bedeutet 'Entfernung'.                        }
{ 'F'    bedeutet 'Figur'.                             }
{ 'R'    bedeutet 'Richtung'.                          }
{ 'S'    bedeutet 'Spieler'.                           }
{ 'UFr'  bedeutet 'Umzuwandelnde_Figur'.               }
{ 'VS'   bedeutet 'Verbleibender_Spieler'.             }
TYPE Type_bf_Zugpraeferendum_Spieler = RECORD
                                        E : INTEGER;
                                        F : INTEGER;
                                        R : INTEGER;
END;
TYPE Type_Einsetzungszugpraeferendum = RECORD
                                        B : EXTENDED;
                                        x : INTEGER;
                                        y : INTEGER;
END;
{ 'B'    bedeutet 'Bewertung'.                         }
TYPE Type_Revierfeld                 = RECORD
                                        P : INTEGER;
                                        x : INTEGER;
                                        y : INTEGER;
END;
TYPE Type_Revierfigur                = RECORD
                                        Wichtigkeit : BOOLEAN;
                                        Helfer      : ARRAY[1..3] OF INTEGER;
                                        Konkurrent  : ARRAY[1..4] OF INTEGER;
                                        RFdA        : INTEGER;
                                        RPA         : INTEGER;
END;
{ 'RFd'  bedeutet 'Revierfeld'.                        }
{ 'A'    bedeutet 'Anzahl'.                            }
{ 'RP'   bedeutet 'Revierpunkte'.                      }
TYPE Type_Zugrangbewertung_Spieler   = RECORD
                                        B : EXTENDED;
                                        E : INTEGER;
                                        F : INTEGER;
                                        R : INTEGER;
END;

VAR  E2M_ZB,FB,SB,RV,VB : ARRAY[1..2,1..2,1..4,1..6,1..7] OF EXTENDED;
VAR  EFB                : ARRAY[0..7,0..10]               OF EXTENDED;
VAR  bf_FrG             : ARRAY[1..4]                     OF INTEGER;
VAR  bf_FrS             : ARRAY[1..4]                     OF INTEGER;
VAR  bf_Zug             : ARRAY[0..40]                    OF Type_bf_Zug;
VAR  bf_Fd              : ARRAY[0..7,0..10]               OF Type_Feld;
VAR  bf_RFd             : ARRAY[1..60]                    OF Type_Revierfeld;
VAR  RFd                : ARRAY[1..2,1..2,1..4,1..60]     OF Type_Revierfeld;
VAR  RFdK               : ARRAY[1..60]                    OF Type_Revierfeld;
VAR  Revierfigur        : ARRAY[1..2,1..2,1..4]           OF Type_Revierfigur;
VAR  E2M_ZRB            : ARRAY[1..2,1..2,1..84]
                           OF Type_Zugrangbewertung_Spieler;
{ 'E2M'  bedeutet 'Ebene2_Mittelspiel'.                }
{ 'ZB'   bedeutet 'Zugbewertung'.                      }
{ 'FB'   bedeutet 'Feldbewertung'.                     }
{ 'VB'   bedeutet 'Verteilungsbewertung'.              }
{ 'SB'   bedeutet 'Standortbewertung'.                 }
{ 'EFB'  bedeutet 'Einsetzungsfeldbewertung'.          }
{ 'RFd'  bedeutet 'Revierfeld'.                        }
{ 'K'    bedeutet 'Konkurrent'.                        }
{ 'E1M'  bedeutet 'Ebene1_Mittelspiel'.                }
{ 'ZRB'  bedeutet 'Zugrangbewertung'.                  }
VAR  Endspiel,Endspielbeginn                  : BOOLEAN;
VAR  ExpA,PExp,RIF                            : EXTENDED;
{ 'ExpA' bedeutet 'Exponentangleichung'.               }
{ 'PExp' bedeutet 'Punkteexponent'.                    }
{ 'RIF'  bedeutet 'Richtungsinflationsfaktor'.         }
VAR  bf_FdA,bf_FrAG,bf_FrAS,bf_Tiefe
     ,Endspielmodus,Endspielmodus_maximal     : INTEGER;
{ 'FrAG' bedeutet 'Figuranzahl_Gegner'.                }
{ 'FrAS' bedeutet 'Figuranzahl_Selbst'.                }
{ 'KL'   bedeutet 'Knotenlaenge'.                      }
VAR  bf_ZPS  : Type_bf_Zugpraeferendum_Spieler;
{ 'ZPS'  bedeutet 'Zugpraeferendum_Spieler'.           }
VAR  EZP     : Type_Einsetzungszugpraeferendum;
{ 'EZP'  bedeutet 'Einsetzungszugpraeferendum'.        }
VAR  E3M_ZPS : Type_Zugrangbewertung_Spieler;
{ 'ZP'   bedeutet 'Zugpraeferendum'.                   }

{---}
TYPE Type_ALB = RECORD
                 D  : EXTENDED;
                 F1 : INTEGER;
                 R1 : INTEGER;
                 E1 : INTEGER;
                 B1 : EXTENDED;
                 F2 : INTEGER;
                 R2 : INTEGER;
                 E2 : INTEGER;
                 B2 : EXTENDED;
END;

VAR  NFB,NSB : ARRAY[1..2,1..2,1..4,1..6,1..7] OF EXTENDED;
VAR  ALB     : ARRAY[1..4,1..6,1..7]           OF Type_ALB;
VAR  ALR     : ARRAY[1..84]                    OF Type_ALB;
VAR  ASB     : ARRAY[1..84]                    OF Type_Zugrangbewertung_Spieler;
{---}


{----- 2.2 Prozeduren und Funktionen ------------------------------------------}


FUNCTION PA1(Punkte : INTEGER) : EXTENDED;
{ 'PA'   bedeutet 'Punktangleichung'.                  }
BEGIN
     PA1 := POWER(Punkte/2,PExp+ExpA);
END;


FUNCTION PA2(Punkte : INTEGER) : EXTENDED;
BEGIN
     PA2 := POWER(Punkte/2,PExp);
END;


FUNCTION PA3(Punkte : INTEGER) : EXTENDED;
BEGIN
     PA3 := POWER(Punkte/2,PExp-ExpA);
END;


PROCEDURE dm_Zweite_Definitionen;
BEGIN
     Endspiel              := FALSE;
     Endspielbeginn        := TRUE;
     Endspielmodus_maximal := 3;
     ExpA := 0.6;
     PExp := 2;
     RIF  := 0.5;
END;


PROCEDURE dm_Initialisierung;
VAR i,T_E,T_F,T_R,T_Spieler,T_Tiefe,x,y : INTEGER;
{ 'T'    bedeutet 'Tempor�r'.                          }
{} VAR T_RANG : INTEGER;
BEGIN
{---}
     FOR T_Rang := 1 TO 84 DO
     BEGIN
          ALR[T_Rang].D := -1000000000000;
          ASB[T_Rang].B := -10;
          ASB[T_Rang].F := -10;
          ASB[T_Rang].R := -10;
          ASB[T_Rang].E := -10;
     END;
{---}
     E3M_ZPS.B       := -1000000000000;
     EZP.B           := -1000000000000;
     EZP.y           := -10;
     EZP.x           := -10;
     FOR y    := 0 TO 7  DO
     FOR x    := 0 TO 10 DO
     BEGIN
          EFB[y,x]   := -1000000000000;
     END;
     FOR T_Tiefe   := 1 TO 2 DO
     FOR T_Spieler := 1 TO 2 DO
     FOR T_F       := 1 TO 4 DO
     FOR T_R       := 1 TO 6 DO
     FOR T_E       := 1 TO 7 DO
     BEGIN
          FB[T_Tiefe,T_Spieler,T_F,T_R,T_E] :=             0;
          SB[T_Tiefe,T_Spieler,T_F,T_R,T_E] := -500000000000;
          VB[T_Tiefe,T_Spieler,T_F,T_R,T_E] :=          -100;
          RV[T_Tiefe,T_Spieler,T_F,T_R,T_E] :=          -100;
     END;
     IF ((ZN = 1) OR (Endspiel = TRUE)) THEN
     BEGIN
          bf_Zug[0].ETZ_PD  := -1000;
          bf_Zug[0].ETZ_FdD := -1000;
          bf_Zug[0].FdD     := 0;
          bf_Zug[0].FrN     := -10;
          bf_Zug[0].TZ_E    := -10;
          bf_Zug[0].TZ_F    := -10;
          bf_Zug[0].TZ_R    := -10;
          bf_Zug[0].TZ_UFr  := -10;
          bf_Zug[0].PD      := 0;
          bf_Zug[0].VS      := 0;
          bf_Zug[0].ZA_x    := -10;
          bf_Zug[0].ZA_y    := -10;
          bf_Zug[0].ZE_x    := -10;
          bf_Zug[0].ZE_y    := -10;
          FOR i := 1 TO 40 DO
          BEGIN
               bf_Zug[i] := bf_Zug[0];
          END;
          bf_ZPS.F := -10;
          bf_ZPS.R := -10;
          bf_ZPS.E := -10;
     END;
END;



FUNCTION dm_Feldabstand(y_1,x_1,y_2,x_2 : EXTENDED) : EXTENDED;
BEGIN
     x_1 := x_1-(3-y_1*0.5);
     x_2 := x_2-(3-y_2*0.5);
     dm_Feldabstand := SQRT(POWER(y_2-y_1,2)+POWER(x_2-x_1,2));
END;


FUNCTION dm_Figurenabstand(FrN_1,FrN_2 : INTEGER) : EXTENDED;
VAR x_1,x_2,y_1,y_2 : EXTENDED;
BEGIN
     y_1 := SFr[1,Selbst,FrN_1].y;
     x_1 := SFr[1,Selbst,FrN_1].x-(3-y_1*0.5);
     y_2 := SFr[1,Selbst,FrN_2].y;
     x_2 := SFr[1,Selbst,FrN_2].x-(3-y_2*0.5);
     dm_Figurenabstand := SQRT(POWER(y_2-y_1,2)+POWER(x_2-x_1,2));
END;


PROCEDURE dm_Einsetzungsfeldbewertung_ET3;
{ 'ET3'  bedeutet 'Eigene_Tiefe_3'.                    }
VAR T_RB2,T_RB3              : ARRAY[1..6]       OF EXTENDED;
{ 'RB'   bedeutet 'Richtungsbewertung'.                }
VAR T_SFd                    : ARRAY[0..7,0..10] OF Type_Feld;
VAR T_B2                     : EXTENDED;
{ 'B'    bedeutet 'Bewertung'.                         }
VAR i,j,T_BR2,T_BR3,T_D3,T_E2,T_E3,T_R2,T_R3,T_RM2,T_RM3
    ,x_1,x_2,x_3,y_1,y_2,y_3 : INTEGER;
{ 'BR'   bedeutet 'Beste_Richtung'.                    }
{ 'D'    bedeutet 'Drehung'.                           }
{ 'RM'   bedeutet 'Richtungsm�glichkeiten'.            }
BEGIN
     FOR y_1 := 0 TO 7  DO
     FOR x_1 := 0 TO 10 DO
      T_SFd[y_1,x_1].E := SFd[1,y_1,x_1].E;
     FOR y_1 := 0 TO 7  DO
     FOR x_1 := 0 TO 10 DO
      IF ((SFd[1,y_1,x_1].P = 1) AND (SFd[1,y_1,x_1].E = 0)) THEN
      Begin
           T_SFd[y_1,x_1].E := 10;
           FOR T_R2 := 1 TO 6 DO T_RB2[T_R2] := -1000000000000;
           T_RM2    := 0;
           FOR T_R2 := 1 TO 6 DO
           BEGIN
                FOR T_E2 := 1 TO 7 DO
                BEGIN
                     y_2 := y_1+RS_y(T_R2,T_E2);
                     x_2 := x_1+RS_x(T_R2,T_E2);
{ |--> }
 IF (EFd(y_2,x_2) = FALSE) THEN BREAK;
 IF ((SFd[1,y_2,x_2].S = Nicht_vorhanden) OR (T_SFd[y_2,x_2].E > 0)) THEN BREAK;
 FOR T_D3 := 1 TO 6 DO T_RB3[T_D3] := -1000000000000;
 T_RM3    := 0;
 FOR T_D3 := 1 TO 5 DO
 BEGIN
      T_R3  := ((T_R2+2+T_D3) MOD 6)+1;
      FOR T_E3 := 1 TO 7 DO
      BEGIN
           y_3 := y_2+RS_y(T_R3,T_E3);
           x_3 := x_2+RS_x(T_R3,T_E3);
{ |--> }
 IF (EFd(y_3,x_3) = FALSE) THEN BREAK;
 IF ((SFd[1,y_3,x_3].S = Nicht_vorhanden) OR (T_SFd[y_3,x_3].E > 0)) THEN BREAK;
 IF (PA3(SFd[1,y_3,x_3].P) > T_RB3[T_D3])
 THEN T_RB3[T_D3] := PA3(SFd[1,y_3,x_3].P);
{ |<-- }
      END;
      IF (T_E3-1 > 0) THEN T_RM3 := T_RM3+1;
 END;
 T_R3 := ((T_R2+2) MOD 6)+1;
 IF (T_E2 > 1) THEN
 BEGIN
      T_RM3 := T_RM3+1;
      FOR T_E3 := 1 TO (T_E2-1) DO
      BEGIN
           y_3 := y_2+RS_y(T_R3,T_E3);
           x_3 := x_2+RS_x(T_R3,T_E3);
{ |--> }
 IF (EFd(y_3,x_3) = FALSE) THEN BREAK;
 IF ((SFd[1,y_3,x_3].S = Nicht_vorhanden) OR (T_SFd[y_3,x_3].E > 0)) THEN BREAK;
 IF (PA3(SFd[1,y_3,x_3].P) > T_RB3[6]) THEN T_RB3[6] := PA3(SFd[1,y_3,x_3].P);
{ |<-- }
      END;
 END;
 IF (T_RM3 = 0) THEN T_B2 := PA2(SFd[1,y_2,x_2].P);
 IF (T_RM3 > 0) THEN
 BEGIN
      T_BR3 := 1;
      FOR T_D3 := 1 TO 6 DO IF (T_RB3[T_D3] > T_RB3[T_BR3]) THEN T_BR3 := T_D3;
      T_B2         := T_RB3[T_BR3];
      T_RB3[T_BR3] := -1000000000000;
      FOR j := 0 TO 4 DO
      BEGIN
           IF (T_RB3[1]+T_RB3[2]+T_RB3[3]+T_RB3[4]+T_RB3[5]+T_RB3[6]
               =
               -6000000000000)
           THEN BREAK;
           FOR T_D3 := 1 TO 6 DO IF (T_RB3[T_D3] > T_RB3[T_BR3])
                                 THEN T_BR3 := T_D3;
           T_B2         := T_B2+T_RB3[T_BR3]*POWER(RIF,j);
           T_RB3[T_BR3] := -1000000000000;
      END;
      T_B2 := T_B2*PA2(SFd[1,y_2,x_2].P);
      IF (T_RM3 = 1) THEN T_B2 := T_B2*100
                     ELSE T_B2 := T_B2*10000;
 END;
{ |<-- }
                     IF (T_B2 > T_RB2[T_R2]) THEN T_RB2[T_R2] := T_B2;
                END;
                IF (T_E2-1 > 0) THEN T_RM2 := T_RM2+1;
           END;
           IF (T_RM2 = 0)
           THEN EFB[y_1,x_1] := PA1(SFd[1,y_1,x_1].P);
           IF (T_RM2 > 0) THEN
           BEGIN
                T_BR2 := 1;
                FOR T_R2 := 1 TO 6 DO
                 IF (T_RB2[T_R2] > T_RB2[T_BR2]) THEN T_BR2 := T_R2;
                EFB[y_1,x_1] := T_RB2[T_BR2];
                T_RB2[T_BR2]          := -1000000000000;
                FOR i := 0 TO 4 DO
                BEGIN
                     IF (T_RB2[1]+T_RB2[2]+T_RB2[3]+T_RB2[4]+T_RB2[5]+T_RB2[6]
                         =
                         -6000000000000) THEN BREAK;
                     FOR T_R2 := 1 TO 6 DO
                      IF (T_RB2[T_R2] > T_RB2[T_BR2])
                      THEN T_BR2 := T_R2;
                     EFB[y_1,x_1] := EFB[y_1,x_1]+T_RB2[T_BR2]*POWER(RIF,i);
                     T_RB2[T_BR2] := -1000000000000;
                END;
                EFB[y_1,x_1] := EFB[y_1,x_1]*PA1(SFd[1,y_1,x_1].P);
                IF ((T_RM2 = 1) OR (T_RM2 = 2) OR (T_RM2 = 3))
                THEN EFB[y_1,x_1] := EFB[y_1,x_1]*100
                ELSE EFB[y_1,x_1] := EFB[y_1,x_1]*10000;
           END;
{ Aber hier neu: }
           SFr[1,Selbst,ZN].y := y_1;
           SFr[1,Selbst,ZN].x := x_1;
           IF (ZN = 3)
           THEN EFB[y_1,x_1] := EFB[y_1,x_1]
                                +( dm_Figurenabstand(1,3)
                                  +dm_Figurenabstand(2,3))/2*75000000;
           IF (ZN = 4)
           THEN EFB[y_1,x_1] := EFB[y_1,x_1]
                                +( dm_Figurenabstand(1,4)
                                  +dm_Figurenabstand(2,4)
                                  +dm_Figurenabstand(3,4))/3*75000000;
{---}
           IF (ZN = 3) THEN Log('('+IntToStr(y_1)+','+IntToStr(x_1)+') :'
                                +' Figurenabstand(1,3): '
                                +FloatToStr(dm_Figurenabstand(1,3))
                                +' Figurenabstand(2,3): '
                                +FloatToStr(dm_Figurenabstand(2,3)));
           IF (ZN = 4) THEN Log('('+IntToStr(y_1)+','+IntToStr(x_1)+') :'
                                +' Figurenabstand(1,4): '
                                +FloatToStr(dm_Figurenabstand(1,4))
                                +' Figurenabstand(2,4): '
                                +FloatToStr(dm_Figurenabstand(2,4))
                                +' Figurenabstand(3,4): '
                                +FloatToStr(dm_Figurenabstand(3,4)));
{---}
{ -Bis hier neu. }
           IF (EFB[y_1,x_1] > EZP.B) THEN
           BEGIN
                EZP.B := EFB[y_1,x_1];
                EZP.y := y_1;
                EZP.x := x_1;
           END;
           T_SFd[y_1,x_1].E := SFd[1,y_1,x_1].E;
      END;
END;


PROCEDURE dm_Feldbewertung_ET3(Tiefe,Spieler : INTEGER);
VAR T_RB2,T_RB3                          : ARRAY [1..6] OF EXTENDED;
VAR T_B2,T_FB                            : EXTENDED;
VAR i,j,T_BR2,T_BR3,T_D2,T_D3,T_E1,T_E2,T_E3,T_F,T_R1,T_R2,T_R3
    ,T_RM2,T_RM3,x_1,x_2,x_3,y_1,y_2,y_3 : INTEGER;
BEGIN
     FOR T_F   := 1 TO 4 DO
     FOR T_R1  := 1 TO 6 DO
     FOR T_E1  := 1 TO 7 DO FB[Tiefe,Spieler,T_F,T_R1,T_E1] := 0;
     FOR T_F  := 1 TO 4 DO
     BEGIN
          IF (((Tiefe = 1)
               AND
               (Revierfigur[1,Selbst,T_F].Wichtigkeit = FALSE))
              OR
              ((Tiefe = 2)
               AND
               (Revierfigur[1,Gegner,T_F].Wichtigkeit = FALSE)
               AND
               (Revierfigur[2,Gegner,T_F].Wichtigkeit = FALSE)))
          THEN CONTINUE;
          FOR T_R1 := 1 TO 6 DO
          FOR T_E1 := 1 TO 7 DO
          BEGIN
               y_1 := SFr[Tiefe,Spieler,T_F].y+RS_y(T_R1,T_E1);
               x_1 := SFr[Tiefe,Spieler,T_F].x+RS_x(T_R1,T_E1);
               IF (EFd(y_1,x_1) = FALSE) THEN BREAK;
               IF ((SFd[Tiefe,y_1,x_1].S = Nicht_vorhanden)
                   OR
              (SFd[Tiefe,y_1,x_1].E > 0)) THEN BREAK;
               FOR T_D2 := 1 TO 6 DO T_RB2[T_D2] := -1000000000000;
               T_RM2    := 0;
               FOR T_D2 := 1 TO 6 DO
               BEGIN
                    T_R2  := ((T_R1+2+T_D2) MOD 6)+1;
                    FOR T_E2 := 1 TO 7 DO
                    BEGIN
                         y_2 := y_1+RS_y(T_R2,T_E2);
                         x_2 := x_1+RS_x(T_R2,T_E2);
{ |--> }
 IF (EFd(y_2,x_2) = FALSE) THEN BREAK;
 IF ((SFd[Tiefe,y_2,x_2].S = Nicht_vorhanden) OR (SFd[Tiefe,y_2,x_2].E > 0))
 THEN BREAK;
 FOR T_D3 := 1 TO 6 DO T_RB3[T_D3] := -1000000000000;
 T_RM3    := 0;
 FOR T_D3 := 1 TO 5 DO
 BEGIN
      T_R3  := ((T_R2+2+T_D3) MOD 6)+1;
      FOR T_E3 := 1 TO 7 DO
      BEGIN
           y_3 := y_2+RS_y(T_R3,T_E3);
           x_3 := x_2+RS_x(T_R3,T_E3);
{ |--> }
 IF (EFd(y_3,x_3) = FALSE) THEN BREAK;
 IF ((SFd[Tiefe,y_3,x_3].S = Nicht_vorhanden) OR (SFd[Tiefe,y_3,x_3].E > 0))
 THEN BREAK;
 IF (PA3(SFd[Tiefe,y_3,x_3].P) > T_RB3[T_D3])
 THEN T_RB3[T_D3] := PA3(SFd[Tiefe,y_3,x_3].P);
{ |<-- }
      END;
      IF (T_E3-1 > 0) THEN T_RM3 := T_RM3+1;
 END;
 T_R3 := ((T_R2+2) MOD 6)+1;
 IF (T_E2 > 1) THEN
 BEGIN
      T_RM3 := T_RM3+1;
      FOR T_E3 := 1 TO (T_E2-1) DO
      BEGIN
           y_3 := y_2+RS_y(T_R3,T_E3);
           x_3 := x_2+RS_x(T_R3,T_E3);
{ |--> }
 IF (EFd(y_3,x_3) = FALSE) THEN BREAK;
 IF ((SFd[Tiefe,y_3,x_3].S = Nicht_vorhanden) OR (SFd[Tiefe,y_3,x_3].E > 0))
 THEN BREAK;
 IF (PA3(SFd[Tiefe,y_3,x_3].P) > T_RB3[6])
 THEN T_RB3[6] := PA3(SFd[Tiefe,y_3,x_3].P);
{ |<-- }
      END;
 END;
 IF (T_RM3 = 0) THEN T_B2 := PA2(SFd[Tiefe,y_2,x_2].P);
 IF (T_RM3 > 0) THEN
 BEGIN
      T_BR3 := 1;
      FOR T_D3 := 1 TO 6 DO IF (T_RB3[T_D3] > T_RB3[T_BR3]) THEN T_BR3 := T_D3;
      T_B2         := T_RB3[T_BR3];
      T_RB3[T_BR3] := -1000000000000;
      FOR j := 0 TO 4 DO
      BEGIN
           IF (T_RB3[1]+T_RB3[2]+T_RB3[3]+T_RB3[4]+T_RB3[5]+T_RB3[6]
               =
               -6000000000000)
           THEN BREAK;
           FOR T_D3 := 1 TO 6 DO IF (T_RB3[T_D3] > T_RB3[T_BR3])
                                 THEN T_BR3 := T_D3;
           T_B2         := T_B2+T_RB3[T_BR3]*POWER(RIF,j);
           T_RB3[T_BR3] := -1000000000000;
      END;
      T_B2 := T_B2*PA2(SFd[Tiefe,y_2,x_2].P);
      IF (T_RM3 = 1) THEN T_B2 := T_B2*100 ELSE T_B2 := T_B2*10000;
 END;
{ |<-- }
                         IF (T_B2 > T_RB2[T_D2]) THEN T_RB2[T_D2] := T_B2;
                    END;
                    IF (T_E2-1 > 0) THEN T_RM2 := T_RM2+1;
               END;
               IF (T_RM2 = 0)
               THEN T_FB := PA1(SFd[Tiefe,y_1,x_1].P);
               IF (T_RM2 > 0) THEN
               BEGIN
                    T_BR2 := 1;
                    FOR T_D2 := 1 TO 6 DO IF (T_RB2[T_D2] > T_RB2[T_BR2])
                                          THEN T_BR2 := T_D2;
                    T_FB := T_RB2[T_BR2];
                    T_RB2[T_BR2]          := -1000000000000;
                    FOR i := 0 TO 4 DO
                    BEGIN
                         IF (T_RB2[1]+T_RB2[2]+T_RB2[3]
                             +T_RB2[4]+T_RB2[5]+T_RB2[6]
                             =
                             -6000000000000) THEN BREAK;
                         FOR T_D2 := 1 TO 6 DO
                         IF (T_RB2[T_D2] > T_RB2[T_BR2]) THEN T_BR2 := T_D2;
                         T_FB := T_FB+T_RB2[T_BR2]*POWER(RIF,i);
                         T_RB2[T_BR2] := -1000000000000;
                    END;
                    T_FB := T_FB*PA1(SFd[Tiefe,y_1,x_1].P);
                    IF (T_RM2 = 1) THEN T_FB := T_FB*100
                                   ELSE T_FB := T_FB*10000;
               END;
               FB[Tiefe,Spieler,T_F,T_R1,T_E1] := T_FB;
          END;
     END;
END;


PROCEDURE dm_Standortbewertung(Tiefe,Spieler : INTEGER);
VAR T_RB1,T_RB2           : ARRAY [1..6]      OF EXTENDED;
VAR SBFd                  : ARRAY[0..7,0..10] OF INTEGER;
{ 'SBFd' bedeutet 'Standortbewertungsfeld'.            }
VAR SBFr                  : ARRAY[1..2,1..4]  OF Type_Figur;
{ 'SBFr' bedeutet 'Standortbewertungsfigur'.           }
VAR T_B1,T_B2,T_SB        : EXTENDED;
VAR i,T_BR1,T_BR2,T_E,T_E1,T_E2,T_F,T_F1,T_R,T_R1,T_R2,T_RM1,T_RM2,T_S,T_S1
     ,x,x_1,x_2,y,y_1,y_2 : INTEGER;
{} VAR ASBE,j : INTEGER;
BEGIN
{}   ASBE := 0;
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO SBFd[y,x] := SFd[Tiefe,y,x].S;
     FOR T_S := 1 TO 2 DO
     FOR T_F := 1 TO 4 DO
     BEGIN
          SBFr[T_S,T_F]                         := SFR[Tiefe,T_S,T_F];
          SBFd[SBFr[T_S,T_F].y,SBFr[T_S,T_F].x] := 0;
     END;
     FOR T_F := 1 TO 4 DO
     FOR T_R := 1 TO 6 DO
     FOR T_E := 1 TO 7 DO SB[Tiefe,Spieler,T_F,T_R,T_E] := -500000000000;
     FOR T_F  := 1 TO 4 DO
     BEGIN
{ |--> }
     IF (((Tiefe = 1)
          AND
          (Revierfigur[1,Selbst,T_F].Wichtigkeit = FALSE))
         OR
         ((Tiefe = 2)
          AND
          (Revierfigur[1,Gegner,T_F].Wichtigkeit = FALSE)
          AND
          (Revierfigur[2,Gegner,T_F].Wichtigkeit = FALSE))) THEN CONTINUE;
     FOR T_R := 1 TO 6 DO
     FOR T_E := 1 TO 7 DO
     BEGIN
          y := SFr[Tiefe,Spieler,T_F].y+RS_y(T_R,T_E);
          x := SFr[Tiefe,Spieler,T_F].x+RS_x(T_R,T_E);
          IF (EFd(y,x)  = FALSE) THEN BREAK;
          IF (SBFd[y,x] = 0)     THEN BREAK;
          SBFr[Spieler,T_F].y := y;
          SBFr[Spieler,T_F].x := x;
          SBFd[y,x]          := 0;
          T_SB               := 0;
          FOR T_S1 := 1 TO 2 DO
          FOR T_F1 := 1 TO 4 DO
          BEGIN
               T_RM1 := 0;
               FOR T_R1 := 1 TO 6 DO
               BEGIN
                    T_RB1[T_R1] := -1000000000000;
                    FOR T_E1 := 1 TO 7 DO
                    BEGIN
                         y_1 := SBFr[T_S1,T_F1].y+RS_y(T_R1,T_E1);
                         x_1 := SBFr[T_S1,T_F1].x+RS_x(T_R1,T_E1);
                         IF (EFd(y_1,x_1) = FALSE) THEN BREAK;
                         IF (SBFd[y_1,x_1] = 0)    THEN BREAK;
                         T_RM2 := 0;
                         FOR T_R2 := 1 TO 6 DO
                         BEGIN
                              T_RB2[T_R2] := -1000000000000;
                              FOR T_E2 := 1 TO 7 DO
                              BEGIN
                                   y_2 := y_1+RS_y(T_R2,T_E2);
                                   x_2 := x_1+RS_x(T_R2,T_E2);
                                   IF (EFd(y_2,x_2) = FALSE) THEN BREAK;
                                   IF (SBFd[y_2,x_2] = 0)    THEN BREAK;
                                   IF (PA2(SFd[Tiefe,y_2,x_2].P) > T_RB2[T_R2])
                                   THEN T_RB2[T_R2]
                                         := PA2(SFd[Tiefe,y_2,x_2].P);
                              END;
                              IF (T_E2-1 > 0) THEN T_RM2 := T_RM2+1;
                         END;
                         IF (T_RM2 = 0) THEN T_B2 := PA1(SFd[Tiefe,y_1,x_1].P);
                         IF (T_RM2 > 0) THEN
                         BEGIN
                              T_BR2        := 1;
                              FOR T_R2     := 1 TO 6 DO
                               IF (T_RB2[T_R2] > T_RB2[1])
                               THEN T_BR2 := T_R2;
                              T_B2         := T_RB2[T_BR2]/RIF;
                              T_RB2[T_BR2] := -1000000000000;
                              FOR i := 0 TO 4 DO
                              BEGIN
                                   IF (T_RB2[1]+T_RB2[2]+T_RB2[3]
                                       +T_RB2[4]+T_RB2[5]+T_RB2[6]
                                       =
                                       -6000000000000) THEN BREAK;
                                   FOR T_R2 := 1 TO 6 DO
                                    IF (T_RB2[T_R2] > T_RB2[T_BR2])
                                    THEN T_BR2 := T_R2;
                                   T_B2 := T_B2+T_RB2[T_BR2]*POWER(RIF,i);
                                   T_RB2[T_BR2] := -1000000000000;
                              END;
                              T_B2 := T_B2*PA1(SFd[Tiefe,y_1,x_1].P);
                              IF (T_RM2 = 1) THEN T_B2 := T_B2*   10
                                             ELSE T_B2 := T_B2*10000;
                         END;
                         IF (T_B2 > T_RB1[T_R1]) THEN T_RB1[T_R1] := T_B2;
                    END;
                    IF (T_E1-1 > 0) THEN T_RM1 := T_RM1+1;
               END;
               T_B1 := 0;
               IF (T_RM1 = 0)
               THEN IF (T_S1 = Gegner) THEN T_B1 := -31000000000
                    ELSE IF (Tiefe = 1) THEN T_B1 := -37000000000
                         ELSE T_B1 := -62000000000;
               IF (T_RM1 > 0) THEN
               BEGIN
                    T_BR1        := 1;
                    FOR T_R1 := 1 TO 6 DO IF (T_RB1[T_R1] > T_RB1[1])
                                          THEN T_BR1 := T_R1;
                    T_B1 := T_RB1[T_BR1]/RIF/RIF;
                    T_RB1[T_BR1] := -1000000000000;
                    FOR i := 0 TO 4 DO
                    BEGIN
                         IF (T_RB1[1]+T_RB1[2]+T_RB1[3]
                             +T_RB1[4]+T_RB1[5]+T_RB1[6]
                             =
                             -6000000000000) THEN BREAK;
                         FOR T_R1 := 1 TO 6 DO IF (T_RB1[T_R1] > T_RB1[T_BR1])
                                               THEN T_BR1 := T_R1;
                         T_B1 := T_B1+T_RB1[T_BR1]*POWER(RIF,i);
                         T_RB1[T_BR1] := -1000000000000;
                    END;
                    IF (T_RM1 = 1) THEN
                    BEGIN
                         T_B1 := T_B1*  100-15000000000;
                    END
                    ELSE T_B1 := T_B1*10000;
               END;
               IF (T_S1 = Spieler)
               THEN IF (T_F1 = T_F) THEN T_SB := T_SB+T_B1
                                    ELSE T_SB := T_SB+T_B1
               ELSE T_SB := T_SB-T_B1*1.5;
          END;
          SBFd[y,x]         := 1;
          SBFr[Spieler,T_F] := SFR[Tiefe,Spieler,T_F];
          SB[Tiefe,Spieler,T_F,T_R,T_E] := T_SB;
{---}
          IF (Tiefe = 1) THEN
          BEGIN
               ASBE := ASBE+1;
               FOR i := 1 TO ASBE DO
               IF (3+T_SB/10000000000 > ASB[i].B) THEN
               BEGIN
                    FOR j := ASBE-1 DOWNTO i DO
                    BEGIN
                         ASB[j+1] := ASB[j];
                    END;
                    ASB[i].B := 3+T_SB/10000000000;
                    ASB[i].F := T_F;
                    ASB[i].R := T_R;
                    ASB[i].E := T_E;
                    BREAK;
               END;
          END;
{---}
     END;
{ |<-- }
     END;
{---}
     IF (Tiefe = 1) THEN FOR i := ASBE+1 TO 84 DO
                         BEGIN
                              ASB[i].B := -10;
                              ASB[i].F := -10;
                              ASB[i].R := -10;
                              ASB[i].E := -10;
                         END;
{---}
END;


{ 1253 - Hier neu: }
TYPE Type_WEFd = RECORD
                  E : INTEGER;
                  I : EXTENDED;
                  S : INTEGER;
END;
{ 'WEFd' bedeutet 'Wellenerreichbarkeitsfeld'.         }
{ 'I'    bedeutet 'Integration'.                       }
TYPE Type_WM   = ARRAY[0..7,0..10] OF Type_WEFd;
{ 'WM'   bedeutet 'Wellenmatrix'.                      }
VAR RE                : ARRAY[1..6,1..7]                OF BOOLEAN;
VAR WE                : ARRAY[1..21]                    OF BOOLEAN;
VAR AVFr,T_VFr        : ARRAY[1..4]                     OF Type_Figur;
VAR U_Fd,V_Fd         : ARRAY[1..56]                    OF Type_Figur;
VAR AVFd,T_VFd,WEFd   : ARRAY[0..7,0..10]               OF Type_WEFd;
{ 'RE'   bedeutet 'Reihenerreichbarkeit'.              }
{ 'WE'   bedeutet 'Wellenerreichbarkeit'.              }
{ 'U_Fd' bedeutet 'Unvollwertiges_Feld'.               }
{ 'V_Fd' bedeutet 'Vollwertiges_Feld'.                 }
{ 'AVFd' bedeutet 'Anfangsverteilungsfeld'.            }
{ 'VFd'  bedeutet 'Verteilungsfeld'.                   }
VAR AVK,AVS,T_VK,T_VS : EXTENDED;
{ 'AVK'  bedeutet 'Anfangsverteilung_Konkurrent'.      }
{ 'AVS'  bedeutet 'Anfangsverteilung_Selbst'.          }
{ 'VK'   bedeutet 'Verteilung_Konkurrent'.             }
{ 'VS'   bedeutet 'Verteilung_Selbst'.                 }


FUNCTION dm_SFA_x(Zentrum_x,Sektor,S : INTEGER) : INTEGER;
{ 'SFA'  bedeutet 'Sektorfeld_absolut'.                }
{ 'S'    bedeutet 'Sektorfeld'.                        }
VAR R2 : INTEGER;
BEGIN
     R2 := Sektor+1;
     IF (R2 = 7) THEN R2 := 1;
     IF (S =  1) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,1)+RS_x(R2,1);
     IF (S =  2) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,1)+RS_x(R2,2);
     IF (S =  3) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,2)+RS_x(R2,1);
     IF (S =  4) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,1)+RS_x(R2,3);
     IF (S =  5) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,2)+RS_x(R2,2);
     IF (S =  6) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,3)+RS_x(R2,1);
     IF (S =  7) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,1)+RS_x(R2,4);
     IF (S =  8) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,2)+RS_x(R2,3);
     IF (S =  9) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,3)+RS_x(R2,2);
     IF (S = 10) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,4)+RS_x(R2,1);
     IF (S = 11) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,1)+RS_x(R2,5);
     IF (S = 12) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,2)+RS_x(R2,4);
     IF (S = 13) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,3)+RS_x(R2,3);
     IF (S = 14) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,4)+RS_x(R2,2);
     IF (S = 15) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,5)+RS_x(R2,1);
     IF (S = 16) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,1)+RS_x(R2,6);
     IF (S = 17) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,2)+RS_x(R2,5);
     IF (S = 18) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,3)+RS_x(R2,4);
     IF (S = 19) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,4)+RS_x(R2,3);
     IF (S = 20) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,5)+RS_x(R2,2);
     IF (S = 21) THEN dm_SFA_x := Zentrum_x+RS_x(Sektor,6)+RS_x(R2,1);
END;


FUNCTION dm_SFA_y(Zentrum_y,Sektor,S : INTEGER) : INTEGER;
{ 'SFA'  bedeutet 'Sektorfeld_absolut'.                }
{ 'S'    bedeutet 'Sektorfeld'.                        }
VAR R2 : INTEGER;
BEGIN
     R2 := Sektor+1;
     IF (R2 = 7) THEN R2 := 1;
     IF (S =  1) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,1)+RS_y(R2,1);
     IF (S =  2) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,1)+RS_y(R2,2);
     IF (S =  3) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,2)+RS_y(R2,1);
     IF (S =  4) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,1)+RS_y(R2,3);
     IF (S =  5) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,2)+RS_y(R2,2);
     IF (S =  6) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,3)+RS_y(R2,1);
     IF (S =  7) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,1)+RS_y(R2,4);
     IF (S =  8) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,2)+RS_y(R2,3);
     IF (S =  9) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,3)+RS_y(R2,2);
     IF (S = 10) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,4)+RS_y(R2,1);
     IF (S = 11) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,1)+RS_y(R2,5);
     IF (S = 12) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,2)+RS_y(R2,4);
     IF (S = 13) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,3)+RS_y(R2,3);
     IF (S = 14) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,4)+RS_y(R2,2);
     IF (S = 15) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,5)+RS_y(R2,1);
     IF (S = 16) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,1)+RS_y(R2,6);
     IF (S = 17) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,2)+RS_y(R2,5);
     IF (S = 18) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,3)+RS_y(R2,4);
     IF (S = 19) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,4)+RS_y(R2,3);
     IF (S = 20) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,5)+RS_y(R2,2);
     IF (S = 21) THEN dm_SFA_y := Zentrum_y+RS_y(Sektor,6)+RS_y(R2,1);
END;


FUNCTION dm_WE(Sektor,Sektorfeld : INTEGER) : BOOLEAN;
VAR R_1,R_2 : INTEGER;
BEGIN
     R_1 := Sektor;
     R_2 := Sektor+1;
     IF (R_2 = 7) THEN R_2 := 1;
     dm_WE := FALSE;
{ |--> <--| }
     IF (Sektorfeld =  1) THEN IF ((RE[R_1,1]) OR (RE[R_2,1]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  3) THEN IF ((RE[R_1,1])
                                   AND
                                   (WE[1]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  6) THEN IF ((RE[R_1,1])
                                   AND
                                   ((WE[1]) AND (RE[R_1,2]))
                                   AND
                                   (WE[3]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 10) THEN IF ((RE[R_1,1])
                                   AND
                                   ((WE[1]) AND (RE[R_1,2]))
                                   AND
                                   ((WE[3]) AND (RE[R_1,3]))
                                   AND
                                   (WE[6]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 15) THEN IF ((RE[R_1,1])
                                   AND
                                   ((WE[1]) AND (RE[R_1,2]))
                                   AND
                                   ((WE[3]) AND (RE[R_1,3]))
                                   AND
                                   ((WE[6]) AND (RE[R_1,4]))
                                   AND
                                   (WE[10]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 21) THEN IF ((RE[R_1,1])
                                   AND
                                   ((WE[1]) AND (RE[R_1,2]))
                                   AND
                                   ((WE[3]) AND (RE[R_1,3]))
                                   AND
                                   ((WE[6]) AND (RE[R_1,4]))
                                   AND
                                   (WE[10])
                                   AND
                                   (WE[15]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 20) THEN IF ((RE[R_1,1])
                                   AND
                                   ((WE[1]) AND (RE[R_1,2]))
                                   AND
                                   (WE[3])
                                   AND
                                   (WE[6])
                                   AND
                                   ((WE[9]) AND (WE[10]))
                                   AND
                                   (WE[14]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 14) THEN IF ((RE[R_1,1])
                                   AND
                                   (WE[1])
                                   AND
                                   (WE[3])
                                   AND
                                   (WE[6])
                                   AND
                                   (WE[9]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 19) THEN IF ((RE[R_1,1])
                                   AND
                                   (WE[1])
                                   AND
                                   (WE[3])
                                   AND
                                   (WE[5])
                                   AND
                                   (WE[9])
                                   AND
                                   (WE[13]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  9) THEN IF ((RE[R_1,1])
                                   AND
                                   (WE[1])
                                   AND
                                   (WE[3])
                                   AND
                                   (WE[5]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 13) THEN IF ((WE[1])
                                   AND
                                   (WE[5])
                                   AND
                                   (((RE[R_2,1])
                                     AND
                                     (WE[2])
                                     AND
                                     (WE[8]))
                                    OR
                                    ((RE[R_1,1])
                                     AND
                                     (WE[3])
                                     AND
                                     (WE[9]))))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 18) THEN IF ((RE[R_2,1])
                                   AND
                                   (WE[1])
                                   AND
                                   (WE[2])
                                   AND
                                   (WE[5])
                                   AND
                                   (WE[8])
                                   AND
                                   (WE[13]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  5) THEN IF ((WE[1])
                                   AND
                                   (((RE[R_2,1]) AND (WE[2]))
                                    OR
                                    ((WE[3]) AND (RE[R_1,1]))))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  8) THEN IF ((RE[R_2,1])
                                   AND
                                   (WE[1])
                                   AND
                                   (WE[2])
                                   AND
                                   (WE[5]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 12) THEN IF ((RE[R_2,1])
                                   AND
                                   (WE[1])
                                   AND
                                   (WE[2])
                                   AND
                                   (WE[4])
                                   AND
                                   (WE[8]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 17) THEN IF ((RE[R_2,1])
                                   AND
                                   ((RE[R_2,2]) AND (WE[1]))
                                   AND
                                   (WE[2])
                                   AND
                                   (WE[4])
                                   AND
                                   ((WE[7]) AND (WE[8]))
                                   AND
                                   (WE[12]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  2) THEN IF ((RE[R_2,1])
                                   AND
                                   (WE[1]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  4) THEN IF ((RE[R_2,1])
                                   AND
                                   ((RE[R_2,2]) AND (WE[1]))
                                   AND
                                   (WE[2]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld =  7) THEN IF ((RE[R_2,1])
                                   AND
                                   ((RE[R_2,2]) AND (WE[1]))
                                   AND
                                   ((RE[R_2,3]) AND (WE[2]))
                                   AND
                                   (WE[4]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 11) THEN IF ((RE[R_2,1])
                                   AND
                                   ((RE[R_2,2]) AND (WE[1]))
                                   AND
                                   ((RE[R_2,3]) AND (WE[2]))
                                   AND
                                   ((RE[R_2,4]) AND (WE[4]))
                                   AND
                                   (WE[7]))
                               THEN dm_WE := TRUE;
     IF (Sektorfeld = 16) THEN IF ((RE[R_2,1])
                                   AND
                                   ((RE[R_2,2]) AND (WE[1]))
                                   AND
                                   ((RE[R_2,3]) AND (WE[2]))
                                   AND
                                   ((RE[R_2,4]) AND (WE[4]))
                                   AND
                                   (WE[7])
                                   AND
                                   (WE[11]))
                               THEN dm_WE := TRUE;
{ <--| |--> }
END;


PROCEDURE dm_Aufstockung(VAR WM : Type_WM;
                         y,x    : INTEGER);
BEGIN
     IF (WM[y,x].I = 1.5) THEN WM[y,x].I := 1.6;
     IF (WM[y,x].I = 1.3) THEN WM[y,x].I := 1.5;
     IF (WM[y,x].I = 1)   THEN WM[y,x].I := 1.3;
     IF (WM[y,x].I = 0)   THEN WM[y,x].I := 1;
END;


PROCEDURE dm_Sektorfeldschaltung(VAR WM : Type_WM;
                                 Zentrum_y,Zentrum_x,Sektor,Sektorfeld
                                        : INTEGER);
VAR x,y : INTEGER;
BEGIN
     WE[Sektorfeld] := FALSE;
     y := dm_SFA_y(Zentrum_y,Sektor,Sektorfeld);
     x := dm_SFA_x(Zentrum_x,Sektor,Sektorfeld);
     IF (EFd(y,x)) THEN
      IF ((WM[y,x].S = 1)
          AND
          (WM[y,x].E = 0)
          AND
          (dm_WE(Sektor,Sektorfeld))) THEN
      BEGIN
           dm_Aufstockung(WM,y,x);
           WE[Sektorfeld] := TRUE;
      END;
END;


PROCEDURE dm_Sektorwelle(VAR WM                     : Type_WM;
                         Zentrum_y,Zentrum_x,Sektor : INTEGER);
VAR Sektorfeld : INTEGER;
BEGIN
     FOR Sektorfeld := 1 TO 21 DO
     dm_Sektorfeldschaltung(WM,Zentrum_y,Zentrum_x,Sektor,Sektorfeld);
END;


{!} PROCEDURE dm_Verteilungsbewertung_alt(Tiefe,Spieler : INTEGER);
VAR T_E,T_E1,T_F,T_F1,T_R,T_R1,T_Sektor,x,x_1,y,y_1 : INTEGER;
BEGIN
     FOR T_F1  := 1 TO 4 DO
     FOR T_R1  := 1 TO 6 DO
     FOR T_E1  := 1 TO 7 DO VB[Tiefe,Spieler,T_F1,T_R1,T_E1] := 0;
     FOR y := 0 TO 7  DO
     FOR x := 0 TO 10 DO
     BEGIN
          AVFd[y,x].E := SFd[Tiefe,y,x].E;
          AVFd[y,x].I := 0;
          AVFd[y,x].S := SFd[Tiefe,y,x].S;
          T_VFd[y,x]  := AVFd[y,x];
     END;
     FOR T_F := 1 TO 4 DO
     BEGIN
          AVFr[T_F]  := SFr[Tiefe,Spieler,T_F];
          T_VFr[T_F] := AVFr[T_F];
     END;
     FOR T_F := 1 TO 4 DO
     BEGIN
          FOR T_R := 1 TO 6 DO
          FOR T_E := 1 TO 7 DO RE[T_R,T_E] := FALSE;
          FOR T_R := 1 TO 6 DO
          FOR T_E := 1 TO 7 DO
          BEGIN
               y := AVFr[T_F].y+RS_y(T_R,T_E);
               x := AVFr[T_F].x+RS_x(T_R,T_E);
               IF (EFd(y,x) = FALSE)                       THEN BREAK;
               IF ((AVFd[y,x].S = 0) OR (AVFd[y,x].E > 0)) THEN BREAK;
               dm_Aufstockung(AVFd,y,x);
               RE[T_R,T_E] := TRUE;
          END;
          FOR T_Sektor := 1 TO 6 DO
           dm_Sektorwelle(AVFd,AVFr[T_F].y,AVFr[T_F].x,T_Sektor);
     END;
     AVS := 0;
     FOR y := 0 TO 7  DO
     FOR x := 0 TO 10 DO
     BEGIN
          AVS         := AVS+AVFd[y,x].I;
          AVFd[y,x].I := 0;
     END;
     FOR T_F := 1 TO 4 DO
     BEGIN
          AVFr[T_F]  := SFr[Tiefe,3-Spieler,T_F];
          T_VFr[T_F] := AVFr[T_F];
     END;
     FOR T_F := 1 TO 4 DO
     BEGIN
          FOR T_R := 1 TO 6 DO
          FOR T_E := 1 TO 7 DO RE[T_R,T_E] := FALSE;
          FOR T_R := 1 TO 6 DO
          FOR T_E := 1 TO 7 DO
          BEGIN
               y := AVFr[T_F].y+RS_y(T_R,T_E);
               x := AVFr[T_F].x+RS_x(T_R,T_E);
               IF (EFd(y,x) = FALSE)                       THEN BREAK;
               IF ((AVFd[y,x].S = 0) OR (AVFd[y,x].E > 0)) THEN BREAK;
               dm_Aufstockung(AVFd,y,x);
               RE[T_R,T_E] := TRUE;
          END;
          FOR T_Sektor := 1 TO 6 DO
           dm_Sektorwelle(AVFd,AVFr[T_F].y,AVFr[T_F].x,T_Sektor);
     END;
     AVK := 0;
     FOR y := 0 TO 7  DO
     FOR x := 0 TO 10 DO AVK := AVK+AVFd[y,x].I;
     { Jetzt die neuen Stellungen: }
     FOR T_F1 := 1 TO 4 DO
     BEGIN
          IF (((Tiefe = 1)
               AND
               (Revierfigur[1,Selbst,T_F1].Wichtigkeit = FALSE))
              OR
              ((Tiefe = 2)
               AND
               (Revierfigur[1,Gegner,T_F1].Wichtigkeit = FALSE)
               AND
               (Revierfigur[2,Gegner,T_F1].Wichtigkeit = FALSE))) THEN CONTINUE;
          FOR T_R1 := 1 TO 6 DO
          FOR T_E1 := 1 TO 7 DO
          BEGIN
               y_1 := SFr[Tiefe,Spieler,T_F1].y+RS_y(T_R1,T_E1);
               x_1 := SFr[Tiefe,Spieler,T_F1].x+RS_x(T_R1,T_E1);
               IF (EFd(y_1,x_1) = FALSE) THEN BREAK;
               IF ((SFd[Tiefe,y_1,x_1].S = 0) OR(SFd[Tiefe,y_1,x_1].E > 0))
               THEN BREAK;
               { T_-Stellung ver�ndern: }
               T_VFd[y_1,x_1].S := 0;
               T_VFd[T_VFr[T_F1].y,T_VFr[T_F1].x].S := 0;
               { T_VS ermitteln: }
               FOR T_F := 1 TO 4 DO
               BEGIN
                     T_VFr[T_F].y := SFr[Tiefe,Spieler,T_F].y;
                     T_VFr[T_F].x := SFr[Tiefe,Spieler,T_F].x;
               END;
               T_VFr[T_F1].y := y_1;
               T_VFr[T_F1].y := x_1;
               FOR T_F := 1 TO 4 DO
               BEGIN
                    FOR T_R := 1 TO 6 DO
                    FOR T_E := 1 TO 7 DO RE[T_R,T_E] := FALSE;
                    FOR T_R := 1 TO 6 DO
                    FOR T_E := 1 TO 7 DO
                    BEGIN
                         y := T_VFr[T_F].y+RS_y(T_R,T_E);
                         x := T_VFr[T_F].x+RS_x(T_R,T_E);
                         IF (EFd(y,x) = FALSE) THEN BREAK;
                         IF ((T_VFd[y,x].S = 0) OR (T_VFd[y,x].E > 0))
                         THEN BREAK;
                         dm_Aufstockung(T_VFd,y,x);
                         RE[T_R,T_E] := TRUE;
                    END;
                    FOR T_Sektor := 1 TO 6 DO
                    dm_Sektorwelle(T_VFd,T_VFr[T_F].y,T_VFr[T_F].x,T_Sektor);
               END;
               T_VS := 0;
               FOR y := 0 TO 7  DO
               FOR x := 0 TO 10 DO
               BEGIN
                    T_VS         := T_VS+T_VFd[y,x].I;
                    T_VFd[y,x].I := 0;
               END;
               { T_VG ermitteln: }
               FOR T_F := 1 TO 4 DO
               BEGIN
                     T_VFr[T_F].y := SFr[Tiefe,3-Spieler,T_F].y;
                     T_VFr[T_F].x := SFr[Tiefe,3-Spieler,T_F].x;
               END;
               FOR T_F := 1 TO 4 DO
               BEGIN
                    FOR T_R := 1 TO 6 DO
                    FOR T_E := 1 TO 7 DO RE[T_R,T_E] := FALSE;
                    FOR T_R := 1 TO 6 DO
                    FOR T_E := 1 TO 7 DO
                    BEGIN
                         y := T_VFr[T_F].y+RS_y(T_R,T_E);
                         x := T_VFr[T_F].x+RS_x(T_R,T_E);
                         IF (EFd(y,x) = FALSE) THEN BREAK;
                         IF ((T_VFd[y,x].S = 0) OR (T_VFd[y,x].E > 0))
                         THEN BREAK;
                         dm_Aufstockung(T_VFd,y,x);
                         RE[T_R,T_E] := TRUE;
                    END;
                    FOR T_Sektor := 1 TO 6 DO
                    dm_Sektorwelle(T_VFd,T_VFr[T_F].y,T_VFr[T_F].x,T_Sektor);
               END;
               T_VK := 0;
               FOR y := 0 TO 7  DO
               FOR x := 0 TO 10 DO
               BEGIN
                    T_VK         := T_VK+T_VFd[y,x].I;
                    T_VFd[y,x].I := 0;
               END;
               { VB[Tiefe,Spieler,Figur,Richtung,Entfernung] festlegen: }
               VB[Tiefe,Spieler,T_F1,T_R1,T_E1] := {AVG-AVS+T_VS-T_VG} T_VS;
               { VB[Tiefe,Spieler,T_F1,T_R1,T_E1] := T_VB/AVB-1; }
               { T_-Stellung zur�ck �ndern: }
               T_VFd[y_1,x_1].S := 1;
          END;
     END;
END;
{ ... bis hier neu. }


{ 1780 - Hier zum zweiten Mal neu: }
TYPE Type_Gruppe = RECORD
                    EK  : EXTENDED;
                    ES  : EXTENDED;
                    FdA : INTEGER;
                    PA  : INTEGER;
                    SP  : EXTENDED;
END;
{ 'ES'   bedeutet 'Eigentum_Spieler'.                  }
{ 'EK'   bedeutet 'Eigentum_Konkurrent'.               }
{ 'SP'   bedeutet 'Spezialpunktzahl'.                  }
TYPE Type_U      = ARRAY[1..6] OF BOOLEAN;
TYPE Type_VFd    = RECORD
                    A : INTEGER;
                    G : INTEGER;
END;
{ 'A'    bedeutet 'Art'.                               }
{ 'G'    bedeutet 'Gruppenidentifikationsnummer'.      }
{ 'U'    bedeutet 'Umgebungsfeldfreiheit'.             }

VAR  T_Fd    : ARRAY[0..7,0..10] OF Type_Feld;
VAR  T_Fr    : ARRAY[1..2,1..4]  OF Type_Figur;
VAR  GFd     : ARRAY[0..52]      OF Type_Figur;
VAR  Gruppe  : ARRAY[1..20]      OF Type_Gruppe;
VAR  RVFd    : ARRAY[1..2,1..60] OF Type_Revierfeld;
VAR  VFd     : ARRAY[0..7,0..10] OF Type_VFd;
{ 'GFd'  bedeutet 'Gruppenfeld'.                       }
{ 'RVFd' bedeutet 'Revierver�nderungsfeld'.            }

FUNCTION dm_Art(U : Type_U) : INTEGER;
BEGIN
{ |--> <--| }
     IF (U[1]) THEN
      IF (U[2]) THEN
       IF (U[3]) THEN
        IF (U[4]) THEN dm_Art := 3
        ELSE
         IF (U[5]) THEN
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 2
         ELSE dm_Art := 3
       ELSE
        IF (U[4]) THEN
         IF (U[5]) THEN
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 2
         ELSE dm_Art := 2
        ELSE
         IF (U[5]) THEN
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 2
         ELSE
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 1
      ELSE
       IF (U[3]) THEN
        IF (U[4]) THEN
         IF (U[5]) THEN
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 2
         ELSE dm_Art := 2
        ELSE dm_Art := 2
       ELSE
        IF (U[4]) THEN
         IF (U[5]) THEN
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 2
         ELSE dm_Art := 2
        ELSE
         IF (U[5]) THEN
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 2
         ELSE dm_Art := 1
     ELSE
      IF (U[2]) THEN
       IF (U[3]) THEN
        IF (U[4]) THEN
         IF (U[5]) THEN dm_Art := 3
         ELSE
          IF (U[6])
          THEN dm_Art := 2
          ELSE dm_Art := 3
        ELSE
         IF (U[5]) THEN dm_Art := 2
         ELSE
          IF (U[6])
          THEN dm_Art := 2
          ELSE dm_Art := 1
       ELSE
        IF (U[4]) THEN dm_Art := 2
        ELSE
         IF (U[5]) THEN dm_Art := 2
         ELSE
          IF (U[6])
          THEN dm_Art := 2
          ELSE dm_Art := 1
      ELSE
       IF (U[3]) THEN
        IF (U[4]) THEN
         IF (U[5]) THEN dm_Art := 3
         ELSE
          IF (U[6])
          THEN dm_Art := 2
          ELSE dm_Art := 1
        ELSE
         IF (U[5]) THEN dm_Art := 2
         ELSE
          IF (U[6])
          THEN dm_Art := 2
          ELSE dm_Art := 1
       ELSE
        IF (U[4]) THEN
         IF (U[5]) THEN
          IF (U[6])
          THEN dm_Art := 3
          ELSE dm_Art := 1
         ELSE
          IF (U[6])
          THEN dm_Art := 2
          ELSE dm_Art := 1
        ELSE dm_Art := 1;
{ <--| |--> }
END;


PROCEDURE dm_Eigentumsaufstockung(T_G,Teilnehmer : INTEGER;
                                  Vollwertigkeit : BOOLEAN);
{ 'G'    bedeutet 'Gruppe'.                            }
BEGIN
     IF (Teilnehmer = 1) THEN
     BEGIN
          IF (Vollwertigkeit) THEN
          BEGIN
               IF (Gruppe[T_G].ES = 1)   THEN Gruppe[T_G].ES := 1.15;
               IF (Gruppe[T_G].ES = 0.6) THEN Gruppe[T_G].ES := 1;
               IF (Gruppe[T_G].ES = 0.4) THEN Gruppe[T_G].ES := 1;
               IF (Gruppe[T_G].ES = 0)   THEN Gruppe[T_G].ES := 1;
          END ELSE
          BEGIN
               IF (Gruppe[T_G].ES = 0.4) THEN Gruppe[T_G].ES := 0.6;
               IF (Gruppe[T_G].ES = 0)   THEN Gruppe[T_G].ES := 0.4;
          END;
     END ELSE
     BEGIN
          IF (Vollwertigkeit) THEN
          BEGIN
               IF (Gruppe[T_G].EK = 1)   THEN Gruppe[T_G].EK := 1.15;
               IF (Gruppe[T_G].EK = 0.6) THEN Gruppe[T_G].EK := 1;
               IF (Gruppe[T_G].EK = 0.4) THEN Gruppe[T_G].EK := 1;
               IF (Gruppe[T_G].EK = 0)   THEN Gruppe[T_G].EK := 1;
          END ELSE
          BEGIN
               IF (Gruppe[T_G].EK = 0.4) THEN Gruppe[T_G].EK := 0.6;
               IF (Gruppe[T_G].EK = 0)   THEN Gruppe[T_G].EK := 0.4;
          END;
     END;
END;


PROCEDURE dm_Verteilungsbewertung(Tiefe,Spieler : INTEGER);
VAR GG,GR : ARRAY[1..20] OF INTEGER;
{ 'GG'   bedeutet 'Gefundene_Gruppe'.                  }
{ 'GG'   bedeutet 'Gruppenrichtung'.                   }
VAR Bereits_eingetragen                                          : BOOLEAN;
VAR ARWK,ARWS,i,j,k,l,m,NFG,T_E,T_E2,T_E3,T_E4,T_F,T_F2,T_FrN,T_G,T_R,T_R2,T_R3
     ,T_R4,T_RWK,T_RWS,T_S,T_Sektor,UP,VP,x,x_1,x_2,x_3,x_4,y,y_1,y_2,y_3,y_4
     : INTEGER;
{ 'ARWK' bedeutet 'Anfangsrevierwert_Konkurrent'.      }
{ 'ARWS' bedeutet 'Anfangsrevierwert_Spieler'.         }
{ 'NFG'  bedeutet 'Nachbarfeldgruppe'.                 }
{ 'RWS'  bedeutet 'Revierwert_Konkurrent'.             }
{ 'RWS'  bedeutet 'Revierwert_Spieler'.                }
{ 'UP'   bedeutet 'Unvollwertige_Punktzahl'.           }
{ 'VP'   bedeutet 'Vollwertige_Punktzahl'.             }
VAR U                                                            : Type_U;
BEGIN
     FOR T_F2  := 1 TO 4 DO
     FOR T_R2  := 1 TO 6 DO
     FOR T_E2  := 1 TO 7 DO
     BEGIN
          VB[Tiefe,Spieler,T_F2,T_R2,T_E2] := -100;
          RV[Tiefe,Spieler,T_F2,T_R2,T_E2] := -100;
     END;
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO
     BEGIN
          VFd[y,x].A := 0;
          VFd[y,x].G := 0;
          IF ((SFd[Tiefe,y,x].S = 0) OR (SFd[Tiefe,y,x].E = 3-Spieler))
          THEN CONTINUE;
          FOR T_R := 1 TO 6 DO
          BEGIN
               U[T_R] := FALSE;
               y_1 := y+RS_y(T_R,1);
               x_1 := x+RS_x(T_R,1);
               IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
               IF ((SFd[Tiefe,y_1,x_1].S = 1) AND (SFd[Tiefe,y_1,x_1].E = 0))
               THEN U[T_R] := TRUE;
          END;
          VFd[y,x].A := dm_Art(U);
     END;
     FOR i := 1 TO 20 DO
     BEGIN
          Gruppe[i].EK  := 0;
          Gruppe[i].ES  := 0;
          Gruppe[i].FdA := 0;
          Gruppe[i].PA  := 0;
          Gruppe[i].SP  := 0;
     END;
     { Felder von der Art 3 gruppieren: }
     T_G := 0;
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO
     BEGIN
          IF ((VFd[y,x].A < 3) OR (VFd[y,x].G > 0)) THEN CONTINUE;
          T_G := T_G+1;
          FOR i := 2 TO 52 DO
          BEGIN
               GFd[i].y := -10;
               GFd[i].x := -10;
          END;
          VFd[y,x].G := T_G;
          Gruppe[T_G].FdA := 1;
          IF (SFd[Tiefe,y,x].E = 0) THEN Gruppe[T_G].PA := SFd[Tiefe,y,x].P;
          GFd[1].y := y;
          GFd[1].x := x;
          i := 1;
          j := 1;
          REPEAT
                FOR T_R := 1 TO 6 DO
                BEGIN
                     y_1 := GFd[j].y+RS_y(T_R,1);
                     x_1 := GFd[j].x+RS_x(T_R,1);
                     IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                     IF ((SFd[Tiefe,y_1,x_1].S = 0)
                         OR
                         (SFd[Tiefe,y_1,x_1].E = 3-Spieler)
                         OR
                         (VFd[y_1,x_1].A < 3)
                         OR
                         (VFd[y_1,x_1].G > 0)) THEN CONTINUE;
                     VFd[y_1,x_1].G := T_G;
                     Gruppe[T_G].FdA := Gruppe[T_G].FdA+1;
                     IF (SFd[Tiefe,y_1,x_1].E = 0)
                     THEN Gruppe[T_G].PA
                          := Gruppe[T_G].PA +SFd[Tiefe,y_1,x_1].P;
                     i := i+1;
                     GFd[i].y := y_1;
                     GFd[i].x := x_1;
                END;
                j := j+1;
          UNTIL (j > i);
     END;
     { Nachbarfelder der Gruppen zu Gruppen hinzuf�gen: }
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO
     BEGIN
          IF ((NOT((VFd[y,x].A = 1) OR (VFd[y,x].A = 2)))
              OR
              (SFd[Tiefe,y,x].S = 0)
              OR
              (SFd[Tiefe,y,x].E = 3-Spieler)) THEN CONTINUE;
          NFG := -10;
          FOR T_R := 1 TO 6 DO
          BEGIN
               y_1 := y+RS_y(T_R,1);
               x_1 := x+RS_x(T_R,1);
               IF ((VFd[y_1,x_1].G > 0) AND (VFd[y_1,x_1].A = 3))
               THEN IF (((NFG > 0) AND (NFG <> VFd[y_1,x_1].G)) OR (NFG = 0))
                    THEN NFG := 0
                    ELSE NFG := VFd[y_1,x_1].G;
          END;
          IF (NFG > 0) THEN
          BEGIN
               VFd[y,x].G     := NFG;
               Gruppe[NFG].PA := Gruppe[NFG].PA+SFd[Tiefe,y,x].P;
          END;
     END;
{---}
     IF (Tiefe = 1)
     THEN FOR y := 0 TO  7 Do
          FOR x := 0 TO 10 DO
           Log('Feld ('+IntToStr(y)+','+IntToStr(x)+') :   Art: '
               +IntToStr(VFd[y,x].A)+'   Gruppe: '+IntToStr(VFd[y,x].G));
{---}
     { Erreichbarkeiten des Spielers ermitteln: }
{    Log('Erreichbarkeiten des Spielers ermitteln:'); }
     FOR T_F := 1 TO 4 DO
     BEGIN
          FOR i := 1 TO T_G DO
          BEGIN
               GG[i] := 0;
               GR[i] := 0;
          END;
          j := 1;
          FOR T_R := 1 TO 6 DO
          BEGIN
               FOR T_E := 1 TO 7 DO
               BEGIN
                    y := SFr[Tiefe,Spieler,T_F].y+RS_y(T_R,T_E);
                    x := SFr[Tiefe,Spieler,T_F].x+RS_x(T_R,T_E);
                    IF (EFd(y,x) = FALSE) THEN BREAK;
                    IF ((SFd[Tiefe,y,x].S = 0) OR (SFd[Tiefe,y,x].E > 0))
                    THEN BREAK;
                    IF (VFd[y,x].G = 0) THEN CONTINUE;
                    Bereits_eingetragen := FALSE;
                    FOR i := 1 TO T_G DO
                    BEGIN
                         IF (GG[i] = VFd[y,x].G) THEN
                         BEGIN
                              Bereits_eingetragen := TRUE;
                              {
                              IF ((GR[i] <> 0)
                                  AND
                                  (GR[i] <> T_R)) THEN
                              BEGIN
                                   dm_Eigentumsaufstockung(VFd[y,x].G,1,TRUE);
                                   GR[i] := 0;
                                   Bereits_eingetragen := TRUE;
                                   BREAK;
                              END;
                              }
                              BREAK;
                         END;
                    END;
                    IF (Bereits_eingetragen = FALSE) THEN
                    BEGIN
                         GG[j] := VFd[y,x].G;
                         GR[j] := T_R;
                         j := j+1;
                    END;
               END;
          END;
          FOR i := 1 TO T_G DO
           IF ((GG[i] > 0) AND (GR[i] > 0))
           THEN dm_Eigentumsaufstockung(GG[i],1,FALSE);
          y := SFr[Tiefe,Spieler,T_F].y;
          x := SFr[Tiefe,Spieler,T_F].x;
          IF (VFd[y,x].G > 0) THEN
          BEGIN
              Bereits_eingetragen := FALSE;
              FOR i := 1 TO T_G DO IF ((GG[i] = VFd[y,x].G) AND (GR[i] = 0))
                                   THEN Bereits_eingetragen := TRUE;
              IF (Bereits_eingetragen = FALSE)
              THEN dm_Eigentumsaufstockung(VFd[y,x].G,1,TRUE);
          END;
     END;
     { Erreichbarkeiten des Konkurrenten ermitteln: }
{    Log('Erreichbarkeiten des Konkurrenten ermitteln:'); }
     FOR T_F := 1 TO 4 DO
     BEGIN
          FOR i := 1 TO T_G DO
          BEGIN
               GG[i] := 0;
               GR[i] := 0;
          END;
          j := 1;
          FOR T_R := 1 TO 6 DO
          BEGIN
               FOR T_E := 1 TO 7 DO
               BEGIN
                    y := SFr[Tiefe,3-Spieler,T_F].y+RS_y(T_R,T_E);
                    x := SFr[Tiefe,3-Spieler,T_F].x+RS_x(T_R,T_E);
                    IF (EFd(y,x) = FALSE) THEN BREAK;
                    IF ((SFd[Tiefe,y,x].S = 0) OR (SFd[Tiefe,y,x].E > 0))
                    THEN BREAK;
                    IF (VFd[y,x].G = 0) THEN CONTINUE;
                    Bereits_eingetragen := FALSE;
                    FOR i := 1 TO T_G DO
                    BEGIN
                         IF (GG[i] = VFd[y,x].G) THEN
                         BEGIN
                              Bereits_eingetragen := TRUE;
                              {
                              IF ((GR[i] <> 0)
                                  AND
                                  (GR[i] <> T_R)) THEN
                              BEGIN
                                   dm_Eigentumsaufstockung(VFd[y,x].G,2,TRUE);
                                   GR[i] := 0;
                                   Bereits_eingetragen := TRUE;
                                   BREAK;
                              END;
                              }
                              BREAK;
                         END;
                    END;
                    IF (Bereits_eingetragen = FALSE) THEN
                    BEGIN
                         GG[j] := VFd[y,x].G;
                         GR[j] := T_R;
                         j := j+1;
                    END;
               END;
          END;
          FOR i := 1 TO T_G DO
           IF ((GG[i] > 0) AND (GR[i] > 0))
           THEN dm_Eigentumsaufstockung(GG[i],2,FALSE);
     END;
{    Gro�e Gruppen zerteilen: }
     FOR i := 1 TO T_G DO
     BEGIN
          IF (Gruppe[i].FdA < 22) THEN CONTINUE;
          FOR j := 1 TO 56 DO
          BEGIN
               U_Fd[j].y := -10;
               U_Fd[j].x := -10;
               V_Fd[j].y := -10;
               V_Fd[j].x := -10;
          END;
          VP := 0;
          UP := 0;
          FOR T_F := 1 TO 4 DO
           IF (VFd[SFr[Tiefe,Spieler,T_F].y
                   ,SFr[Tiefe,Spieler,T_F].x].G = i) THEN
           BEGIN
                { Welle: }
                FOR y := 0 TO  7 DO
                FOR x := 0 TO 10 DO
                BEGIN
                     WEFd[y,x].E := SFd[Tiefe,y,x].E;
                     WEFd[y,x].S := SFd[Tiefe,y,x].S;
                     WEFd[y,x].I := 0;
                END;
                FOR T_R3 := 1 TO 6 DO
                FOR T_E3 := 1 TO 7 DO RE[T_R3,T_E3] := FALSE;
                FOR T_R3 := 1 TO 6 DO
                FOR T_E3 := 1 TO 7 DO
                BEGIN
                     y := SFr[Tiefe,Spieler,T_F].y+RS_y(T_R3,T_E3);
                     x := SFr[Tiefe,Spieler,T_F].x+RS_x(T_R3,T_E3);
                     IF (EFd(y,x) = FALSE)                       THEN BREAK;
                     IF ((WEFd[y,x].S = 0) OR (WEFd[y,x].E > 0)) THEN BREAK;
                     dm_Aufstockung(WEFd,y,x);
                     RE[T_R3,T_E3] := TRUE;
                END;
                FOR T_Sektor := 1 TO 6 DO
                 dm_Sektorwelle(WEFd,SFr[Tiefe,Spieler,T_F].y
                                    ,SFr[Tiefe,Spieler,T_F].x,T_Sektor);
                { Nach der Welle: }
                FOR y := 0 TO  7 DO
                FOR x := 0 TO 10 DO
                BEGIN
                     IF (WEFd[y,x].S = 0) THEN CONTINUE;
                     IF (WEFd[y,x].E > 0) THEN CONTINUE;
                     IF (VFd[y,x].G <> i) THEN CONTINUE;
                     IF (WEFd[y,x].I = 0) THEN CONTINUE;
                     FOR j := 1 TO 4 DO
                      IF ((VFd[SFr[Tiefe,3-Spieler,j].y
                               ,SFr[Tiefe,3-Spieler,j].x].G = i)
                          AND
                          (dm_Feldabstand(SFr[Tiefe,Spieler,T_F].y
                                          ,SFr[Tiefe,Spieler,T_F].x
                                          ,y,x)
                           >
                           dm_Feldabstand(SFr[Tiefe,Spieler,T_F].y
                                          ,SFr[Tiefe,Spieler,T_F].x
                                          ,SFr[Tiefe,3-Spieler,j].y
                                          ,SFr[Tiefe,3-Spieler,j].x))
                          AND
                          (dm_Feldabstand(SFr[Tiefe,Spieler,T_F].y
                                          ,SFr[Tiefe,Spieler,T_F].x
                                          ,y,x)
                           >
                           dm_Feldabstand(SFr[Tiefe,3-Spieler,j].y
                                          ,SFr[Tiefe,3-Spieler,j].x
                                          ,y,x))) THEN CONTINUE;
                     Bereits_eingetragen := FALSE;
                     FOR j := 1 TO 56 DO
                      IF ((V_Fd[j].y = y)
                          AND
                          (V_Fd[j].x = x)) THEN
                      BEGIN
                           Bereits_eingetragen := TRUE;
                           BREAK;
                      END;
                     IF (Bereits_eingetragen = TRUE) THEN CONTINUE;
                     j := 0;
                     REPEAT
                           j := j+1;
                     UNTIL ((V_Fd[j].y = -10)
                            AND
                            (V_Fd[j].x = -10));
                     V_Fd[j].y := y;
                     V_Fd[j].x := x;
                     VP := VP+SFd[Tiefe,y,x].P;
                END;
           END ELSE
           BEGIN
                FOR T_R3 := 1 TO 6 DO
                FOR T_E3 := 1 TO 7 DO
                BEGIN
                     y_3 := SFr[Tiefe,Spieler,T_F].y+RS_y(T_R3,T_E3);
                     x_3 := SFr[Tiefe,Spieler,T_F].x+RS_x(T_R3,T_E3);
                     IF (EFd(y_3,x_3) = FALSE)  THEN BREAK;
                     IF ((WEFd[y_3,x_3].S = 0)
                         OR
                         (WEFd[y_3,x_3].E > 0)) THEN BREAK;
                     IF (VFd[y_3,x_3].G <> i)   THEN BREAK;
                     FOR T_R4 := 1 TO 6 DO
                     FOR T_E4 := 1 TO 7 DO
                     BEGIN
                          y_4 := y_3+RS_y(T_R4,T_E4);
                          x_4 := x_3+RS_x(T_R4,T_E4);
                          IF (EFd(y_4,x_4) = FALSE)  THEN BREAK;
                          IF ((WEFd[y_4,x_4].S = 0)
                              OR
                              (WEFd[y_4,x_4].E > 0)) THEN BREAK;
                          IF (VFd[y_4,x_4].G <> i)   THEN BREAK;
                          Bereits_eingetragen := FALSE;
                          FOR j := 1 TO 21 DO
                          IF ((U_Fd[j].y = y_4)
                              AND
                              (U_Fd[j].x = x_4)) THEN
                          BEGIN
                               Bereits_eingetragen := TRUE;
                               BREAK;
                          END;
                          IF (Bereits_eingetragen = TRUE) THEN BREAK;
                          j := 0;
                          REPEAT
                                j := j+1;
                          UNTIL ((U_Fd[j].y = -10)
                                 AND
                                 (U_Fd[j].x = -10));
                          U_Fd[j].y := y_4;
                          U_Fd[j].x := x_4;
                          UP := UP+SFd[Tiefe,y_4,x_4].P;
                     END;
                END;
           END;
          Gruppe[i].SP := (VP+UP/2)*Gruppe[i].ES/(Gruppe[i].ES+Gruppe[i].EK);
                {
                  Vollwertigkeit:
                  ===============
                  Jetzt jedes Feld durchgehen! Zum Ausscheiden reicht:
                  - Nicht aus der urspr�nglichen Gruppe i
                  - Nicht von Welle erreichbar
                  - ((Z > FA) AND (Z > W))
                  Felder, die nicht ausgeschieden sind, so behandeln:
                  - In V_Fd eintragen!
                  - VP erh�hen!
                  Unvollwertigkeit:
                  =================
                  Jetzt jedes Feld durchgehen! Zum Dabeisein reicht:
                  - Nach zwei Z�gen von Figur des best. Teilnehmers erreichbar,
                    w�hrend auch das nach einem Zug erreichte Feld aus der
                    urspr�nglichen Gruppe stammt
                  Felder, die dabei sind, so behandeln:
                  - In U_Fd eintragen!
                  - UP erh�hen!
                  Danach:
                  =======
                  - SP berechnen! (VP * Verh�ltnis + UP / 2 * Verh�ltnis)
                  Au�erdem:
                  =========
                  - AVS auf neue andere Weise berechnen lassen!
                }
     END;
     { AVS ermitteln: }
     AVS := 0;
     FOR i := 1 TO T_G DO
      IF (Gruppe[i].ES+Gruppe[i].EK > 0)
      THEN IF (Gruppe[i].SP = 0)
           THEN AVS
                 := AVS+Gruppe[i].PA*Gruppe[i].ES/(Gruppe[i].ES+Gruppe[i].EK)
           ELSE AVS := AVS+Gruppe[i].SP;
{---}
     IF (Tiefe = 1) THEN
     BEGIN
          FOR i := 1 TO T_G DO
           Log('Spieler: Gruppe['+IntToStr(i)
               +']:   ES = '+FloatToStr(Gruppe[i].ES)
               +'   EK = '+FloatToStr(Gruppe[i].EK)
               +'   FdA = '+IntToStr(Gruppe[i].FdA)
               +'   PA = '+IntToStr(Gruppe[i].PA));
     END;
{---}
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO
     BEGIN
          VFd[y,x].A := 0;
          VFd[y,x].G := 0;
          IF ((SFd[Tiefe,y,x].S = 0) OR (SFd[Tiefe,y,x].E = Spieler))
          THEN CONTINUE;
          FOR T_R := 1 TO 6 DO
          BEGIN
               U[T_R] := FALSE;
               y_1 := y+RS_y(T_R,1);
               x_1 := x+RS_x(T_R,1);
               IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
               IF ((SFd[Tiefe,y_1,x_1].S = 1) AND (SFd[Tiefe,y_1,x_1].E = 0))
               THEN U[T_R] := TRUE;
          END;
          VFd[y,x].A := dm_Art(U);
     END;
     FOR i := 1 TO 20 DO
     BEGIN
          Gruppe[i].EK  := 0;
          Gruppe[i].ES  := 0;
          Gruppe[i].FdA := 0;
          Gruppe[i].PA  := 0;
          Gruppe[i].SP  := 0;
     END;
     { Felder von der Art 3 gruppieren: }
     T_G := 0;
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO
     BEGIN
          IF ((VFd[y,x].A < 3) OR (VFd[y,x].G > 0)) THEN CONTINUE;
          T_G := T_G+1;
          FOR i := 2 TO 52 DO
          BEGIN
               GFd[i].y := -10;
               GFd[i].x := -10;
          END;
          VFd[y,x].G := T_G;
          Gruppe[T_G].FdA := 1;
          IF (SFd[Tiefe,y,x].E = 0) THEN Gruppe[T_G].PA := SFd[Tiefe,y,x].P;
          GFd[1].y := y;
          GFd[1].x := x;
          i := 1;
          j := 1;
          REPEAT
                FOR T_R := 1 TO 6 DO
                BEGIN
                     y_1 := GFd[j].y+RS_y(T_R,1);
                     x_1 := GFd[j].x+RS_x(T_R,1);
                     IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                     IF ((SFd[Tiefe,y_1,x_1].S = 0)
                         OR
                         (SFd[Tiefe,y_1,x_1].E = Spieler)
                         OR
                         (VFd[y_1,x_1].A < 3)
                         OR
                         (VFd[y_1,x_1].G > 0)) THEN CONTINUE;
                     VFd[y_1,x_1].G := T_G;
                     Gruppe[T_G].FdA := Gruppe[T_G].FdA+1;
                     IF (SFd[Tiefe,y_1,x_1].E = 0)
                     THEN Gruppe[T_G].PA
                          := Gruppe[T_G].PA +SFd[Tiefe,y_1,x_1].P;
                     i := i+1;
                     GFd[i].y := y_1;
                     GFd[i].x := x_1;
                END;
                j := j+1;
          UNTIL (j > i);
     END;
     { Nachbarfelder der Gruppen zu Gruppen hinzuf�gen: }
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO
     BEGIN
          IF ((NOT((VFd[y,x].A = 1) OR (VFd[y,x].A = 2)))
              OR
              (SFd[Tiefe,y,x].S = 0)
              OR
              (SFd[Tiefe,y,x].E = Spieler)) THEN CONTINUE;
          NFG := -10;
          FOR T_R := 1 TO 6 DO
          BEGIN
               y_1 := y+RS_y(T_R,1);
               x_1 := x+RS_x(T_R,1);
               IF ((VFd[y_1,x_1].G > 0) AND (VFd[y_1,x_1].A = 3))
               THEN IF (((NFG > 0) AND (NFG <> VFd[y_1,x_1].G)) OR (NFG = 0))
                    THEN NFG := 0
                    ELSE NFG := VFd[y_1,x_1].G;
          END;
          IF (NFG > 0) THEN
          BEGIN
               VFd[y,x].G     := NFG;
               Gruppe[NFG].PA := Gruppe[NFG].PA+SFd[Tiefe,y,x].P;
          END;
     END;
     { Erreichbarkeiten des Spielers ermitteln: }
     FOR T_F := 1 TO 4 DO
     BEGIN
          FOR i := 1 TO T_G DO
          BEGIN
               GG[i] := 0;
               GR[i] := 0;
          END;
          j := 1;
          FOR T_R := 1 TO 6 DO
          BEGIN
               FOR T_E := 1 TO 7 DO
               BEGIN
                    y := SFr[Tiefe,Spieler,T_F].y+RS_y(T_R,T_E);
                    x := SFr[Tiefe,Spieler,T_F].x+RS_x(T_R,T_E);
                    IF (EFd(y,x) = FALSE) THEN BREAK;
                    IF ((SFd[Tiefe,y,x].S = 0) OR (SFd[Tiefe,y,x].E > 0))
                    THEN BREAK;
                    IF (VFd[y,x].G = 0) THEN CONTINUE;
                    Bereits_eingetragen := FALSE;
                    FOR i := 1 TO T_G DO
                    BEGIN
                         IF (GG[i] = VFd[y,x].G) THEN
                         BEGIN
                              Bereits_eingetragen := TRUE;
                              {
                              IF ((GR[i] <> 0)
                                  AND
                                  (GR[i] <> T_R)) THEN
                              BEGIN
                                   dm_Eigentumsaufstockung(VFd[y,x].G,1,TRUE);
                                   GR[i] := 0;
                                   Bereits_eingetragen := TRUE;
                                   BREAK;
                              END;
                              }
                              BREAK;
                         END;
                    END;
                    IF (Bereits_eingetragen = FALSE) THEN
                    BEGIN
                         GG[j] := VFd[y,x].G;
                         GR[j] := T_R;
                         j := j+1;
                    END;
               END;
          END;
          FOR i := 1 TO T_G DO
           IF ((GG[i] > 0) AND (GR[i] > 0))
           THEN dm_Eigentumsaufstockung(GG[i],1,FALSE);
     END;
     { Erreichbarkeiten des Konkurrenten ermitteln: }
     FOR T_F := 1 TO 4 DO
     BEGIN
          FOR i := 1 TO T_G DO
          BEGIN
               GG[i] := 0;
               GR[i] := 0;
          END;
          j := 1;
          FOR T_R := 1 TO 6 DO
          BEGIN
               FOR T_E := 1 TO 7 DO
               BEGIN
                    y := SFr[Tiefe,3-Spieler,T_F].y+RS_y(T_R,T_E);
                    x := SFr[Tiefe,3-Spieler,T_F].x+RS_x(T_R,T_E);
                    IF (EFd(y,x) = FALSE) THEN BREAK;
                    IF ((SFd[Tiefe,y,x].S = 0) OR (SFd[Tiefe,y,x].E > 0))
                    THEN BREAK;
                    IF (VFd[y,x].G = 0) THEN CONTINUE;
                    Bereits_eingetragen := FALSE;
                    FOR i := 1 TO T_G DO
                    BEGIN
                         IF (GG[i] = VFd[y,x].G) THEN
                         BEGIN
                              Bereits_eingetragen := TRUE;
                              {
                              IF ((GR[i] <> 0)
                                  AND
                                  (GR[i] <> T_R)) THEN
                              BEGIN
                                   dm_Eigentumsaufstockung(VFd[y,x].G,2,TRUE);
                                   GR[i] := 0;
                                   Bereits_eingetragen := TRUE;
                                   BREAK;
                              END;
                              }
                              BREAK;
                         END;
                    END;
                    IF (Bereits_eingetragen = FALSE) THEN
                    BEGIN
                         GG[j] := VFd[y,x].G;
                         GR[j] := T_R;
                         j := j+1;
                    END;
               END;
          END;
          FOR i := 1 TO T_G DO
           IF ((GG[i] > 0) AND (GR[i] > 0))
           THEN dm_Eigentumsaufstockung(GG[i],2,FALSE);
          y := SFr[Tiefe,3-Spieler,T_F].y;
          x := SFr[Tiefe,3-Spieler,T_F].x;
          IF (VFd[y,x].G > 0) THEN
          BEGIN
              Bereits_eingetragen := FALSE;
              FOR i := 1 TO T_G DO IF ((GG[i] = VFd[y,x].G) AND (GR[i] = 0))
                                   THEN Bereits_eingetragen := TRUE;
              IF (Bereits_eingetragen = FALSE)
              THEN dm_Eigentumsaufstockung(VFd[y,x].G,2,TRUE);
          END;
     END;
     {    Gro�e Gruppen zerteilen: }
     FOR i := 1 TO T_G DO
     BEGIN
          IF (Gruppe[i].FdA < 22) THEN CONTINUE;
          FOR j := 1 TO 56 DO
          BEGIN
               U_Fd[j].y := -10;
               U_Fd[j].x := -10;
               V_Fd[j].y := -10;
               V_Fd[j].x := -10;
          END;
          VP := 0;
          UP := 0;
          FOR T_F := 1 TO 4 DO
           IF (VFd[SFr[Tiefe,3-Spieler,T_F].y
                   ,SFr[Tiefe,3-Spieler,T_F].x].G = i) THEN
           BEGIN
                { Welle: }
                FOR y := 0 TO  7 DO
                FOR x := 0 TO 10 DO
                BEGIN
                     WEFd[y,x].E := SFd[Tiefe,y,x].E;
                     WEFd[y,x].S := SFd[Tiefe,y,x].S;
                     WEFd[y,x].I := 0;
                END;
                FOR T_R3 := 1 TO 6 DO
                FOR T_E3 := 1 TO 7 DO RE[T_R3,T_E3] := FALSE;
                FOR T_R3 := 1 TO 6 DO
                FOR T_E3 := 1 TO 7 DO
                BEGIN
                     y := SFr[Tiefe,3-Spieler,T_F].y+RS_y(T_R3,T_E3);
                     x := SFr[Tiefe,3-Spieler,T_F].x+RS_x(T_R3,T_E3);
                     IF (EFd(y,x) = FALSE)                       THEN BREAK;
                     IF ((WEFd[y,x].S = 0) OR (WEFd[y,x].E > 0)) THEN BREAK;
                     dm_Aufstockung(WEFd,y,x);
                     RE[T_R3,T_E3] := TRUE;
                END;
                FOR T_Sektor := 1 TO 6 DO
                 dm_Sektorwelle(WEFd,SFr[Tiefe,3-Spieler,T_F].y
                                    ,SFr[Tiefe,3-Spieler,T_F].x,T_Sektor);
                { Nach der Welle: }
                FOR y := 0 TO  7 DO
                FOR x := 0 TO 10 DO
                BEGIN
                     IF (WEFd[y,x].S = 0) THEN CONTINUE;
                     IF (WEFd[y,x].E > 0) THEN CONTINUE;
                     IF (VFd[y,x].G <> i) THEN CONTINUE;
                     IF (WEFd[y,x].I = 0) THEN CONTINUE;
                     FOR j := 1 TO 4 DO
                      IF ((VFd[SFr[Tiefe,Spieler,j].y
                               ,SFr[Tiefe,Spieler,j].x].G = i)
                          AND
                          (dm_Feldabstand(SFr[Tiefe,3-Spieler,T_F].y
                                          ,SFr[Tiefe,3-Spieler,T_F].x
                                          ,y,x)
                           >
                           dm_Feldabstand(SFr[Tiefe,3-Spieler,T_F].y
                                          ,SFr[Tiefe,3-Spieler,T_F].x
                                          ,SFr[Tiefe,Spieler,j].y
                                          ,SFr[Tiefe,Spieler,j].x))
                          AND
                          (dm_Feldabstand(SFr[Tiefe,3-Spieler,T_F].y
                                          ,SFr[Tiefe,3-Spieler,T_F].x
                                          ,y,x)
                           >
                           dm_Feldabstand(SFr[Tiefe,Spieler,j].y
                                          ,SFr[Tiefe,Spieler,j].x
                                          ,y,x))) THEN CONTINUE;
                     Bereits_eingetragen := FALSE;
                     FOR j := 1 TO 56 DO
                      IF ((V_Fd[j].y = y)
                          AND
                          (V_Fd[j].x = x)) THEN
                      BEGIN
                           Bereits_eingetragen := TRUE;
                           BREAK;
                      END;
                     IF (Bereits_eingetragen = TRUE) THEN CONTINUE;
                     j := 0;
                     REPEAT
                           j := j+1;
                     UNTIL ((V_Fd[j].y = -10)
                            AND
                            (V_Fd[j].x = -10));
                     V_Fd[j].y := y;
                     V_Fd[j].x := x;
                     VP := VP+SFd[Tiefe,y,x].P;
                END;
           END ELSE
           BEGIN
                FOR T_R3 := 1 TO 6 DO
                FOR T_E3 := 1 TO 7 DO
                BEGIN
                     y_3 := SFr[Tiefe,3-Spieler,T_F].y+RS_y(T_R3,T_E3);
                     x_3 := SFr[Tiefe,3-Spieler,T_F].x+RS_x(T_R3,T_E3);
                     IF (EFd(y_3,x_3) = FALSE)  THEN BREAK;
                     IF ((WEFd[y_3,x_3].S = 0)
                         OR
                         (WEFd[y_3,x_3].E > 0)) THEN BREAK;
                     IF (VFd[y_3,x_3].G <> i)   THEN BREAK;
                     FOR T_R4 := 1 TO 6 DO
                     FOR T_E4 := 1 TO 7 DO
                     BEGIN
                          y_4 := y_3+RS_y(T_R4,T_E4);
                          x_4 := x_3+RS_x(T_R4,T_E4);
                          IF (EFd(y_4,x_4) = FALSE)  THEN BREAK;
                          IF ((WEFd[y_4,x_4].S = 0)
                              OR
                              (WEFd[y_4,x_4].E > 0)) THEN BREAK;
                          IF (VFd[y_4,x_4].G <> i)   THEN BREAK;
                          Bereits_eingetragen := FALSE;
                          FOR j := 1 TO 21 DO
                          IF ((U_Fd[j].y = y_4)
                              AND
                              (U_Fd[j].x = x_4)) THEN
                          BEGIN
                               Bereits_eingetragen := TRUE;
                               BREAK;
                          END;
                          IF (Bereits_eingetragen = TRUE) THEN BREAK;
                          j := 0;
                          REPEAT
                                j := j+1;
                          UNTIL ((U_Fd[j].y = -10)
                                 AND
                                 (U_Fd[j].x = -10));
                          U_Fd[j].y := y_4;
                          U_Fd[j].x := x_4;
                          UP := UP+SFd[Tiefe,y_4,x_4].P;
                     END;
                END;
           END;
          Gruppe[i].SP := (VP+UP/2)*Gruppe[i].EK/(Gruppe[i].ES+Gruppe[i].EK);
     END;
     { AVK ermitteln: }
     AVK := 0;
     FOR i := 1 TO T_G DO
      IF (Gruppe[i].ES+Gruppe[i].EK > 0)
      THEN IF (Gruppe[i].SP = 0)
           THEN AVK
                 := AVK+Gruppe[i].PA*Gruppe[i].EK/(Gruppe[i].ES+Gruppe[i].EK)
           ELSE AVK := AVK+Gruppe[i].SP;
{}   Log('AVS = '+FloatToStr(AVS)+'   AVK = '+FloatToStr(AVK));
     { ARWS und ARWK ermitteln: }
     ARWS := 0;
     i := 0;
     FOR T_F := 1 TO 4 DO
      IF (Revierfigur[Tiefe,Spieler,T_F].Wichtigkeit = TRUE)
      THEN FOR k := 1 TO Revierfigur[Tiefe,Spieler,T_F].RFdA DO
           BEGIN
                Bereits_eingetragen := FALSE;
                l := 1;
                WHILE (l <= i) DO
                BEGIN
                     IF ((RFd[Tiefe,Spieler,T_F,k].y
                          =
                          RVFd[Spieler,l].y)
                         AND
                         (RFd[Tiefe,Spieler,T_F,k].x
                          =
                          RVFd[Spieler,l].x)) THEN
                     BEGIN
                          Bereits_eingetragen := TRUE;
                          BREAK;
                     END;
                     l := l+1;
                END;
                IF (Bereits_eingetragen = FALSE) THEN
                BEGIN
                     i := i+1;
                     RVFd[Spieler,i].y := RFd[Tiefe,Spieler,T_F,k].y;
                     RVFd[Spieler,i].x := RFd[Tiefe,Spieler,T_F,k].x;
                     ARWS := ARWS+SFd[Tiefe,RVFd[Spieler,i].y
                                           ,RVFd[Spieler,i].x].P;
                END;
           END;
     ARWK := 0;
     j := 0;
     FOR T_F := 1 TO 4 DO
     BEGIN
          FOR T_R := 1 TO 6 DO
          BEGIN
               y_1 := SFr[Tiefe,3-Spieler,T_F].y+RS_y(T_R,1);
               x_1 := SFr[Tiefe,3-Spieler,T_F].x+RS_x(T_R,1);
               IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
               IF ((SFd[Tiefe,y_1,x_1].S = 0) OR (SFd[Tiefe,y_1,x_1].E > 0))
               THEN CONTINUE;
               Bereits_eingetragen := FALSE;
               FOR l := 1 TO j DO
                IF ((RVFd[3-Spieler,l].y = y_1)
                    AND
                    (RVFd[3-Spieler,l].x = x_1)) THEN
                BEGIN
                     Bereits_eingetragen := TRUE;
                     BREAK;
                END;
               IF (Bereits_eingetragen = FALSE) THEN
               BEGIN
                    j := j+1;
                    RVFd[3-Spieler,j].y := y_1;
                    RVFd[3-Spieler,j].x := x_1;
                    ARWK := ARWK+SFd[Tiefe,y_1,x_1].P;
               END;
          END;
     END;
     m := 1;
     REPEAT
           FOR T_R := 1 TO 6 DO
           BEGIN
                y_1 := RVFd[3-Spieler,m].y+RS_y(T_R,1);
                x_1 := RVFd[3-Spieler,m].x+RS_x(T_R,1);
                IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                IF ((SFd[Tiefe,y_1,x_1].S = 0) OR (SFd[Tiefe,y_1,x_1].E > 0))
                THEN CONTINUE;
                Bereits_eingetragen := FALSE;
                FOR l := 1 TO j DO
                 IF ((RVFd[3-Spieler,l].y = y_1)
                     AND
                     (RVFd[3-Spieler,l].x = x_1)) THEN
                 BEGIN
                      Bereits_eingetragen := TRUE;
                      BREAK;
                 END;
                IF (Bereits_eingetragen = FALSE) THEN
                BEGIN
                     j := j+1;
                     RVFd[3-Spieler,j].y := y_1;
                     RVFd[3-Spieler,j].x := x_1;
                     ARWK := ARWK+SFd[Tiefe,y_1,x_1].P;
                END;
           END;
           m := m+1;
     UNTIL (m > j);
{}   Log('ARWS = '+IntToStr(ARWS)+'   ARWK = '+IntToStr(ARWK));
     { Zugstellungen im Einzelnen behandeln: }
     FOR T_F2 := 1 TO 4 DO
     BEGIN
{ |--> }
     IF (((Tiefe = 1)
          AND
          (Revierfigur[1,Selbst,T_F2].Wichtigkeit = FALSE))
         OR
         ((Tiefe = 2)
          AND
          (Revierfigur[1,Gegner,T_F2].Wichtigkeit = FALSE)
          AND
          (Revierfigur[2,Gegner,T_F2].Wichtigkeit = FALSE))) THEN CONTINUE;
     FOR T_R2 := 1 TO 6 DO
     FOR T_E2 := 1 TO 7 DO
     BEGIN
          y_2 := SFr[Tiefe,Spieler,T_F2].y+RS_y(T_R2,T_E2);
          x_2 := SFr[Tiefe,Spieler,T_F2].x+RS_x(T_R2,T_E2);
          IF (EFd(y_2,x_2) = FALSE) THEN BREAK;
          IF ((SFd[Tiefe,y_2,x_2].S = Nicht_vorhanden)
              OR
              (SFd[Tiefe,y_2,x_2].E > 0))
          THEN BREAK;
          FOR y := 0 TO 7  DO
          FOR x := 0 TO 10 DO T_Fd[y,x] := SFd[Tiefe,y,x];
          T_Fd[SFr[Tiefe,Spieler,T_F2].y,SFr[Tiefe,Spieler,T_F2].x].E   := 0;
          T_Fd[SFr[Tiefe,Spieler,T_F2].y,SFr[Tiefe,Spieler,T_F2].x].FrN := 0;
          T_Fd[SFr[Tiefe,Spieler,T_F2].y,SFr[Tiefe,Spieler,T_F2].x].P   := 0;
          T_Fd[SFr[Tiefe,Spieler,T_F2].y,SFr[Tiefe,Spieler,T_F2].x].S   := 0;
          T_Fd[y_2,x_2].E   := Spieler;
          T_Fd[y_2,x_2].FrN := T_F2;
          FOR T_S   := 1 TO 2 DO
          FOR T_FrN := 1 TO 4 DO T_Fr[T_S,T_FrN] := SFr[Tiefe,T_S,T_FrN];
          T_Fr[Spieler,T_F2].y := y_2;
          T_Fr[Spieler,T_F2].x := x_2;
          { T_RWS und T_RWK ermitteln: }
{}        IF (Tiefe = 1) THEN Log('T_RWS und T_RWK ermitteln:');
          T_RWS := 0;
          T_RWK := 0;
          i := 0;
          j := 0;
          FOR T_F := 1 TO 4 DO
          BEGIN
               IF (Revierfigur[Tiefe,Spieler,T_F].Wichtigkeit = TRUE)
               THEN FOR T_R := 1 TO 6 DO
                    BEGIN
                         y_1 := T_Fr[Spieler,T_F].y+RS_y(T_R,1);
                         x_1 := T_Fr[Spieler,T_F].x+RS_x(T_R,1);
                         IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                         IF ((T_Fd[y_1,x_1].S = 0) OR (T_Fd[y_1,x_1].E > 0))
                         THEN CONTINUE;
                         Bereits_eingetragen := FALSE;
                         FOR l := 1 TO i DO
                          IF ((RVFd[Spieler,l].y = y_1)
                              AND
                              (RVFd[Spieler,l].x = x_1)) THEN
                          BEGIN
                               Bereits_eingetragen := TRUE;
                               BREAK;
                          END;
                         IF (Bereits_eingetragen = FALSE) THEN
                         BEGIN
                              i := i+1;
                              RVFd[Spieler,i].y := y_1;
                              RVFd[Spieler,i].x := x_1;
                              T_RWS := T_RWS+T_Fd[y_1,x_1].P;
                         END;
                    END;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    y_1 := T_Fr[3-Spieler,T_F].y+RS_y(T_R,1);
                    x_1 := T_Fr[3-Spieler,T_F].x+RS_x(T_R,1);
                    IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                    IF ((T_Fd[y_1,x_1].S = 0) OR (T_Fd[y_1,x_1].E > 0))
                    THEN CONTINUE;
                    Bereits_eingetragen := FALSE;
                    FOR l := 1 TO j DO
                     IF ((RVFd[3-Spieler,l].y = y_1)
                         AND
                         (RVFd[3-Spieler,l].x = x_1)) THEN
                     BEGIN
                          Bereits_eingetragen := TRUE;
                          BREAK;
                     END;
                    IF (Bereits_eingetragen = FALSE) THEN
                    BEGIN
                         j := j+1;
                         RVFd[3-Spieler,j].y := y_1;
                         RVFd[3-Spieler,j].x := x_1;
                         T_RWK := T_RWK+T_Fd[y_1,x_1].P;
                    END;
               END;
          END;
          m := 1;
          REPEAT
                FOR T_R := 1 TO 6 DO
                BEGIN
                     y_1 := RVFd[Spieler,m].y+RS_y(T_R,1);
                     x_1 := RVFd[Spieler,m].x+RS_x(T_R,1);
                     IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                     IF ((T_Fd[y_1,x_1].S = 0) OR (T_Fd[y_1,x_1].E > 0))
                     THEN CONTINUE;
                     Bereits_eingetragen := FALSE;
                     FOR l := 1 TO i DO
                      IF ((RVFd[Spieler,l].y = y_1)
                          AND
                          (RVFd[Spieler,l].x = x_1)) THEN
                      BEGIN
                           Bereits_eingetragen := TRUE;
                           BREAK;
                      END;
                     IF (Bereits_eingetragen = FALSE) THEN
                     BEGIN
                          i := i+1;
                          RVFd[Spieler,i].y := y_1;
                          RVFd[Spieler,i].x := x_1;
                          T_RWS := T_RWS+T_Fd[y_1,x_1].P;
                     END;
                END;
                m := m+1;
          UNTIL (m > i);
          m := 1;
          REPEAT
                FOR T_R := 1 TO 6 DO
                BEGIN
                     y_1 := RVFd[3-Spieler,m].y+RS_y(T_R,1);
                     x_1 := RVFd[3-Spieler,m].x+RS_x(T_R,1);
                     IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                     IF ((T_Fd[y_1,x_1].S = 0) OR (T_Fd[y_1,x_1].E > 0))
                     THEN CONTINUE;
                     Bereits_eingetragen := FALSE;
                     FOR l := 1 TO j DO
                      IF ((RVFd[3-Spieler,l].y = y_1)
                          AND
                          (RVFd[3-Spieler,l].x = x_1)) THEN
                      BEGIN
                           Bereits_eingetragen := TRUE;
                           BREAK;
                      END;
                     IF (Bereits_eingetragen = FALSE) THEN
                     BEGIN
                          j := j+1;
                          RVFd[3-Spieler,j].y := y_1;
                          RVFd[3-Spieler,j].x := x_1;
                          T_RWK := T_RWK+T_Fd[y_1,x_1].P;
                     END;
                END;
                m := m+1;
          UNTIL (m > j);
{}        IF (Tiefe = 1) THEN Log('T_RWK = '+IntToStr(T_RWK)
                                  +'   T_RWS = '+IntToStr(T_RWS));
          IF ((-6 < T_RWS-ARWS-T_RWK+ARWK)
              AND
              (T_RWS-ARWS-T_RWK+ARWK < 7)
              AND
              (bf_FdA > 17))
          THEN  RV[Tiefe,Spieler,T_F2,T_R2,T_E2] := 0
          ELSE  RV[Tiefe,Spieler,T_F2,T_R2,T_E2] := T_RWS-ARWS-T_RWK+ARWK;
          { Verteilungen der neuen Stellungen bewerten: }
          FOR y := 0 TO  7 DO
          FOR x := 0 TO 10 DO
          BEGIN
               VFd[y,x].A := 0;
               VFd[y,x].G := 0;
               IF ((T_Fd[y,x].S = 0) OR (T_Fd[y,x].E = Spieler-3))
               THEN CONTINUE;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    U[T_R] := FALSE;
                    y_1 := y+RS_y(T_R,1);
                    x_1 := x+RS_x(T_R,1);
                    IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                    IF ((T_Fd[y_1,x_1].S = 1) AND (SFd[Tiefe,y_1,x_1].E = 0))
                    THEN U[T_R] := TRUE;
               END;
               VFd[y,x].A := dm_Art(U);
          END;
          FOR i := 1 TO 20 DO
          BEGIN
               Gruppe[i].EK  := 0;
               Gruppe[i].ES  := 0;
               Gruppe[i].FdA := 0;
               Gruppe[i].PA  := 0;
               Gruppe[i].SP  := 0;
          END;
          { Felder von der Art 3 gruppieren: }
          T_G := 0;
          FOR y := 0 TO  7 DO
          FOR x := 0 TO 10 DO
          BEGIN
               IF ((VFd[y,x].A < 3) OR (VFd[y,x].G > 0)) THEN CONTINUE;
               T_G := T_G+1;
               FOR i := 2 TO 52 DO
               BEGIN
                    GFd[i].y := -10;
                    GFd[i].x := -10;
               END;
               VFd[y,x].G := T_G;
               Gruppe[T_G].FdA := 1;
               IF (T_Fd[y,x].E = 0) THEN Gruppe[T_G].PA := T_Fd[y,x].P;
               GFd[1].y := y;
               GFd[1].x := x;
               i := 1;
               j := 1;
               REPEAT
                     FOR T_R := 1 TO 6 DO
                     BEGIN
                          y_1 := GFd[j].y+RS_y(T_R,1);
                          x_1 := GFd[j].x+RS_x(T_R,1);
                          IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                          IF ((T_Fd[y_1,x_1].S = 0)
                              OR
                              (T_Fd[y_1,x_1].E = 3-Spieler)
                              OR
                              (VFd[y_1,x_1].A < 3)
                              OR
                              (VFd[y_1,x_1].G > 0)) THEN CONTINUE;
                          VFd[y_1,x_1].G := T_G;
                          Gruppe[T_G].FdA := Gruppe[T_G].FdA+1;
                          IF (T_Fd[y_1,x_1].E = 0)
                          THEN Gruppe[T_G].PA
                                := Gruppe[T_G].PA +T_Fd[y_1,x_1].P;
                          i := i+1;
                          GFd[i].y := y_1;
                          GFd[i].x := x_1;
                     END;
                     j := j+1;
               UNTIL (j > i);
          END;
          { Nachbarfelder der Gruppen zu Gruppen hinzuf�gen: }
          FOR y := 0 TO  7 DO
          FOR x := 0 TO 10 DO
          BEGIN
               IF ((NOT((VFd[y,x].A = 1) OR (VFd[y,x].A = 2)))
                   OR
                   (T_Fd[y,x].S = 0)
                   OR
                   (T_Fd[y,x].E = 3-Spieler)) THEN CONTINUE;
               NFG := -10;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    y_1 := y+RS_y(T_R,1);
                    x_1 := x+RS_x(T_R,1);
                    IF ((VFd[y_1,x_1].G > 0) AND (VFd[y_1,x_1].A = 3))
                    THEN IF (((NFG > 0) AND (NFG <> VFd[y_1,x_1].G))
                             OR
                             (NFG = 0))
                         THEN NFG := 0
                         ELSE NFG := VFd[y_1,x_1].G;
               END;
               IF (NFG > 0) THEN
               BEGIN
                    VFd[y,x].G     := NFG;
                    Gruppe[NFG].PA := Gruppe[NFG].PA+T_Fd[y,x].P;
               END;
          END;
          { Erreichbarkeiten des Spielers ermitteln: }
          FOR T_F := 1 TO 4 DO
          BEGIN
               FOR i := 1 TO T_G DO
               BEGIN
                    GG[i] := 0;
                    GR[i] := 0;
               END;
               j := 1;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    FOR T_E := 1 TO 7 DO
                    BEGIN
                         y := T_Fr[Spieler,T_F].y+RS_y(T_R,T_E);
                         x := T_Fr[Spieler,T_F].x+RS_x(T_R,T_E);
                         IF (EFd(y,x) = FALSE) THEN BREAK;
                         IF ((T_Fd[y,x].S = 0) OR (SFd[Tiefe,y,x].E > 0))
                         THEN BREAK;
                         IF (VFd[y,x].G = 0) THEN CONTINUE;
                         Bereits_eingetragen := FALSE;
                         FOR i := 1 TO T_G DO
                         BEGIN
                              IF (GG[i] = VFd[y,x].G) THEN
                              BEGIN
                                   Bereits_eingetragen := TRUE;
                                   {
                                   IF ((GR[i] <> 0)
                                       AND
                                       (GR[i] <> T_R)) THEN
                                   BEGIN
                                        dm_Eigentumsaufstockung(VFd[y,x].G
                                                                ,1,TRUE);
                                        GR[i] := 0;
                                        Bereits_eingetragen := TRUE;
                                        BREAK;
                                   END;
                                   }
                                   BREAK;
                              END;
                         END;
                         IF (Bereits_eingetragen = FALSE) THEN
                         BEGIN
                              GG[j] := VFd[y,x].G;
                              GR[j] := T_R;
                              j := j+1;
                         END;
                    END;
               END;
               FOR i := 1 TO T_G DO
                IF ((GG[i] > 0) AND (GR[i] > 0))
                THEN dm_Eigentumsaufstockung(GG[i],1,FALSE);
               y := T_Fr[Spieler,T_F].y;
               x := T_Fr[Spieler,T_F].x;
               IF (VFd[y,x].G > 0) THEN
               BEGIN
                    Bereits_eingetragen := FALSE;
                    FOR i := 1 TO T_G DO
                     IF ((GG[i] = VFd[y,x].G) AND (GR[i] = 0))
                     THEN Bereits_eingetragen := TRUE;
                    IF (Bereits_eingetragen = FALSE)
                    THEN dm_Eigentumsaufstockung(VFd[y,x].G,1,TRUE);
               END;
          END;
          { Erreichbarkeiten des Konkurrenten ermitteln: }
          FOR T_F := 1 TO 4 DO
          BEGIN
               FOR i := 1 TO T_G DO
               BEGIN
                    GG[i] := 0;
                    GR[i] := 0;
               END;
               j := 1;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    FOR T_E := 1 TO 7 DO
                    BEGIN
                         y := T_Fr[3-Spieler,T_F].y+RS_y(T_R,T_E);
                         x := T_Fr[3-Spieler,T_F].x+RS_x(T_R,T_E);
                         IF (EFd(y,x) = FALSE) THEN BREAK;
                     IF ((T_Fd[y,x].S = 0) OR (SFd[Tiefe,y,x].E > 0))
                         THEN BREAK;
                         IF (VFd[y,x].G = 0) THEN CONTINUE;
                         Bereits_eingetragen := FALSE;
                         FOR i := 1 TO T_G DO
                         BEGIN
                              IF (GG[i] = VFd[y,x].G) THEN
                              BEGIN
                                   Bereits_eingetragen := TRUE;
                                   {
                                   IF ((GR[i] <> 0)
                                       AND
                                       (GR[i] <> T_R)) THEN
                                   BEGIN
                                        dm_Eigentumsaufstockung(VFd[y,x].G
                                                                ,2,TRUE);
                                        GR[i] := 0;
                                        Bereits_eingetragen := TRUE;
                                        BREAK;
                                   END;
                                   }
                                   BREAK;
                              END;
                         END;
                         IF (Bereits_eingetragen = FALSE) THEN
                         BEGIN
                              GG[j] := VFd[y,x].G;
                              GR[j] := T_R;
                              j := j+1;
                         END;
                    END;
               END;
               FOR i := 1 TO T_G DO
                IF ((GG[i] > 0) AND (GR[i] > 0))
                THEN dm_Eigentumsaufstockung(GG[i],2,FALSE);
          END;
          {    Gro�e Gruppen zerteilen: }
          FOR i := 1 TO T_G DO
          BEGIN
               IF (Gruppe[i].FdA < 22) THEN CONTINUE;
               FOR j := 1 TO 56 DO
               BEGIN
                    U_Fd[j].y := -10;
                    U_Fd[j].x := -10;
                    V_Fd[j].y := -10;
                    V_Fd[j].x := -10;
               END;
               VP := 0;
               UP := 0;
               FOR T_F := 1 TO 4 DO
                IF (VFd[T_Fr[Spieler,T_F].y
                        ,T_Fr[Spieler,T_F].x].G = i) THEN
                BEGIN
                     { Welle: }
                     FOR y := 0 TO  7 DO
                     FOR x := 0 TO 10 DO
                     BEGIN
                          WEFd[y,x].E := T_Fd[y,x].E;
                          WEFd[y,x].S := T_Fd[y,x].S;
                          WEFd[y,x].I := 0;
                     END;
                     FOR T_R3 := 1 TO 6 DO
                     FOR T_E3 := 1 TO 7 DO RE[T_R3,T_E3] := FALSE;
                     FOR T_R3 := 1 TO 6 DO
                     FOR T_E3 := 1 TO 7 DO
                     BEGIN
                          y := T_Fr[Spieler,T_F].y+RS_y(T_R3,T_E3);
                          x := T_Fr[Spieler,T_F].x+RS_x(T_R3,T_E3);
                          IF (EFd(y,x) = FALSE) THEN BREAK;
                          IF ((WEFd[y,x].S = 0) OR (WEFd[y,x].E > 0))
                          THEN BREAK;
                          dm_Aufstockung(WEFd,y,x);
                          RE[T_R3,T_E3] := TRUE;
                     END;
                     FOR T_Sektor := 1 TO 6 DO
                      dm_Sektorwelle(WEFd,T_Fr[Spieler,T_F].y
                                         ,T_Fr[Spieler,T_F].x,T_Sektor);
                     { Nach der Welle: }
                     FOR y := 0 TO  7 DO
                     FOR x := 0 TO 10 DO
                     BEGIN
                          IF (WEFd[y,x].S = 0) THEN CONTINUE;
                          IF (WEFd[y,x].E > 0) THEN CONTINUE;
                          IF (VFd[y,x].G <> i) THEN CONTINUE;
                          IF (WEFd[y,x].I = 0) THEN CONTINUE;
                          FOR j := 1 TO 4 DO
                           IF ((VFd[T_Fr[3-Spieler,j].y
                                    ,T_Fr[3-Spieler,j].x].G = i)
                               AND
                               (dm_Feldabstand(T_Fr[Spieler,T_F].y
                                               ,T_Fr[Spieler,T_F].x
                                               ,y,x)
                                >
                                dm_Feldabstand(T_Fr[Spieler,T_F].y
                                               ,T_Fr[Spieler,T_F].x
                                               ,T_Fr[3-Spieler,j].y
                                               ,T_Fr[3-Spieler,j].x))
                               AND
                               (dm_Feldabstand(T_Fr[Spieler,T_F].y
                                               ,T_Fr[Spieler,T_F].x
                                               ,y,x)
                                >
                                dm_Feldabstand(T_Fr[3-Spieler,j].y
                                               ,T_Fr[3-Spieler,j].x
                                               ,y,x))) THEN CONTINUE;
                          Bereits_eingetragen := FALSE;
                          FOR j := 1 TO 56 DO
                           IF ((V_Fd[j].y = y)
                               AND
                               (V_Fd[j].x = x)) THEN
                           BEGIN
                                Bereits_eingetragen := TRUE;
                                BREAK;
                           END;
                          IF (Bereits_eingetragen = TRUE) THEN CONTINUE;
                          j := 0;
                          REPEAT
                                j := j+1;
                          UNTIL ((V_Fd[j].y = -10)
                                 AND
                                 (V_Fd[j].x = -10));
                          V_Fd[j].y := y;
                          V_Fd[j].x := x;
                          VP := VP+T_Fd[y,x].P;
                     END;
                END ELSE
                BEGIN
                     FOR T_R3 := 1 TO 6 DO
                     FOR T_E3 := 1 TO 7 DO
                     BEGIN
                          y_3 := T_Fr[Spieler,T_F].y+RS_y(T_R3,T_E3);
                          x_3 := T_Fr[Spieler,T_F].x+RS_x(T_R3,T_E3);
                          IF (EFd(y_3,x_3) = FALSE)  THEN BREAK;
                          IF ((WEFd[y_3,x_3].S = 0)
                              OR
                              (WEFd[y_3,x_3].E > 0)) THEN BREAK;
                          IF (VFd[y_3,x_3].G <> i)   THEN BREAK;
                          FOR T_R4 := 1 TO 6 DO
                          FOR T_E4 := 1 TO 7 DO
                          BEGIN
                               y_4 := y_3+RS_y(T_R4,T_E4);
                               x_4 := x_3+RS_x(T_R4,T_E4);
                               IF (EFd(y_4,x_4) = FALSE)  THEN BREAK;
                               IF ((WEFd[y_4,x_4].S = 0)
                                   OR
                                   (WEFd[y_4,x_4].E > 0)) THEN BREAK;
                               IF (VFd[y_4,x_4].G <> i)   THEN BREAK;
                               Bereits_eingetragen := FALSE;
                               FOR j := 1 TO 21 DO
                               IF ((U_Fd[j].y = y_4)
                                   AND
                                   (U_Fd[j].x = x_4)) THEN
                               BEGIN
                                    Bereits_eingetragen := TRUE;
                                    BREAK;
                               END;
                               IF (Bereits_eingetragen = TRUE) THEN BREAK;
                               j := 0;
                               REPEAT
                                     j := j+1;
                               UNTIL ((U_Fd[j].y = -10)
                                      AND
                                      (U_Fd[j].x = -10));
                               U_Fd[j].y := y_4;
                               U_Fd[j].x := x_4;
                               UP := UP+T_Fd[y_4,x_4].P;
                          END;
                     END;
                END;
               Gruppe[i].SP
                := (VP+UP/2)*Gruppe[i].ES/(Gruppe[i].ES+Gruppe[i].EK);
          END;
          { T_VS ermitteln: }
          T_VS := 0;
          FOR i := 1 TO T_G DO
           IF (Gruppe[i].ES+Gruppe[i].EK > 0)
           THEN IF (Gruppe[i].SP = 0)
                THEN T_VS := T_VS+Gruppe[i].PA
                                  *Gruppe[i].ES/(Gruppe[i].ES+Gruppe[i].EK)
                ELSE T_VS := T_VS+Gruppe[i].SP;
          FOR y := 0 TO  7 DO
          FOR x := 0 TO 10 DO
          BEGIN
               VFd[y,x].A := 0;
               VFd[y,x].G := 0;
               IF ((T_Fd[y,x].S = 0) OR (T_Fd[y,x].E = Spieler))
               THEN CONTINUE;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    U[T_R] := FALSE;
                    y_1 := y+RS_y(T_R,1);
                    x_1 := x+RS_x(T_R,1);
                    IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                    IF ((T_Fd[y_1,x_1].S = 1) AND (T_Fd[y_1,x_1].E = 0))
                    THEN U[T_R] := TRUE;
               END;
               VFd[y,x].A := dm_Art(U);
          END;
          FOR i := 1 TO 20 DO
          BEGIN
               Gruppe[i].EK  := 0;
               Gruppe[i].ES  := 0;
               Gruppe[i].FdA := 0;
               Gruppe[i].PA  := 0;
               Gruppe[i].SP  := 0;
          END;
          { Felder von der Art 3 gruppieren: }
          T_G := 0;
          FOR y := 0 TO  7 DO
          FOR x := 0 TO 10 DO
          BEGIN
               IF ((VFd[y,x].A < 3) OR (VFd[y,x].G > 0)) THEN CONTINUE;
               T_G := T_G+1;
               FOR i := 2 TO 52 DO
               BEGIN
                    GFd[i].y := -10;
                    GFd[i].x := -10;
               END;
               VFd[y,x].G := T_G;
               Gruppe[T_G].FdA := 1;
               IF (T_Fd[y,x].E = 0) THEN Gruppe[T_G].PA := T_Fd[y,x].P;
               GFd[1].y := y;
               GFd[1].x := x;
               i := 1;
               j := 1;
               REPEAT
                     FOR T_R := 1 TO 6 DO
                     BEGIN
                          y_1 := GFd[j].y+RS_y(T_R,1);
                          x_1 := GFd[j].x+RS_x(T_R,1);
                          IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
                          IF ((T_Fd[y_1,x_1].S = 0)
                              OR
                              (T_Fd[y_1,x_1].E = Spieler)
                              OR
                              (VFd[y_1,x_1].A < 3)
                              OR
                              (VFd[y_1,x_1].G > 0)) THEN CONTINUE;
                          VFd[y_1,x_1].G := T_G;
                          Gruppe[T_G].FdA := Gruppe[T_G].FdA+1;
                          IF (T_Fd[y_1,x_1].E = 0)
                          THEN Gruppe[T_G].PA
                                := Gruppe[T_G].PA +T_Fd[y_1,x_1].P;
                          i := i+1;
                          GFd[i].y := y_1;
                          GFd[i].x := x_1;
                     END;
                     j := j+1;
               UNTIL (j > i);
          END;
          { Nachbarfelder der Gruppen zu Gruppen hinzuf�gen: }
          FOR y := 0 TO  7 DO
          FOR x := 0 TO 10 DO
          BEGIN
               IF ((NOT((VFd[y,x].A = 1) OR (VFd[y,x].A = 2)))
                   OR
                   (T_Fd[y,x].S = 0)
                   OR
                   (T_Fd[y,x].E = Spieler)) THEN CONTINUE;
               NFG := -10;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    y_1 := y+RS_y(T_R,1);
                    x_1 := x+RS_x(T_R,1);
                    IF ((VFd[y_1,x_1].G > 0) AND (VFd[y_1,x_1].A = 3))
                    THEN IF (((NFG > 0) AND (NFG <> VFd[y_1,x_1].G))
                             OR
                             (NFG = 0))
                         THEN NFG := 0
                         ELSE NFG := VFd[y_1,x_1].G;
               END;
               IF (NFG > 0) THEN
               BEGIN
                    VFd[y,x].G     := NFG;
                    Gruppe[NFG].PA := Gruppe[NFG].PA+T_Fd[y,x].P;
               END;
          END;
          { Erreichbarkeiten des Spielers ermitteln: }
          FOR T_F := 1 TO 4 DO
          BEGIN
               FOR i := 1 TO T_G DO
               BEGIN
                    GG[i] := 0;
                    GR[i] := 0;
               END;
               j := 1;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    FOR T_E := 1 TO 7 DO
                    BEGIN
                         y := T_Fr[Spieler,T_F].y+RS_y(T_R,T_E);
                         x := T_Fr[Spieler,T_F].x+RS_x(T_R,T_E);
                         IF (EFd(y,x) = FALSE) THEN BREAK;
                         IF ((T_Fd[y,x].S = 0) OR (T_Fd[y,x].E > 0))
                         THEN BREAK;
                         IF (VFd[y,x].G = 0) THEN CONTINUE;
                         Bereits_eingetragen := FALSE;
                         FOR i := 1 TO T_G DO
                         BEGIN
                              IF (GG[i] = VFd[y,x].G) THEN
                              BEGIN
                                   Bereits_eingetragen := TRUE;
                                   {
                                   IF ((GR[i] <> 0)
                                       AND
                                       (GR[i] <> T_R)) THEN
                                   BEGIN
                                        dm_Eigentumsaufstockung(VFd[y,x].G
                                                                ,1,TRUE);
                                        GR[i] := 0;
                                        Bereits_eingetragen := TRUE;
                                        BREAK;
                                   END;
                                   }
                                   BREAK;
                              END;
                         END;
                         IF (Bereits_eingetragen = FALSE) THEN
                         BEGIN
                              GG[j] := VFd[y,x].G;
                              GR[j] := T_R;
                              j := j+1;
                         END;
                    END;
               END;
               FOR i := 1 TO T_G DO
                IF ((GG[i] > 0) AND (GR[i] > 0))
                THEN dm_Eigentumsaufstockung(GG[i],1,FALSE);
          END;
          { Erreichbarkeiten des Konkurrenten ermitteln: }
          FOR T_F := 1 TO 4 DO
          BEGIN
               FOR i := 1 TO T_G DO
               BEGIN
                    GG[i] := 0;
                    GR[i] := 0;
               END;
               j := 1;
               FOR T_R := 1 TO 6 DO
               BEGIN
                    FOR T_E := 1 TO 7 DO
                    BEGIN
                         y := T_Fr[3-Spieler,T_F].y+RS_y(T_R,T_E);
                         x := T_Fr[3-Spieler,T_F].x+RS_x(T_R,T_E);
                         IF (EFd(y,x) = FALSE) THEN BREAK;
                     IF ((T_Fd[y,x].S = 0) OR (T_Fd[y,x].E > 0))
                         THEN BREAK;
                         IF (VFd[y,x].G = 0) THEN CONTINUE;
                         Bereits_eingetragen := FALSE;
                         FOR i := 1 TO T_G DO
                         BEGIN
                              IF (GG[i] = VFd[y,x].G) THEN
                              BEGIN
                                   Bereits_eingetragen := TRUE;
                                   {
                                   IF ((GR[i] <> 0)
                                       AND
                                       (GR[i] <> T_R)) THEN
                                   BEGIN
                                        dm_Eigentumsaufstockung(VFd[y,x].G
                                                                ,2,TRUE);
                                        GR[i] := 0;
                                        Bereits_eingetragen := TRUE;
                                        BREAK;
                                   END;
                                   }
                                   BREAK;
                              END;
                         END;
                         IF (Bereits_eingetragen = FALSE) THEN
                         BEGIN
                              GG[j] := VFd[y,x].G;
                              GR[j] := T_R;
                              j := j+1;
                         END;
                    END;
               END;
               FOR i := 1 TO T_G DO
                IF ((GG[i] > 0) AND (GR[i] > 0))
                THEN dm_Eigentumsaufstockung(GG[i],2,FALSE);
               y := T_Fr[3-Spieler,T_F].y;
               x := T_Fr[3-Spieler,T_F].x;
               IF (VFd[y,x].G > 0) THEN
               BEGIN
                    Bereits_eingetragen := FALSE;
                    FOR i := 1 TO T_G DO
                     IF ((GG[i] = VFd[y,x].G) AND (GR[i] = 0))
                     THEN Bereits_eingetragen := TRUE;
                    IF (Bereits_eingetragen = FALSE)
                    THEN dm_Eigentumsaufstockung(VFd[y,x].G,2,TRUE);
               END;
          END;
          {    Gro�e Gruppen zerteilen: }
          FOR i := 1 TO T_G DO
          BEGIN
               IF (Gruppe[i].FdA < 22) THEN CONTINUE;
               FOR j := 1 TO 56 DO
               BEGIN
                    U_Fd[j].y := -10;
                    U_Fd[j].x := -10;
                    V_Fd[j].y := -10;
                    V_Fd[j].x := -10;
               END;
               VP := 0;
               UP := 0;
               FOR T_F := 1 TO 4 DO
                IF (VFd[T_Fr[3-Spieler,T_F].y
                        ,T_Fr[3-Spieler,T_F].x].G = i) THEN
                BEGIN
                     { Welle: }
                     FOR y := 0 TO  7 DO
                     FOR x := 0 TO 10 DO
                     BEGIN
                          WEFd[y,x].E := T_Fd[y,x].E;
                          WEFd[y,x].S := T_Fd[y,x].S;
                          WEFd[y,x].I := 0;
                     END;
                     FOR T_R3 := 1 TO 6 DO
                     FOR T_E3 := 1 TO 7 DO RE[T_R3,T_E3] := FALSE;
                     FOR T_R3 := 1 TO 6 DO
                     FOR T_E3 := 1 TO 7 DO
                     BEGIN
                          y := T_Fr[3-Spieler,T_F].y+RS_y(T_R3,T_E3);
                          x := T_Fr[3-Spieler,T_F].x+RS_x(T_R3,T_E3);
                          IF (EFd(y,x) = FALSE) THEN BREAK;
                          IF ((WEFd[y,x].S = 0) OR (WEFd[y,x].E > 0))
                          THEN BREAK;
                          dm_Aufstockung(WEFd,y,x);
                          RE[T_R3,T_E3] := TRUE;
                     END;
                     FOR T_Sektor := 1 TO 6 DO
                      dm_Sektorwelle(WEFd,T_Fr[3-Spieler,T_F].y
                                         ,T_Fr[3-Spieler,T_F].x,T_Sektor);
                     { Nach der Welle: }
                     FOR y := 0 TO  7 DO
                     FOR x := 0 TO 10 DO
                     BEGIN
                          IF (WEFd[y,x].S = 0) THEN CONTINUE;
                          IF (WEFd[y,x].E > 0) THEN CONTINUE;
                          IF (VFd[y,x].G <> i) THEN CONTINUE;
                          IF (WEFd[y,x].I = 0) THEN CONTINUE;
                          FOR j := 1 TO 4 DO
                           IF ((VFd[T_Fr[Spieler,j].y
                                    ,T_Fr[Spieler,j].x].G = i)
                               AND
                               (dm_Feldabstand(T_Fr[3-Spieler,T_F].y
                                               ,T_Fr[3-Spieler,T_F].x
                                               ,y,x)
                                >
                                dm_Feldabstand(T_Fr[3-Spieler,T_F].y
                                               ,T_Fr[3-Spieler,T_F].x
                                               ,T_Fr[Spieler,j].y
                                               ,T_Fr[Spieler,j].x))
                               AND
                               (dm_Feldabstand(T_Fr[3-Spieler,T_F].y
                                               ,T_Fr[3-Spieler,T_F].x
                                               ,y,x)
                                >
                                dm_Feldabstand(T_Fr[Spieler,j].y
                                               ,T_Fr[Spieler,j].x
                                               ,y,x))) THEN CONTINUE;
                          Bereits_eingetragen := FALSE;
                          FOR j := 1 TO 56 DO
                           IF ((V_Fd[j].y = y)
                               AND
                               (V_Fd[j].x = x)) THEN
                           BEGIN
                                Bereits_eingetragen := TRUE;
                                BREAK;
                           END;
                          IF (Bereits_eingetragen = TRUE) THEN CONTINUE;
                          j := 0;
                          REPEAT
                                j := j+1;
                          UNTIL ((V_Fd[j].y = -10)
                                 AND
                                 (V_Fd[j].x = -10));
                          V_Fd[j].y := y;
                          V_Fd[j].x := x;
                          VP := VP+T_Fd[y,x].P;
                     END;
                END ELSE
                BEGIN
                     FOR T_R3 := 1 TO 6 DO
                     FOR T_E3 := 1 TO 7 DO
                     BEGIN
                          y_3 := T_Fr[3-Spieler,T_F].y+RS_y(T_R3,T_E3);
                          x_3 := T_Fr[3-Spieler,T_F].x+RS_x(T_R3,T_E3);
                          IF (EFd(y_3,x_3) = FALSE)  THEN BREAK;
                          IF ((WEFd[y_3,x_3].S = 0)
                              OR
                              (WEFd[y_3,x_3].E > 0)) THEN BREAK;
                          IF (VFd[y_3,x_3].G <> i)   THEN BREAK;
                          FOR T_R4 := 1 TO 6 DO
                          FOR T_E4 := 1 TO 7 DO
                          BEGIN
                               y_4 := y_3+RS_y(T_R4,T_E4);
                               x_4 := x_3+RS_x(T_R4,T_E4);
                               IF (EFd(y_4,x_4) = FALSE)  THEN BREAK;
                               IF ((WEFd[y_4,x_4].S = 0)
                                   OR
                                   (WEFd[y_4,x_4].E > 0)) THEN BREAK;
                               IF (VFd[y_4,x_4].G <> i)   THEN BREAK;
                               Bereits_eingetragen := FALSE;
                               FOR j := 1 TO 21 DO
                               IF ((U_Fd[j].y = y_4)
                                   AND
                                   (U_Fd[j].x = x_4)) THEN
                               BEGIN
                                    Bereits_eingetragen := TRUE;
                                    BREAK;
                               END;
                               IF (Bereits_eingetragen = TRUE) THEN BREAK;
                               j := 0;
                               REPEAT
                                     j := j+1;
                               UNTIL ((U_Fd[j].y = -10)
                                      AND
                                      (U_Fd[j].x = -10));
                               U_Fd[j].y := y_4;
                               U_Fd[j].x := x_4;
                               UP := UP+T_Fd[y_4,x_4].P;
                          END;
                     END;
                END;
               Gruppe[i].SP
                := (VP+UP/2)*Gruppe[i].EK/(Gruppe[i].ES+Gruppe[i].EK);
          END;
          { T_VK ermitteln: }
          T_VK := 0;
          FOR i := 1 TO T_G DO
           IF (Gruppe[i].ES+Gruppe[i].EK > 0)
           THEN IF (Gruppe[i].SP = 0)
                THEN T_VK := T_VK+Gruppe[i].PA
                                  *Gruppe[i].EK/(Gruppe[i].ES+Gruppe[i].EK)
                ELSE T_VK := T_VK+Gruppe[i].SP;
          { VB[Tiefe,Spieler,Figur,Richtung,Entfernung] ermitteln: }
          { IF ((AVK-AVS+T_VS-T_VK) < -0.3)
          THEN VB[Tiefe,Spieler,T_F2,T_R2,T_E2] := -3
          ELSE } VB[Tiefe,Spieler,T_F2,T_R2,T_E2] := AVK-AVS+T_VS-T_VK;
     END;
{ |<-- }
     END;
END;
{ ... bis hier zum zweiten Mal neu. }


PROCEDURE dm_NFdU(Tiefe,Spieler,T_F : INTEGER;
                  VAR j             : INTEGER;
                  y,x               : INTEGER);
{ 'NFdU' bedeutet 'Nachbarfelduntersuchung'.           }
VAR Bereits_eingetragen : BOOLEAN;
VAR i,T_R,x_1,y_1       : INTEGER;
BEGIN
     FOR T_R := 1 TO 6 DO
     BEGIN
          y_1 := y+RS_y(T_R,1);
          x_1 := x+RS_x(T_R,1);
          IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
          IF ((SFd[Tiefe,y_1,x_1].S = Vorhanden)
              AND
              (SFd[Tiefe,y_1,x_1].E = 0)) THEN
          BEGIN
               Bereits_eingetragen := FALSE;
               FOR i := 1 TO j-1 DO
                IF ((RFd[Tiefe,Spieler,T_F,i].y = y_1)
                    AND
                    (RFd[Tiefe,Spieler,T_F,i].x = x_1))
                THEN Bereits_eingetragen := TRUE;
               IF (Bereits_eingetragen = TRUE) THEN CONTINUE;
               RFd[Tiefe,Spieler,T_F,j].y := y_1;
               RFd[Tiefe,Spieler,T_F,j].x := x_1;
               RFd[Tiefe,Spieler,T_F,j].P := SFd[Tiefe,y_1,x_1].P;
               Revierfigur[Tiefe,Spieler,T_F].RPA
                := Revierfigur[Tiefe,Spieler,T_F].RPA+SFd[Tiefe,y_1,x_1].P;
               j := j+1;
          END;
          IF ((SFd[Tiefe,y_1,x_1].S = Vorhanden)
              AND
              (SFd[Tiefe,y_1,x_1].E = Spieler)
              AND
              (SFd[Tiefe,y_1,x_1].FrN <> T_F))
          THEN FOR i := 1 TO 3 DO
               BEGIN
                    IF (Revierfigur[Tiefe,Spieler,T_F].Helfer[i]
                        =
                        SFd[Tiefe,y_1,x_1].FrN) THEN BREAK;
                    IF (Revierfigur[Tiefe,Spieler,T_F].Helfer[i] = 0) THEN
                    BEGIN
                         Revierfigur[Tiefe,Spieler,T_F].Helfer[i]
                          := SFd[Tiefe,y_1,x_1].FrN;
                         BREAK;
                    END;
               END;
          IF ((SFd[Tiefe,y_1,x_1].S = Vorhanden)
              AND
              (SFd[Tiefe,y_1,x_1].E = 3-Spieler)) THEN
          BEGIN
               Revierfigur[Tiefe,Spieler,T_F].Wichtigkeit := TRUE;
               FOR i := 1 TO 4 DO
               BEGIN
                    IF (Revierfigur[Tiefe,Spieler,T_F].Konkurrent[i]
                        =
                        SFd[Tiefe,y_1,x_1].FrN) THEN BREAK;
                    IF (Revierfigur[Tiefe,Spieler,T_F].Konkurrent[i] = 0) THEN
                    BEGIN
                         Revierfigur[Tiefe,Spieler,T_F].Konkurrent[i]
                          := SFd[Tiefe,y_1,x_1].FrN;
                         BREAK;
                    END;
               END;
          END;
     END;
END;


PROCEDURE dm_NFdUK(Tiefe,Spieler,T_F : INTEGER;
                   VAR j             : INTEGER;
                   VAR GRFd_gefunden : BOOLEAN;
                   y,x               : INTEGER);
VAR Bereits_eingetragen : BOOLEAN;
VAR i,T_R,x_1,y_1       : INTEGER;
BEGIN
     FOR T_R := 1 TO 6 DO
     BEGIN
          y_1 := y+RS_y(T_R,1);
          x_1 := x+RS_x(T_R,1);
          IF (EFd(y_1,x_1) = FALSE) THEN CONTINUE;
          FOR i := 1 TO Revierfigur[Tiefe,Spieler,T_F].RFdA DO
           IF ((RFd[Tiefe,Spieler,T_F,i].y = y_1)
               AND
               (RFd[Tiefe,Spieler,T_F,i].x = x_1)) THEN
           BEGIN
                GRFd_gefunden := TRUE;
                BREAK;
           END;
          IF (GRFd_gefunden = TRUE) THEN BREAK;
          IF ((SFd[Tiefe,y_1,x_1].S = Vorhanden)
              AND
              (SFd[Tiefe,y_1,x_1].E = 0)) THEN
          BEGIN
               Bereits_eingetragen := FALSE;
               FOR i := 1 TO j-1 DO
                IF ((RFdK[i].y = y_1) AND (RFdK[i].x = x_1))
                THEN Bereits_eingetragen := TRUE;
               IF (Bereits_eingetragen = TRUE) THEN CONTINUE;
               RFdK[j].y := y_1;
               RFdK[j].x := x_1;
               j := j+1;
          END;
     END;
END;


PROCEDURE dm_Reviersuche(Tiefe,Spieler : INTEGER);
VAR Nachbarn_gefunden,GRFd_gefunden : BOOLEAN;
{ 'GRFd' bedeutet 'Gemeinsames_Revierfeld'.            }
VAR i,j,k,T_F,T_K,T_R               : INTEGER;
BEGIN
     FOR T_F := 1 TO 4 DO
     BEGIN
          Revierfigur[Tiefe,Spieler,T_F].Wichtigkeit := FALSE;
          FOR i := 1 TO 4 DO Revierfigur[Tiefe,Spieler,T_F].Konkurrent[i] := 0;
          FOR i := 1 TO 3 DO Revierfigur[Tiefe,Spieler,T_F].Helfer[i]     := 0;
          Revierfigur[Tiefe,Spieler,T_F].RFdA        := 0;
          Revierfigur[Tiefe,Spieler,T_F].RPA         := 0;
          j := 1;
          dm_NFdU(Tiefe,Spieler,T_F,j
                  ,SFr[Tiefe,Spieler,T_F].y
                  ,SFr[Tiefe,Spieler,T_F].x);
          i := 0;
          WHILE (i+1 <= j-1) DO
          BEGIN
               i := i+1;
               dm_NFdU(Tiefe,Spieler,T_F,j
                       ,RFd[Tiefe,Spieler,T_F,i].y
                       ,RFd[Tiefe,Spieler,T_F,i].x);
          END;
          Revierfigur[Tiefe,Spieler,T_F].RFdA := i;
          FOR k := 1 TO 4 DO
          BEGIN
               T_K := Revierfigur[Tiefe,Spieler,T_F].Konkurrent[k];
               IF (T_K > 0) THEN
               BEGIN
                    Nachbarn_gefunden := FALSE;
                    FOR T_R := 1 TO 6 DO
                     IF ((SFr[Tiefe,3-Spieler,T_K].y+RS_y(T_R,1)
                          =
                          SFr[Tiefe,Spieler,T_F].y)
                         AND
                         (SFr[Tiefe,3-Spieler,T_K].x+RS_x(T_R,1)
                          =
                          SFr[Tiefe,Spieler,T_F].x))
                     THEN Nachbarn_gefunden := TRUE;
                    IF (Nachbarn_gefunden = FALSE) THEN BREAK;
                    GRFd_gefunden     := FALSE;
                    j := 1;
                    dm_NFdUK(Tiefe,Spieler,T_F,j,GRFd_gefunden
                             ,SFr[Tiefe,3-Spieler,T_K].y
                             ,SFr[Tiefe,3-Spieler,T_K].x);
                    IF (GRFd_gefunden = TRUE) THEN BREAK;
                    i := 0;
                    WHILE (i+1 <= j-1) DO
                    BEGIN
                         i := i+1;
                         dm_NFdUK(Tiefe,Spieler,T_F,j,GRFd_gefunden
                                  ,RFdK[i].y
                                  ,RFdK[i].x);
                         IF (GRFd_gefunden = TRUE) THEN BREAK;
                    END;
                    IF (GRFd_gefunden = TRUE) THEN BREAK;
               END;
          END;
          IF ((Nachbarn_gefunden = FALSE) OR (GRFd_gefunden = TRUE))
          THEN CONTINUE;
          Revierfigur[Tiefe,Spieler,T_F].Wichtigkeit := FALSE;
     END;
     IF ((Revierfigur[Tiefe,Spieler,1].Wichtigkeit = FALSE)
         AND
         (Revierfigur[Tiefe,Spieler,2].Wichtigkeit = FALSE)
         AND
         (Revierfigur[Tiefe,Spieler,3].Wichtigkeit = FALSE)
         AND
         (Revierfigur[Tiefe,Spieler,4].Wichtigkeit = FALSE)
         AND
         (Tiefe = 1)) THEN
     BEGIN
          Endspiel              := TRUE;
          Endspielmodus_maximal := 2;
     END;
END;


PROCEDURE dm_bf_Primaer(FrN : INTEGER);
VAR T_F,T_S,T_Stellung,x,y : INTEGER;
VAR bf_Fr                  : Type_Figur;
BEGIN
{}   Log('Prim�re Brute Force Engine gestartet f�r Figur Nr. '
{}       +IntToStr(FrN)
{}       +'.');
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO bf_Fd[y,x] := SFd[1,y,x];
     FOR T_S := 1 TO 2 DO
     FOR T_F := 1 TO 4 DO bf_Fd[SFr[1,T_S,T_F].y,SFr[1,T_S,T_F].x].S := 0;
     bf_Fr := SFr[1,Selbst,FrN];
     bf_Tiefe := 0;
     bf_Zug[0].TZ_F := FrN;
     bf_Zug[0].TZ_R := 1;
     bf_Zug[0].TZ_E := 0;
     T_Stellung := 0;
     WHILE (TRUE) DO
     BEGIN
          WHILE (TRUE) DO
          BEGIN
               bf_Zug[bf_Tiefe].TZ_E := bf_Zug[bf_Tiefe].TZ_E+1;
               IF (bf_Zug[bf_Tiefe].TZ_E > 7) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN BREAK;
                    bf_Zug[bf_Tiefe].TZ_E := 1;
               END;
               y := bf_Fr.y+RS_y(bf_Zug[bf_Tiefe].TZ_R,bf_Zug[bf_Tiefe].TZ_E);
               x := bf_Fr.x+RS_x(bf_Zug[bf_Tiefe].TZ_R,bf_Zug[bf_Tiefe].TZ_E);
               IF (EFd(y,x) = FALSE) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN BREAK;
                    bf_Zug[bf_Tiefe].TZ_E := 0;
                    CONTINUE;
               END;
               IF (bf_Fd[y,x].S = 0) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN BREAK;
                    bf_Zug[bf_Tiefe].TZ_E := 0;
                    CONTINUE;
               END;
               BREAK;
          END;
          IF (bf_Zug[bf_Tiefe].TZ_R < 7) THEN
          BEGIN
               T_Stellung := T_Stellung+1;
               bf_Tiefe   := bf_Tiefe+1;
               IF (T_Stellung = 500000) THEN
               BEGIN
{}                  Log(IntToStr(T_Stellung)+' - VZZ: '+IntToStr(VZZ));
{}                  Log('Prim�re Brute Force Engine wegen Zeitmangels'
{}                      +' vorzeitig heruntergefahren. VZZ: '+IntToStr(VZZ));
{}                  Log('bf_ZPS: '
                        +IntToStr(bf_ZPS.F)
                        +IntToStr(bf_ZPS.R)
                        +IntToStr(bf_ZPS.E));
                    Zug := IntToStr(SFr[1,Selbst,bf_ZPS.F].y)
                           +' '+
                           IntToStr(SFr[1,Selbst,bf_ZPS.F].x)
                           +' '+
                           IntToStr(SFr[1,Selbst,bf_ZPS.F].y
                           +RS_y(bf_ZPS.R,bf_ZPS.E))
                           +' '+
                           IntToStr(SFr[1,Selbst,bf_ZPS.F].x
                           +RS_x(bf_ZPS.R,bf_ZPS.E));
                    EXIT;
               END;
               bf_Fd[bf_Fr.y,bf_Fr.x].S := 0;
               bf_Zug[bf_Tiefe].ZA_y    := bf_Fr.y;
               bf_Zug[bf_Tiefe].ZA_x    := bf_Fr.x;
               bf_Zug[bf_Tiefe].PD      := bf_Zug[bf_Tiefe-1].PD+bf_Fd[y,x].P;
               bf_Zug[bf_Tiefe].FdD     := bf_Zug[bf_Tiefe-1].FdD+1;
               bf_Zug[bf_Tiefe].TZ_R    := 1;
               bf_Zug[bf_Tiefe].TZ_E    := 0;
               bf_Zug[bf_Tiefe].ETZ_PD  := -1000;
               bf_Zug[bf_Tiefe].ETZ_FdD := -1000;
               bf_Fr.y := y;
               bf_Fr.x := x;
          END ELSE
          BEGIN
               IF (bf_Tiefe = 0) THEN BREAK;
               bf_Fd[bf_Fr.y,bf_Fr.x].S := 1;
               bf_Fr.y                  := bf_Zug[bf_Tiefe].ZA_y;
               bf_Fr.x                  := bf_Zug[bf_Tiefe].ZA_x;
               IF (bf_Zug[bf_Tiefe].ETZ_PD = -1000) THEN
               BEGIN
                    IF ((bf_Zug[bf_Tiefe].PD
                         =
                         Revierfigur[1,Selbst,FrN].RPA)
                        AND
                        (bf_Zug[bf_Tiefe].FdD
                         =
                         Revierfigur[1,Selbst,FrN].RFdA)) THEN
                    BEGIN
                         Zug := IntToStr(SFr[1,Selbst,FrN].y)
                                +' '+
                                IntToStr(SFr[1,Selbst,FrN].x)
                                +' '+
                                IntToStr(SFr[1,Selbst,FrN].y
                                         +RS_y(bf_Zug[0].TZ_R,bf_Zug[0].TZ_E))
                                +' '+
                                IntToStr(SFr[1,Selbst,FrN].x
                                         +RS_x(bf_Zug[0].TZ_R,bf_Zug[0].TZ_E));
{}                       Log('Perfekten Zug gefunden. Prim�re Brute Force'
{}                           +' Engine vorzeitig heruntergefahren.');
                         Exit;
                    END;
                    IF ((bf_Zug[bf_Tiefe].PD > bf_Zug[0].ETZ_PD)
                        OR
                        ((bf_Zug[bf_Tiefe].PD = bf_Zug[0].ETZ_PD)
                         AND
                         (bf_Zug[bf_Tiefe].FdD > bf_Zug[0].ETZ_FdD))) THEN
                    BEGIN
                         bf_ZPS.R := bf_Zug[0].TZ_R;
                         bf_ZPS.E := bf_Zug[0].TZ_E;
                         bf_Zug[0].ETZ_PD  := bf_Zug[bf_Tiefe].PD;
                         bf_Zug[0].ETZ_FdD := bf_Zug[bf_Tiefe].FdD;
                    END;
                    bf_Zug[bf_Tiefe].ETZ_PD  := bf_Zug[bf_Tiefe].PD;
                    bf_Zug[bf_Tiefe].ETZ_FdD := bf_Zug[bf_Tiefe].FdD;
               END;
               IF ((bf_Zug[bf_Tiefe].ETZ_PD
                    >
                    bf_Zug[bf_Tiefe-1].ETZ_PD)
                   OR
                   ((bf_Zug[bf_Tiefe].ETZ_PD
                     =
                     bf_Zug[bf_Tiefe-1].ETZ_PD)
                    AND
                    (bf_Zug[bf_Tiefe].ETZ_FdD
                     >
                     bf_Zug[bf_Tiefe-1].ETZ_FdD))) THEN
               BEGIN
                    bf_Zug[bf_Tiefe-1].ETZ_PD  := bf_Zug[bf_Tiefe].ETZ_PD;
                    bf_Zug[bf_Tiefe-1].ETZ_FdD := bf_Zug[bf_Tiefe].ETZ_FdD;
                    IF (bf_Tiefe = 1) THEN
                    BEGIN
                         bf_ZPS.R := bf_Zug[0].TZ_R;
                         bf_ZPS.E := bf_Zug[0].TZ_E;
                    END;
               END;
               bf_Tiefe := bf_Tiefe-1;
          END;
     END;
     Zug := IntToStr(SFr[1,Selbst,FrN].y)
            +' '+
            IntToStr(SFr[1,Selbst,FrN].x)
            +' '+
            IntToStr(SFr[1,Selbst,FrN].y+RS_y(bf_ZPS.R,bf_ZPS.E))
            +' '+
            IntToStr(SFr[1,Selbst,FrN].x+RS_x(bf_ZPS.R,bf_ZPS.E));
{}          Log('Prim�re Brute Force Engine heruntergefahren.');
END;


PROCEDURE dm_bf_Sekundaer;
VAR bf_Fr                           : ARRAY[1..4] OF Type_Figur;
VAR Bereits_eingetragen             : BOOLEAN;
VAR bf_FrA,i,j,k,T_F,T_Stellung,x,y : INTEGER;
BEGIN
{}   Log('Sekund�re Brute Force Engine gestartet f�r Figuren Nr. '
{}       +IntToStr(bf_FrS[1])+' '
{}       +IntToStr(bf_FrS[2])+' '
{}       +IntToStr(bf_FrS[3])+' '
{}       +IntToStr(bf_FrS[4])+'.');
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO bf_Fd[y,x] := SFd[1,y,x];
     FOR T_F := 1 TO 4 DO
     BEGIN
          bf_Fr[T_F]                                       := SFr[1,Selbst,T_F];
          bf_Fd[bf_Fr[T_F].y,bf_Fr[T_F].x].S               := 0;
          bf_Fd[SFr[1,Gegner,T_F].y,SFr[1,Gegner,T_F].x].S := 0;
     END;
     bf_FdA := 0;
     i := 0;
     FOR T_F := 1 TO 4 DO
      IF (bf_FrS[T_F] > 0)
      THEN FOR j := 1 TO Revierfigur[1,Selbst,bf_FrS[T_F]].RFdA DO
           BEGIN
                Bereits_eingetragen := FALSE;
                k := 1;
                WHILE (k <= i) DO
                BEGIN
                     IF ((RFd[1,Selbst,bf_FrS[T_F],j].y = bf_RFd[k].y)
                         AND
                         (RFd[1,Selbst,bf_FrS[T_F],j].x = bf_RFd[k].x)) THEN
                     BEGIN
                          Bereits_eingetragen := TRUE;
                          BREAK;
                     END;
                     k := k+1;
                END;
                IF (Bereits_eingetragen = FALSE) THEN
                BEGIN
                     i := i+1;
                     bf_RFd[i].y := RFd[1,Selbst,bf_FrS[T_F],j].y;
                     bf_RFd[i].x := RFd[1,Selbst,bf_FrS[T_F],j].x;
                     bf_FdA := bf_FdA+1;
                END;
           END;
     bf_FrA := 2;
     IF (bf_FrS[3] > 0) THEN bf_FrA := 3;
     IF (bf_FrS[4] > 0) THEN bf_FrA := 4;
     bf_Tiefe         := 0;
     bf_Zug[0].TZ_UFr := 1;
     bf_Zug[0].TZ_F   := bf_FrS[1];
     bf_Zug[0].TZ_R   := 1;
     bf_Zug[0].TZ_E   := 0;
     T_Stellung := 0;
     WHILE (TRUE) DO
     BEGIN
          WHILE (TRUE) DO
          BEGIN
               bf_Zug[bf_Tiefe].TZ_E := bf_Zug[bf_Tiefe].TZ_E+1;
               IF (bf_Zug[bf_Tiefe].TZ_E > 7) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_UFr := bf_Zug[bf_Tiefe].TZ_UFr+1;
                         IF (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrA) THEN BREAK;
                         bf_Zug[bf_Tiefe].TZ_F
                          := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         bf_Zug[bf_Tiefe].TZ_R := 1;
                    END;
                    bf_Zug[bf_Tiefe].TZ_E := 1;
               END;
               y := bf_Fr[bf_Zug[bf_Tiefe].TZ_F].y
                    +RS_y(bf_Zug[bf_Tiefe].TZ_R,bf_Zug[bf_Tiefe].TZ_E);
               x := bf_Fr[bf_Zug[bf_Tiefe].TZ_F].x
                    +RS_x(bf_Zug[bf_Tiefe].TZ_R,bf_Zug[bf_Tiefe].TZ_E);
               IF (EFd(y,x) = FALSE) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_UFr := bf_Zug[bf_Tiefe].TZ_UFr+1;
                         IF (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrA) THEN BREAK;
                         bf_Zug[bf_Tiefe].TZ_F
                          := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         bf_Zug[bf_Tiefe].TZ_R := 1;
                    END;
                    bf_Zug[bf_Tiefe].TZ_E := 0;
                    CONTINUE;
               END;
               IF (bf_Fd[y,x].S = 0) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_UFr := bf_Zug[bf_Tiefe].TZ_UFr+1;
                         IF (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrA) THEN BREAK;
                         bf_Zug[bf_Tiefe].TZ_F
                          := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         bf_Zug[bf_Tiefe].TZ_R := 1;
                    END;
                    bf_Zug[bf_Tiefe].TZ_E := 0;
                    CONTINUE;
               END;
               BREAK;
          END;
          IF (bf_Zug[bf_Tiefe].TZ_UFr <= bf_FrA) THEN
          BEGIN
               T_Stellung := T_Stellung+1;
               bf_Tiefe := bf_Tiefe+1;
               IF (T_Stellung = 500000) THEN
               BEGIN
{}                  Log(IntToStr(T_Stellung)+' - VZZ: '+IntToStr(VZZ));
{}                  Log('Sekund�re Brute Force Engine wegen Zeitmangels'
{}                      +' vorzeitig heruntergefahren. VZZ: '+IntToStr(VZZ));
{}                  Log('bf_ZPS: '
                        +IntToStr(bf_ZPS.F)
                        +IntToStr(bf_ZPS.R)
                        +IntToStr(bf_ZPS.E));
                    Zug := IntToStr(SFr[1,Selbst,bf_ZPS.F].y)
                           +' '+
                           IntToStr(SFr[1,Selbst,bf_ZPS.F].x)
                           +' '+
                           IntToStr(SFr[1,Selbst,bf_ZPS.F].y
                           +RS_y(bf_ZPS.R,bf_ZPS.E))
                           +' '+
                           IntToStr(SFr[1,Selbst,bf_ZPS.F].x
                           +RS_x(bf_ZPS.R,bf_ZPS.E));
                    EXIT;
               END;
               bf_Zug[bf_Tiefe].ZA_y    := bf_Fr[bf_Zug[bf_Tiefe-1].TZ_F].y;
               bf_Zug[bf_Tiefe].ZA_x    := bf_Fr[bf_Zug[bf_Tiefe-1].TZ_F].x;
               bf_Zug[bf_Tiefe].ZE_y    := y;
               bf_Zug[bf_Tiefe].ZE_x    := x;
               bf_Fd[y,x].S             := 0;
               bf_Zug[bf_Tiefe].FrN     := bf_Zug[bf_Tiefe-1].TZ_F;
               bf_Zug[bf_Tiefe].PD      := bf_Zug[bf_Tiefe-1].PD+bf_Fd[y,x].P;
               bf_Zug[bf_Tiefe].FdD     := bf_Zug[bf_Tiefe-1].FdD+1;
               bf_Zug[bf_Tiefe].TZ_UFr  := 1;
               bf_Zug[bf_Tiefe].TZ_F    := bf_FrS[1];;
               bf_Zug[bf_Tiefe].TZ_R    := 1;
               bf_Zug[bf_Tiefe].TZ_E    := 0;
               bf_Zug[bf_Tiefe].ETZ_PD  := -1000;
               bf_Zug[bf_Tiefe].ETZ_FdD := -1000;
               bf_Fr[bf_Zug[bf_Tiefe-1].TZ_F].y := y;
               bf_Fr[bf_Zug[bf_Tiefe-1].TZ_F].x := x;
          END ELSE
          BEGIN
               IF (bf_Tiefe = 0) THEN BREAK;
               bf_Fd[bf_Zug[bf_Tiefe].ZE_y,bf_Zug[bf_Tiefe].ZE_x].S := 1;
               bf_Fr[bf_Zug[bf_Tiefe].FrN].y := bf_Zug[bf_Tiefe].ZA_y;
               bf_Fr[bf_Zug[bf_Tiefe].FrN].x := bf_Zug[bf_Tiefe].ZA_x;
               IF (bf_Zug[bf_Tiefe].ETZ_PD = -1000) THEN
               BEGIN
                    IF (bf_Zug[bf_Tiefe].FdD = bf_FdA) THEN
                    BEGIN
                         Zug := IntToStr(SFr[1,Selbst,bf_Zug[0].TZ_F].y)
                                +' '+
                                IntToStr(SFr[1,Selbst,bf_Zug[0].TZ_F].x)
                                +' '+
                                IntToStr(SFr[1,Selbst,bf_Zug[0].TZ_F].y
                                         +RS_y(bf_Zug[0].TZ_R,bf_Zug[0].TZ_E))
                                +' '+
                                IntToStr(SFr[1,Selbst,bf_Zug[0].TZ_F].x
                                         +RS_x(bf_Zug[0].TZ_R,bf_Zug[0].TZ_E));
{}                       Log('Perfekten Zug gefunden. Sekund�re Brute Force'
{}                           +' Engine vorzeitig heruntergefahren.');
                         Exit;
                    END;
                    IF ((bf_Zug[bf_Tiefe].PD > bf_Zug[0].ETZ_PD)
                        OR
                        ((bf_Zug[bf_Tiefe].PD = bf_Zug[0].ETZ_PD)
                         AND
                         (bf_Zug[bf_Tiefe].FdD > bf_Zug[0].ETZ_FdD))) THEN
                    BEGIN
                         bf_ZPS.F := bf_Zug[0].TZ_F;
                         bf_ZPS.R := bf_Zug[0].TZ_R;
                         bf_ZPS.E := bf_Zug[0].TZ_E;
                         bf_Zug[0].ETZ_PD  := bf_Zug[bf_Tiefe].PD;
                         bf_Zug[0].ETZ_FdD := bf_Zug[bf_Tiefe].FdD;
                    END;
                    bf_Zug[bf_Tiefe].ETZ_PD  := bf_Zug[bf_Tiefe].PD;
                    bf_Zug[bf_Tiefe].ETZ_FdD := bf_Zug[bf_Tiefe].FdD;
               END;
               IF ((bf_Zug[bf_Tiefe].ETZ_PD
                    >
                    bf_Zug[bf_Tiefe-1].ETZ_PD)
                   OR
                   ((bf_Zug[bf_Tiefe].ETZ_PD
                     =
                     bf_Zug[bf_Tiefe-1].ETZ_PD)
                    AND
                    (bf_Zug[bf_Tiefe].ETZ_FdD
                     >
                     bf_Zug[bf_Tiefe-1].ETZ_FdD))) THEN
               BEGIN
                    bf_Zug[bf_Tiefe-1].ETZ_PD  := bf_Zug[bf_Tiefe].ETZ_PD;
                    bf_Zug[bf_Tiefe-1].ETZ_FdD := bf_Zug[bf_Tiefe].ETZ_FdD;
                    IF (bf_Tiefe = 1) THEN
                    BEGIN
                         bf_ZPS.F := bf_Zug[0].TZ_F;
                         bf_ZPS.R := bf_Zug[0].TZ_R;
                         bf_ZPS.E := bf_Zug[0].TZ_E;
                    END;
               END;
               bf_Tiefe := bf_Tiefe-1;
          END;
     END;
     Zug := IntToStr(SFr[1,Selbst,bf_ZPS.F].y)
            +' '+
            IntToStr(SFr[1,Selbst,bf_ZPS.F].x)
            +' '+
            IntToStr(SFr[1,Selbst,bf_ZPS.F].y+RS_y(bf_ZPS.R,bf_ZPS.E))
            +' '+
            IntToStr(SFr[1,Selbst,bf_ZPS.F].x+RS_x(bf_ZPS.R,bf_ZPS.E));
{}          Log('Sekund�re Brute Force Engine heruntergefahren.');
END;


PROCEDURE dm_bf_Tertiaer;
VAR bf_Fr       : ARRAY[1..2,1..4] OF Type_Figur;
VAR T_F,T_S,x,y : INTEGER;
BEGIN
{}   Log('Terti�re Brute Force Engine gestartet f�r Figuren Nr. '
{}       +IntToStr(bf_FrS[1])
{}       +IntToStr(bf_FrS[2])
{}       +IntToStr(bf_FrS[3])
{}       +IntToStr(bf_FrS[4])+' und '
{}       +IntToStr(bf_FrG[1])
{}       +IntToStr(bf_FrG[2])
{}       +IntToStr(bf_FrG[3])
{}       +IntToStr(bf_FrG[4])+'.');
     FOR y := 0 TO  7 DO
     FOR x := 0 TO 10 DO bf_Fd[y,x] := SFd[1,y,x];
     FOR T_S := 1 TO 2 DO
     FOR T_F := 1 TO 4 DO
     BEGIN
          bf_Fr[T_S,T_F]                             := SFr[1,T_S,T_F];
          bf_Fd[bf_Fr[T_S,T_F].y,bf_Fr[T_S,T_F].x].S := 0;
     END;
     bf_Tiefe         := 0;
     bf_Zug[0].TZ_UFr := 1;
     bf_Zug[0].TZ_S   := Selbst;
     bf_Zug[0].TZ_F   := bf_FrS[1];
     bf_Zug[0].TZ_R   := 1;
     bf_Zug[0].TZ_E   := 0;
     WHILE (TRUE) DO
     BEGIN
          WHILE (TRUE) DO
          BEGIN
               bf_Zug[bf_Tiefe].TZ_E := bf_Zug[bf_Tiefe].TZ_E+1;
               IF (bf_Zug[bf_Tiefe].TZ_E > 7) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_UFr := bf_Zug[bf_Tiefe].TZ_UFr+1;
                         IF ((bf_Zug[bf_Tiefe].VS = 0)
                             AND
                             ((bf_Zug[bf_Tiefe].ETZ_PD = -1000)
                              OR
                              (bf_Zug[bf_Tiefe].ETZ_PD =  1000))
                             AND
                             (((bf_Tiefe MOD 2 = 0)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                              OR
                              ((bf_Tiefe MOD 2 = 1)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG)))) THEN
                         BEGIN
                              IF (bf_Tiefe MOD 2 = 0) THEN
                              BEGIN
                                   bf_Zug[bf_Tiefe].VS   := Gegner;
                                   bf_Zug[bf_Tiefe].TZ_S := Gegner;
                              END ELSE
                              BEGIN
                                   bf_Zug[bf_Tiefe].VS   := Selbst;
                                   bf_Zug[bf_Tiefe].TZ_S := Selbst;
                              END;
                              bf_Zug[bf_Tiefe].ETZ_PD
                               := bf_Zug[bf_Tiefe].ETZ_PD *(-1);
                              bf_Zug[bf_Tiefe].ETZ_FdD
                               := bf_Zug[bf_Tiefe].ETZ_FdD*(-1);
                              bf_Zug[bf_Tiefe].TZ_UFr := 1;
                              bf_Zug[bf_Tiefe].TZ_R   := 1;
                         END;
                         IF (((bf_Zug[bf_Tiefe].VS = 0)
                              AND
                              (((bf_Tiefe MOD 2 = 0)
                                AND
                                (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                               OR
                               ((bf_Tiefe MOD 2 = 1)
                                AND
                                (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG))))
                             OR
                             (((bf_Zug[bf_Tiefe].VS = Selbst)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                              OR
                              ((bf_Zug[bf_Tiefe].VS = Gegner)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG))))
                         THEN BREAK;
                         IF ((bf_Zug[bf_Tiefe].VS = 0) AND (bf_Tiefe MOD 2 = 0))
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF ((bf_Zug[bf_Tiefe].VS = 0) AND (bf_Tiefe MOD 2 = 1))
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrG[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF (bf_Zug[bf_Tiefe].VS = Selbst)
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF (bf_Zug[bf_Tiefe].VS = Gegner)
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrG[bf_Zug[bf_Tiefe].TZ_UFr];
                         bf_Zug[bf_Tiefe].TZ_R := 1;
                    END;
                    bf_Zug[bf_Tiefe].TZ_E := 1;
               END;
               y := bf_Fr[bf_Zug[bf_Tiefe].TZ_S,bf_Zug[bf_Tiefe].TZ_F].y
                    +RS_y(bf_Zug[bf_Tiefe].TZ_R,bf_Zug[bf_Tiefe].TZ_E);
               x := bf_Fr[bf_Zug[bf_Tiefe].TZ_S,bf_Zug[bf_Tiefe].TZ_F].x
                    +RS_x(bf_Zug[bf_Tiefe].TZ_R,bf_Zug[bf_Tiefe].TZ_E);
               IF (EFd(y,x) = FALSE) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_UFr := bf_Zug[bf_Tiefe].TZ_UFr+1;
                         IF ((bf_Zug[bf_Tiefe].VS = 0)
                             AND
                             ((bf_Zug[bf_Tiefe].ETZ_PD = -1000)
                              OR
                              (bf_Zug[bf_Tiefe].ETZ_PD =  1000))
                             AND
                             (((bf_Tiefe MOD 2 = 0)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                              OR
                              ((bf_Tiefe MOD 2 = 1)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG)))) THEN
                         BEGIN
                              IF (bf_Tiefe MOD 2 = 0) THEN
                              BEGIN
                                   bf_Zug[bf_Tiefe].VS   := Gegner;
                                   bf_Zug[bf_Tiefe].TZ_S := Gegner;
                              END ELSE
                              BEGIN
                                   bf_Zug[bf_Tiefe].VS   := Selbst;
                                   bf_Zug[bf_Tiefe].TZ_S := Selbst;
                              END;
                              bf_Zug[bf_Tiefe].ETZ_PD
                               := bf_Zug[bf_Tiefe].ETZ_PD *(-1);
                              bf_Zug[bf_Tiefe].ETZ_FdD
                               := bf_Zug[bf_Tiefe].ETZ_FdD*(-1);
                              bf_Zug[bf_Tiefe].TZ_UFr := 1;
                              bf_Zug[bf_Tiefe].TZ_R   := 1;
                         END;
                         IF (((bf_Zug[bf_Tiefe].VS = 0)
                              AND
                              (((bf_Tiefe MOD 2 = 0)
                                AND
                                (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                               OR
                               ((bf_Tiefe MOD 2 = 1)
                                AND
                                (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG))))
                             OR
                             (((bf_Zug[bf_Tiefe].VS = Selbst)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                              OR
                              ((bf_Zug[bf_Tiefe].VS = Gegner)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG))))
                         THEN BREAK;
                         IF ((bf_Zug[bf_Tiefe].VS = 0) AND (bf_Tiefe MOD 2 = 0))
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF ((bf_Zug[bf_Tiefe].VS = 0) AND (bf_Tiefe MOD 2 = 1))
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrG[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF (bf_Zug[bf_Tiefe].VS = Selbst)
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF (bf_Zug[bf_Tiefe].VS = Gegner)
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrG[bf_Zug[bf_Tiefe].TZ_UFr];
                         bf_Zug[bf_Tiefe].TZ_R := 1;
                    END;
                    bf_Zug[bf_Tiefe].TZ_E := 0;
                    CONTINUE;
               END;
               IF (bf_Fd[y,x].S = 0) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].TZ_R := bf_Zug[bf_Tiefe].TZ_R+1;
                    IF (bf_Zug[bf_Tiefe].TZ_R = 7) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_UFr := bf_Zug[bf_Tiefe].TZ_UFr+1;
                         IF ((bf_Zug[bf_Tiefe].VS = 0)
                             AND
                             ((bf_Zug[bf_Tiefe].ETZ_PD = -1000)
                              OR
                              (bf_Zug[bf_Tiefe].ETZ_PD =  1000))
                             AND
                             (((bf_Tiefe MOD 2 = 0)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                              OR
                              ((bf_Tiefe MOD 2 = 1)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG)))) THEN
                         BEGIN
                              IF (bf_Tiefe MOD 2 = 0) THEN
                              BEGIN
                                   bf_Zug[bf_Tiefe].VS   := Gegner;
                                   bf_Zug[bf_Tiefe].TZ_S := Gegner;
                              END ELSE
                              BEGIN
                                   bf_Zug[bf_Tiefe].VS   := Selbst;
                                   bf_Zug[bf_Tiefe].TZ_S := Selbst;
                              END;
                              bf_Zug[bf_Tiefe].ETZ_PD
                               := bf_Zug[bf_Tiefe].ETZ_PD *(-1);
                              bf_Zug[bf_Tiefe].ETZ_FdD
                               := bf_Zug[bf_Tiefe].ETZ_FdD*(-1);
                              bf_Zug[bf_Tiefe].TZ_UFr := 1;
                              bf_Zug[bf_Tiefe].TZ_R   := 1;
                         END;
                         IF (((bf_Zug[bf_Tiefe].VS = 0)
                              AND
                              (((bf_Tiefe MOD 2 = 0)
                                AND
                                (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                               OR
                               ((bf_Tiefe MOD 2 = 1)
                                AND
                                (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG))))
                             OR
                             (((bf_Zug[bf_Tiefe].VS = Selbst)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAS))
                              OR
                              ((bf_Zug[bf_Tiefe].VS = Gegner)
                               AND
                               (bf_Zug[bf_Tiefe].TZ_UFr > bf_FrAG))))
                         THEN BREAK;
                         IF ((bf_Zug[bf_Tiefe].VS = 0) AND (bf_Tiefe MOD 2 = 0))
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF ((bf_Zug[bf_Tiefe].VS = 0) AND (bf_Tiefe MOD 2 = 1))
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrG[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF (bf_Zug[bf_Tiefe].VS = Selbst)
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrS[bf_Zug[bf_Tiefe].TZ_UFr];
                         IF (bf_Zug[bf_Tiefe].VS = Gegner)
                         THEN bf_Zug[bf_Tiefe].TZ_F
                               := bf_FrG[bf_Zug[bf_Tiefe].TZ_UFr];
                         bf_Zug[bf_Tiefe].TZ_R := 1;
                    END;
                    bf_Zug[bf_Tiefe].TZ_E := 0;
                    CONTINUE;
               END;
               BREAK;
          END;
          IF (((bf_Zug[bf_Tiefe].TZ_S = Selbst)
               AND
               (bf_Zug[bf_Tiefe].TZ_UFr <= bf_FrAS))
              OR
              ((bf_Zug[bf_Tiefe].TZ_S = Gegner)
               AND
               (bf_Zug[bf_Tiefe].TZ_UFr <= bf_FrAG))) THEN
          BEGIN
               bf_Tiefe := bf_Tiefe+1;
               bf_Zug[bf_Tiefe].ZA_y
                := bf_Fr[bf_Zug[bf_Tiefe-1].TZ_S,bf_Zug[bf_Tiefe-1].TZ_F].y;
               bf_Zug[bf_Tiefe].ZA_x
                := bf_Fr[bf_Zug[bf_Tiefe-1].TZ_S,bf_Zug[bf_Tiefe-1].TZ_F].x;
               bf_Zug[bf_Tiefe].ZE_y    := y;
               bf_Zug[bf_Tiefe].ZE_x    := x;
               bf_Fd[y,x].S := 0;
               bf_Zug[bf_Tiefe].FrN     := bf_Zug[bf_Tiefe-1].TZ_F;
               bf_Zug[bf_Tiefe].VS      := bf_Zug[bf_Tiefe-1].VS;
               IF (bf_Zug[bf_Tiefe-1].TZ_S = Selbst) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].PD   := bf_Zug[bf_Tiefe-1].PD+bf_Fd[y,x].P;
                    bf_Zug[bf_Tiefe].FdD  := bf_Zug[bf_Tiefe-1].FdD+1;
                    IF (bf_Zug[bf_Tiefe].VS = Selbst) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_F    := bf_FrS[1];
                         bf_Zug[bf_Tiefe].ETZ_PD  := -1000;
                         bf_Zug[bf_Tiefe].ETZ_FdD := -1000;
                    END ELSE
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_F    := bf_FrG[1];
                         bf_Zug[bf_Tiefe].ETZ_PD  := 1000;
                         bf_Zug[bf_Tiefe].ETZ_FdD := 1000;
                    END;
               END ELSE
               BEGIN
                    bf_Zug[bf_Tiefe].PD   := bf_Zug[bf_Tiefe-1].PD-bf_Fd[y,x].P;
                    bf_Zug[bf_Tiefe].FdD  := bf_Zug[bf_Tiefe-1].FdD-1;
                    IF (bf_Zug[bf_Tiefe].VS = Gegner) THEN
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_F    := bf_FrG[1];
                         bf_Zug[bf_Tiefe].ETZ_PD  := 1000;
                         bf_Zug[bf_Tiefe].ETZ_FdD := 1000;
                    END ELSE
                    BEGIN
                         bf_Zug[bf_Tiefe].TZ_F    := bf_FrS[1];
                         bf_Zug[bf_Tiefe].ETZ_PD  := -1000;
                         bf_Zug[bf_Tiefe].ETZ_FdD := -1000;
                    END;
               END;
               bf_Zug[bf_Tiefe].TZ_UFr  := 1;
               IF (bf_Zug[bf_Tiefe].VS = 0)
               THEN bf_Zug[bf_Tiefe].TZ_S := 3-bf_Zug[bf_Tiefe-1].TZ_S
               ELSE bf_Zug[bf_Tiefe].TZ_S := bf_Zug[bf_Tiefe].VS;
               bf_Zug[bf_Tiefe].TZ_R    := 1;
               bf_Zug[bf_Tiefe].TZ_E    := 0;
               bf_Fr[bf_Zug[bf_Tiefe-1].TZ_S,bf_Zug[bf_Tiefe-1].TZ_F].y := y;
               bf_Fr[bf_Zug[bf_Tiefe-1].TZ_S,bf_Zug[bf_Tiefe-1].TZ_F].x := x;
          END ELSE
          BEGIN
               IF (bf_Tiefe = 0) THEN BREAK;
               bf_Fd[bf_Zug[bf_Tiefe].ZE_y,bf_Zug[bf_Tiefe].ZE_x].S := 1;
               bf_Fr[bf_Zug[bf_Tiefe-1].TZ_S,bf_Zug[bf_Tiefe].FrN].y
                := bf_Zug[bf_Tiefe].ZA_y;
               bf_Fr[bf_Zug[bf_Tiefe-1].TZ_S,bf_Zug[bf_Tiefe].FrN].x
                := bf_Zug[bf_Tiefe].ZA_x;
               IF ((bf_Zug[bf_Tiefe].ETZ_PD = -1000)
                   OR
                   (bf_Zug[bf_Tiefe].ETZ_PD = 1000)) THEN
               BEGIN
                    bf_Zug[bf_Tiefe].ETZ_PD  := bf_Zug[bf_Tiefe].PD;
                    bf_Zug[bf_Tiefe].ETZ_FdD := bf_Zug[bf_Tiefe].FdD;
               END;
               IF ((bf_Zug[bf_Tiefe-1].TZ_S = Selbst)
                   AND
                   ((bf_Zug[bf_Tiefe].ETZ_PD
                     >
                     bf_Zug[bf_Tiefe-1].ETZ_PD)
                    OR
                    ((bf_Zug[bf_Tiefe].ETZ_PD
                      =
                      bf_Zug[bf_Tiefe-1].ETZ_PD)
                     AND
                     (bf_Zug[bf_Tiefe].ETZ_FdD
                      >
                      bf_Zug[bf_Tiefe-1].ETZ_FdD)))) THEN
               BEGIN
                    bf_Zug[bf_Tiefe-1].ETZ_PD  := bf_Zug[bf_Tiefe].ETZ_PD;
                    bf_Zug[bf_Tiefe-1].ETZ_FdD := bf_Zug[bf_Tiefe].ETZ_FdD;
                    IF (bf_Tiefe = 1) THEN
                    BEGIN
                         bf_ZPS.F := bf_Zug[0].TZ_F;
                         bf_ZPS.R := bf_Zug[0].TZ_R;
                         bf_ZPS.E := bf_Zug[0].TZ_E;
                    END;
               END;
               IF ((bf_Zug[bf_Tiefe-1].TZ_S = Gegner)
                   AND
                   ((bf_Zug[bf_Tiefe].ETZ_PD
                     <
                     bf_Zug[bf_Tiefe-1].ETZ_PD)
                    OR
                    ((bf_Zug[bf_Tiefe].ETZ_PD
                      =
                      bf_Zug[bf_Tiefe-1].ETZ_PD)
                     AND
                     (bf_Zug[bf_Tiefe].ETZ_FdD
                      <
                      bf_Zug[bf_Tiefe-1].ETZ_FdD)))) THEN
               BEGIN
                    bf_Zug[bf_Tiefe-1].ETZ_PD  := bf_Zug[bf_Tiefe].ETZ_PD;
                    bf_Zug[bf_Tiefe-1].ETZ_FdD := bf_Zug[bf_Tiefe].ETZ_FdD;
               END;
               bf_Tiefe := bf_Tiefe-1;
          END;
     END;
     Zug := IntToStr(SFr[1,Selbst,bf_ZPS.F].y)
            +' '+
            IntToStr(SFr[1,Selbst,bf_ZPS.F].x)
            +' '+
            IntToStr(SFr[1,Selbst,bf_ZPS.F].y+RS_y(bf_ZPS.R,bf_ZPS.E))
            +' '+
            IntToStr(SFr[1,Selbst,bf_ZPS.F].x+RS_x(bf_ZPS.R,bf_ZPS.E));
{}          Log('Terti�re Brute Force Engine heruntergefahren.');
END;


PROCEDURE dm_FdA_Ermittlung_Tertiaer;
VAR Bereits_eingetragen : BOOLEAN;
VAR i,j,k,T_F           : INTEGER;
BEGIN
     FOR T_F := 1 TO 4 DO bf_FrS[T_F] := 0;
     FOR T_F := 1 TO 4 DO bf_FrG[T_F] := 0;
     bf_FdA  := 0;
     bf_FrAS := 0;
     bf_FrAG := 0;
     i := 1;
     j := 1;
     FOR T_F := 1 TO 4 DO
     BEGIN
          IF (Revierfigur[1,Selbst,T_F].Wichtigkeit = TRUE) THEN
          BEGIN
               bf_FrS[i] := T_F;
               bf_FrAS := bf_FrAS+1;
               i := i+1;
          END;
          IF (Revierfigur[1,Gegner,T_F].Wichtigkeit = TRUE) THEN
          BEGIN
               bf_FrG[j] := T_F;
               bf_FrAG := bf_FrAG+1;
               j := j+1;
          END;
     END;
     i := 0;
     FOR T_F := 1 TO 4 DO
     BEGIN
          IF (bf_FrS[T_F] > 0)
          THEN FOR j := 1 TO Revierfigur[1,Selbst,bf_FrS[T_F]].RFdA DO
               BEGIN
                    Bereits_eingetragen := FALSE;
                    k := 1;
                    WHILE (k <= i) DO
                    BEGIN
                         IF ((RFd[1,Selbst,bf_FrS[T_F],j].y
                              =
                              bf_RFd[k].y)
                             AND
                             (RFd[1,Selbst,bf_FrS[T_F],j].x
                              =
                              bf_RFd[k].x)) THEN
                         BEGIN
                              Bereits_eingetragen := TRUE;
                              BREAK;
                         END;
                         k := k+1;
                    END;
                    IF (Bereits_eingetragen = FALSE) THEN
                    BEGIN
                         i := i+1;
                         bf_RFd[i].y := RFd[1,Selbst,bf_FrS[T_F],j].y;
                         bf_RFd[i].x := RFd[1,Selbst,bf_FrS[T_F],j].x;
                         bf_FdA := bf_FdA+1;
                    END;
               END;
          IF (bf_FrG[T_F] > 0)
          THEN FOR j := 1 TO Revierfigur[1,Gegner,bf_FrG[T_F]].RFdA DO
               BEGIN
                    Bereits_eingetragen := FALSE;
                    k := 1;
                    WHILE (k <= i) DO
                    BEGIN
                         IF ((RFd[1,Gegner,bf_FrG[T_F],j].y
                              =
                              bf_RFd[k].y)
                             AND
                             (RFd[1,Gegner,bf_FrG[T_F],j].x
                              =
                              bf_RFd[k].x)) THEN
                         BEGIN
                              Bereits_eingetragen := TRUE;
                              BREAK;
                         END;
                         k := k+1;
                    END;
                    IF (Bereits_eingetragen = FALSE) THEN
                    BEGIN
                         i := i+1;
                         bf_RFd[i].y := RFd[1,Gegner,bf_FrG[T_F],j].y;
                         bf_RFd[i].x := RFd[1,Gegner,bf_FrG[T_F],j].x;
                         bf_FdA := bf_FdA+1;
                    END;
               END;
     END;
END;


PROCEDURE dm_Ebene2_Mittelspiel(Tiefe : INTEGER);
VAR GFB,T_ZB : EXTENDED;
{ 'GFB'  bedeutet 'Gr��te_Feldbewertung'.              }
{ 'GSB'  bedeutet 'Gr��te_Standortbewertung'.          }
{ 'KFB'  bedeutet 'Kleinste_Feldbewertung'.            }
{ 'KSB'  bedeutet 'Kleinste_Standortbewertung'.        }
VAR i,j,k,T_E,T_F,T_R,T_Rang,T_Spieler : INTEGER;
BEGIN
     T_Spieler := 2-((Selbst-1+Tiefe) MOD 2);
     FOR T_Rang := 1 TO 84 DO
     BEGIN
          E2M_ZRB[Tiefe,T_Spieler,T_Rang].B := -1000000000000;
          E2M_ZRB[Tiefe,T_Spieler,T_Rang].F := -10;
          E2M_ZRB[Tiefe,T_Spieler,T_Rang].R := -10;
          E2M_ZRB[Tiefe,T_Spieler,T_Rang].E := -10;
     END;
     dm_Reviersuche(Tiefe,T_Spieler);
     IF (Endspiel = TRUE) THEN EXIT;
     IF (Tiefe = 1) THEN
     BEGIN
          dm_Reviersuche(1,Gegner);
          dm_FdA_Ermittlung_Tertiaer;
          IF (bf_FdA <= 12) THEN
          BEGIN
{}        Log('Fr�hzeitiges Endspiel: bf_FdA = '+IntToStr(bf_FdA));
               Endspiel      := TRUE;
               Endspielmodus := 3;
               EXIT;
          END;
{}        Log('Kein fr�hzeitiges Endspiel: bf_FdA = '+IntToStr(bf_FdA));
     END;
     dm_Feldbewertung_ET3(Tiefe,T_Spieler);
     dm_Standortbewertung(Tiefe,T_Spieler);
     dm_Verteilungsbewertung(Tiefe,T_Spieler);
     GFB :=             0;
     FOR T_F := 1 TO 4 DO
     FOR T_R := 1 TO 6 DO
     FOR T_E := 1 TO 7 DO
     BEGIN
          IF (FB[Tiefe,T_Spieler,T_F,T_R,T_E] > GFB)
          THEN GFB := FB[Tiefe,T_Spieler,T_F,T_R,T_E];
     END;
     IF (GFB = 0) THEN GFB := 1;
     i := 0;
     FOR T_F := 1 TO 4 DO
     FOR T_R := 1 TO 6 DO
     FOR T_E := 1 TO 7 DO
     BEGIN
          E2M_ZB[Tiefe,T_Spieler,T_F,T_R,T_E]
           := FB[Tiefe,T_Spieler,T_F,T_R,T_E]/GFB
              +SB[Tiefe,T_Spieler,T_F,T_R,T_E]/19000000000 { 25000000000 }
              +VB[Tiefe,T_Spieler,T_F,T_R,T_E]/3.7
              +RV[Tiefe,T_Spieler,T_F,T_R,T_E];
          NFB[Tiefe,T_Spieler,T_F,T_R,T_E]
           := FB[Tiefe,T_Spieler,T_F,T_R,T_E]/GFB;
          NSB[Tiefe,T_Spieler,T_F,T_R,T_E]
           := SB[Tiefe,T_Spieler,T_F,T_R,T_E]/25000000000;
          T_ZB := E2M_ZB[Tiefe,T_Spieler,T_F,T_R,T_E];
          i := i+1;
          FOR j := 1 TO i DO
           IF (T_ZB > E2M_ZRB[Tiefe,T_Spieler,j].B) THEN
           BEGIN
                FOR k := i-1 DOWNTO j DO
                 E2M_ZRB[Tiefe,T_Spieler,k+1] := E2M_ZRB[Tiefe,T_Spieler,k];
                E2M_ZRB[Tiefe,T_Spieler,j].B := T_ZB;
                E2M_ZRB[Tiefe,T_Spieler,j].F := T_F;
                E2M_ZRB[Tiefe,T_Spieler,j].R := T_R;
                E2M_ZRB[Tiefe,T_Spieler,j].E := T_E;
                BREAK;
           END;
     END;
END;


PROCEDURE dm_Ebene3_Mittelspiel;
VAR T_E,T_F,T_R,T_S,T_FrN,x,x_1,y,y_1 : INTEGER;
{} VAR ALE,i,j,T_Rang                 : INTEGER;
BEGIN
     dm_Ebene2_Mittelspiel(1);
     IF (Endspiel = TRUE) THEN EXIT;
{}   ALE := 0;
     FOR T_F := 1 TO 4 DO
     FOR T_R := 1 TO 6 DO
     FOR T_E := 1 TO 7 DO
     BEGIN
          y_1 := SFr[1,Selbst,T_F].y+RS_y(T_R,T_E);
          x_1 := SFr[1,Selbst,T_F].x+RS_x(T_R,T_E);
          IF (EFd(y_1,x_1) = FALSE) THEN BREAK;
          IF ((SFd[1,y_1,x_1].S = Nicht_vorhanden) OR (SFd[1,y_1,x_1].E > 0))
          THEN BREAK;
{---}
          Log('Jetzt werden die Antwortz�ge auf '
               +IntToStr(T_F)
               +IntToStr(T_R)
               +IntToStr(T_E)
               +' berechnet.');
{---}
          FOR y := 0 TO 7  DO
          FOR x := 0 TO 10 DO SFd[2,y,x] := SFd[1,y,x];
          SFd[2,SFr[1,Selbst,T_F].y,SFr[1,Selbst,T_F].x].E   := 0;
          SFd[2,SFr[1,Selbst,T_F].y,SFr[1,Selbst,T_F].x].FrN := 0;
          SFd[2,SFr[1,Selbst,T_F].y,SFr[1,Selbst,T_F].x].P   := 0;
          SFd[2,SFr[1,Selbst,T_F].y,SFr[1,Selbst,T_F].x].S   := 0;
          SFd[2,y_1,x_1].E   := Selbst;
          SFd[2,y_1,x_1].FrN := T_F;
          FOR T_S   := 1 TO 2 DO
          FOR T_FrN := 1 TO 4 DO SFr[2,T_S,T_FrN] := SFr[1,T_S,T_FrN];
          SFr[2,Selbst,T_F].y := y_1;
          SFr[2,Selbst,T_F].x := x_1;
          dm_Ebene2_Mittelspiel(2);
{---}
          Log('Und dazu als n�chstes die Bewertung f�r Ebene 3:');
{---}
          IF (E2M_ZB[1,Selbst,T_F,T_R,T_E]-E2M_ZRB[2,Gegner,1].B
              >
              E3M_ZPS.B) THEN
          BEGIN
{---}
               Log('Neuen besten Zug entdeckt: '
                   +IntToStr(T_F)
                   +IntToStr(T_R)
                   +IntToStr(T_E));
{---}
               E3M_ZPS.B := E2M_ZB[1,Selbst,T_F,T_R,T_E]-E2M_ZRB[2,Gegner,1].B;
               E3M_ZPS.F := T_F;
               E3M_ZPS.R := T_R;
               E3M_ZPS.E := T_E;
{---}
               Log('Er tr�gt die besondere Differenz: '+FloatToStr(E3M_ZPS.B));
{---}
          END;
{---}
          ALB[T_F,T_R,T_E].D  := E2M_ZB[1,Selbst,T_F,T_R,T_E]
                                 -E2M_ZRB[2,Gegner,1].B;
          ALB[T_F,T_R,T_E].F1 := T_F;
          ALB[T_F,T_R,T_E].R1 := T_R;
          ALB[T_F,T_R,T_E].E1 := T_E;
          ALB[T_F,T_R,T_E].B1 := E2M_ZB[1,Selbst,T_F,T_R,T_E];
          ALB[T_F,T_R,T_E].F2 := E2M_ZRB[2,Gegner,1].F;
          ALB[T_F,T_R,T_E].R2 := E2M_ZRB[2,Gegner,1].R;
          ALB[T_F,T_R,T_E].E2 := E2M_ZRB[2,Gegner,1].E;
          ALB[T_F,T_R,T_E].B2 := E2M_ZRB[2,Gegner,1].B;
          ALE := ALE+1;
          FOR i := 1 TO ALE DO
          IF (ALB[T_F,T_R,T_E].D > ALR[i].D) THEN
          BEGIN
               FOR j := ALE-1 DOWNTO i DO
               BEGIN
                    ALR[j+1] := ALR[j];
               END;
               ALR[i] := ALB[T_F,T_R,T_E];
               BREAK;
          END;
{---}
     END;
{---}
     FOR T_Rang := ALE+1 TO 84 DO
     BEGIN
          ALR[T_Rang].D  := -1000000000000;
          ALR[T_Rang].F1 := -10;
          ALR[T_Rang].R1 := -10;
          ALR[T_Rang].E1 := -10;
          ALR[T_Rang].B1 := -1000000000000;
          ALR[T_Rang].F2 := -10;
          ALR[T_Rang].R2 := -10;
          ALR[T_Rang].E2 := -10;
          ALR[T_Rang].B2 := -1000000000000;
     END;
{---}
     Zug := IntToStr(SFr[1,Selbst,E3M_ZPS.F].y)
            +' '+
            IntToStr(SFr[1,Selbst,E3M_ZPS.F].x)
            +' '+
            IntToStr(SFr[1,Selbst,E3M_ZPS.F].y+RS_y(E3M_ZPS.R,E3M_ZPS.E))
            +' '+
            IntToStr(SFr[1,Selbst,E3M_ZPS.F].x+RS_x(E3M_ZPS.R,E3M_ZPS.E));
END;


PROCEDURE dm_Ebene3_Endspiel;
VAR i,j,T_F,T_F2,T_H : INTEGER;
{ 'H'    bedeutet 'Helfer'.                            }
BEGIN
{}   Log('"Endspielturbo" gestartet.');
{}   Log('Anfangs-VZZ: '+IntToStr(VZZ));
     IF (Endspielbeginn = TRUE) THEN
     BEGIN
          Endspielbeginn := FALSE;
          IF (Endspielmodus <> 3) THEN Endspielmodus_maximal := 2
          ELSE BEGIN
               dm_bf_Tertiaer;
               EXIT;
          END;
     END ELSE
     BEGIN
          dm_Reviersuche(1,Selbst);
          IF (Endspielmodus_maximal = 3) THEN
          BEGIN
               dm_Reviersuche(1,Gegner);
               dm_FdA_Ermittlung_Tertiaer;
               IF (bf_FdA <= 10) THEN
               BEGIN
                    dm_bf_Tertiaer;
                    EXIT;
               END ELSE Endspielmodus_maximal := 2;
          END;
     END;
     FOR T_F := 1 TO 4 DO bf_FrS[T_F] := 0;
     bf_FdA  := 0;
     j := 2;
     FOR T_F := 1 TO 4 DO
     BEGIN
          IF (Revierfigur[1,Selbst,T_F].RFdA > 0) THEN
          BEGIN
               bf_FrS[1] := T_F;
               i := 0;
               WHILE (TRUE) DO
               BEGIN
                    i := i+1;
                    FOR T_H := 1 TO 3 DO
                    BEGIN
                         T_F2 := Revierfigur[1,Selbst,bf_FrS[i]].Helfer[T_H];
                         IF (T_F2 = 0) THEN BREAK;
                         IF ((Revierfigur[1,Selbst,T_F2].RFdA > 0)
                             AND
                             (T_F2 <> bf_FrS[1])
                             AND
                             (T_F2 <> bf_FrS[2])
                             AND
                             (T_F2 <> bf_FrS[3])) THEN
                         BEGIN
                              bf_FrS[j] := T_F2;
                              j := j+1;
                         END;
                    END;
                    IF (i+1 > j-1) THEN BREAK;
               END;
               IF (j > 2) THEN
               BEGIN
                    dm_bf_Sekundaer;
                    EXIT;
               END;
               dm_bf_Primaer(T_F);
               EXIT;
          END;
     END;
END;

                              
PROCEDURE dm_Zugausgabe;
{} VAR T_E,T_F,T_R,T_Rang,x,y : INTEGER;
BEGIN
     IF (ZN = 1)  THEN dm_Zweite_Definitionen;
     IF (ZN <= 4) THEN
     BEGIN
          dm_Einsetzungsfeldbewertung_ET3;
          Zug := IntToStr(EZP.y)
                 +' '+
                 IntToStr(EZP.x)
                 +' 0 0';
{---}
          Log('--- Einsetzungsfeldbewertungen nach Bezeichnung sortiert: ---');
          FOR y := 0 TO 7  DO
          FOR x := 0 TO 10 DO
           Log('EFB nach Bezeichnung sortiert: '
               +IntToStr(y)+IntToStr(x)+' : '+FloatToStr(EFB[y,x]));
          Log('EZP: '
              +IntToStr(EZP.y)+IntToStr(EZP.x)+' : '+FloatToStr(EZP.B));
{---}
     END;
     IF (ZN > 4) THEN
     BEGIN
          Endspielmodus := 1;
          IF (Endspiel = FALSE) THEN dm_Ebene3_Mittelspiel;
          IF (Endspiel = TRUE)  THEN dm_Ebene3_Endspiel;
{---}
          IF (Endspiel = TRUE) THEN EXIT;
          Log('--- Z�ge nach Bezeichnung sortiert: ---');
          FOR T_F := 1 TO 4 DO
          FOR T_R := 1 TO 6 DO
          FOR T_E := 1 TO 7 DO Log('Z�ge nach Bezeichnung sortiert: '
                                   +FloatToStr(ALB[T_F,T_R,T_E].D)
                                   +' : '
                                   +IntToStr(T_F)
                                   +IntToStr(T_R)
                                   +IntToStr(T_E)
                                   +' = '
                                   +FloatToStr(ALB[T_F,T_R,T_E].B1)
                                   +' --> '
                                   +IntToStr(ALB[T_F,T_R,T_E].F2)
                                   +IntToStr(ALB[T_F,T_R,T_E].R2)
                                   +IntToStr(ALB[T_F,T_R,T_E].E2)                                             +' = '
                                   +FloatToStr(ALB[T_F,T_R,T_E].B2));
          Log('--- Z�ge nach Rang sortiert: ---');
          FOR T_Rang := 84 DOWNTO 1 DO Log('Z�ge nach Rang sortiert: '
                                           +IntToStr(T_Rang)
                                           +'.) '
                                           +FloatToStr(ALR[T_Rang].D)
                                           +' : '
                                           +IntToStr(ALR[T_Rang].F1)
                                           +IntToStr(ALR[T_Rang].R1)
                                           +IntToStr(ALR[T_Rang].E1)
                                           +' = '
                                           +FloatToStr(ALR[T_Rang].B1)
                                           +' --> '
                                           +IntToStr(ALR[T_Rang].F2)
                                           +IntToStr(ALR[T_Rang].R2)
                                           +IntToStr(ALR[T_Rang].E2)
                                           +' = '
                                           +FloatToStr(ALR[T_Rang].B2));
          Log('--- Normierte Bewertungen nach Bezeichnung sortiert: ---');
          FOR T_F := 1 TO 4 DO
          FOR T_R := 1 TO 6 DO
          FOR T_E := 1 TO 7 DO Log('Bewertungen:'
                                   +' '+IntToStr(T_F)
                                   +' '+IntToStr(T_R)
                                   +' '+IntToStr(T_E)
                                   +' --> FB :'
                                   +' '+FloatToStr(NFB[1,Selbst,T_F,T_R,T_E])
                                   +'   SB :'
                                   +' '+FloatToStr(NSB[1,Selbst,T_F,T_R,T_E])
                                   +'   VB :'
                                   +' '+FloatToStr(VB[1,Selbst,T_F,T_R,T_E]/4)
                                   +'   RV :'
                                   +' '+FloatToStr(RV[1,Selbst,T_F,T_R,T_E]));
          FOR T_F := 1 TO 4 DO Log('Selbst '
                                   +IntToStr(T_F)
                                   +': '
                                   +IntToStr(SFr[1,Selbst,T_F].y)
                                   +' '
                                   +IntToStr(SFr[1,Selbst,T_F].x));
          FOR T_F := 1 TO 4 DO Log('Gegner '
                                   +IntToStr(T_F)
                                   +': '
                                   +IntToStr(SFr[1,Gegner,T_F].y)
                                   +' '
                                   +IntToStr(SFr[1,Gegner,T_F].x));
          FOR T_F := 1 TO 4 DO
          IF (Revierfigur[1,Selbst,T_F].Wichtigkeit = TRUE)
          THEN Log('Die eigene Figur mit der Nummer '
                   +IntToStr(T_F)
                   +' ist noch wichtig.')
          ELSE Log('Die eigene Figur mit der Nummer '
                   +IntToStr(T_F)
                   +' ist im Moment unwichtig.');
{---}
     END;
END;


{=== 3 Hauptprozedur ==========================================================}


BEGIN
     dm_Client_Information;
     dm_Erste_Definitionen;
     dm_Initialisierung;
     REPEAT
           READLN(Informationen);
           IF (StrToInt(Informationen[1]) = 1) THEN
           BEGIN
                QueryPerformanceCounter(ZZBS);
                IF (NOT((ZN = 1) AND ((SN) = FALSE))) THEN
                BEGIN
                     ZDG[ZN] := ZZBS-ZZBG;
                     GZDG    := GZDG+ZDG[ZN];
                END;
                dm_Informationsverarbeitung;
                dm_Zugausgabe;
                dm_Zugsendung;
           END;
     UNTIL (Informationen[1] = '2');
END.
