unit DebugForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DebugMemo, StdCtrls, Menus;

type


  //----------------------------------------------------------------------------

  TForm_Debug = class(TForm)
    MainMenu: TMainMenu;
    MM_File: TMenuItem;
    MM_Save: TMenuItem;
    MM_Load: TMenuItem;
    MM_Edit: TMenuItem;
    MM_Time: TMenuItem;
    MM_Clear: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    MM_AddEntry: TMenuItem;
    MM_Date: TMenuItem;
    MM_View: TMenuItem;
    MM_Close: TMenuItem;
    MM_Draws: TMenuItem;
    MM_PlayerNames: TMenuItem;
    procedure MM_PlayerNamesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MM_DrawsClick(Sender: TObject);
    procedure MM_CloseClick(Sender: TObject);
    procedure MemoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MM_DateClick(Sender: TObject);
    procedure MM_AddEntryClick(Sender: TObject);
    procedure DialogCanClose(Sender: TObject; var CanClose: Boolean);
    procedure MM_LoadClick(Sender: TObject);
    procedure MM_SaveClick(Sender: TObject);
    procedure MM_TimeClick(Sender: TObject);
    procedure MM_ClearClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
    FMemo: TDebugMemo;
    FConfirmBeforeClear: Boolean;
  public
    { Public-Deklarationen }
    property Memo: TDebugMemo read FMemo write FMemo;
    property ConfirmBeforeClear: Boolean read FConfirmBeforeClear write FConfirmBeforeClear; //Wird vor dem Leeren nachgefragt?
  end;

  procedure SendDebug(S: String);

//------------------------------------------------------------------------------
var
  Form_Debug: TForm_Debug;
  DebugPlayername: Boolean; //Spieler oder Clientname anzeigen

implementation

{$R *.dfm}

uses Unit_Server_Main;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure SendDebug(S: String);
begin
  Form_Debug.Memo.Send(S);
end;


//##############################
//########## FORMULAR ##########
//##############################

procedure TForm_Debug.FormCreate(Sender: TObject);
begin
  IF FMemo=nil
    THEN begin
      FMemo:=TDebugMemo.Create(self);
      FMemo.Parent:=self;
      with FMemo do
        begin
          AddTime:=MM_Time.Checked;
          AddDate:=MM_Date.Checked;
        end;
    end;
  FConfirmBeforeClear:=TRUE;
end;

//------------------------------------------------------------------------------

procedure TForm_Debug.FormShow(Sender: TObject);
begin
  MM_TimeClick(self);
  MM_PlayerNamesClick(self);
  MM_DrawsClick(self);
  FormResize(self);
end;

//------------------------------------------------------------------------------

procedure TForm_Debug.FormResize(Sender: TObject);
begin
  with FMemo do
    begin
      Left:=0;
      Top:=0;
      Width:=self.ClientWidth;
      Height:=self.ClientHeight - Top;
    end;
end;






//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//##############################
//########## MAINMENU ##########
//##############################


procedure TForm_Debug.MM_CloseClick(Sender: TObject);
begin
  close;
end;

//------------------------------------------------------------------------------

procedure TForm_Debug.MM_ClearClick(Sender: TObject);
begin
  IF length(Memo.Entries)=0 THEN exit;
  IF (FConfirmBeforeClear=FALSE) OR (MessageDlg('Alle Eintr�ge im DebugMemo unwiderruflich l�schen?',mtConfirmation,[mbOK,mbAbort],0)=mrOK)
    THEN begin
      FMemo.Clear;
    end;
end;
//------------------------------------------------------------------------------
procedure TForm_Debug.MM_AddEntryClick(Sender: TObject);
var S: String;
begin
  S:=InputBox('Hinzuf�gen','Neuer Eintrag:','');
  IF S<>'' THEN Memo.Send(S);
end;

//------------------------------------------------------------------------------

procedure TForm_Debug.MM_DrawsClick(Sender: TObject);
begin
  IF Sender=MM_Draws
    THEN Board.DebugSendDraw:=MM_Draws.Checked
    ELSE IF Board<>nil THEN MM_Draws.Checked:=Board.DebugSendDraw;
end;
//------------------------------------------------------------------------------
procedure TForm_Debug.MM_PlayerNamesClick(Sender: TObject);
begin
  IF Sender=MM_PlayerNames
    THEN DebugPlayername:=MM_PlayerNames.Checked
    ELSE MM_PlayerNames.Checked:=DebugPlayername;
end;
//------------------------------------------------------------------------------
procedure TForm_Debug.MM_TimeClick(Sender: TObject);
begin
 IF Sender=MM_Time
   THEN Memo.AddTime:=MM_Time.Checked
   ELSE MM_Time.Checked:=Memo.AddTime;
end;
//------------------------------------------------------------------------------
procedure TForm_Debug.MM_DateClick(Sender: TObject);
begin
 IF Sender=MM_Date
   THEN Memo.AddDate:=MM_Date.Checked
   ELSE MM_Date.Checked:=Memo.AddDate;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//##############################
//########### EINGABE ##########
//##############################


procedure TForm_Debug.MemoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
;
end;



//##############################
//###### LADEN/SPEICHERN #######
//##############################

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_Debug.MM_SaveClick(Sender: TObject);
begin
  SaveDialog.Execute;
end;
//------------------------------------------------------------------------------
procedure TForm_Debug.MM_LoadClick(Sender: TObject);
begin
  OpenDialog.Execute;
end;
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

function CheckFileEnding(var Path: String; const FileType: String; const Change: Boolean): Boolean;
      //�berpr�ft die Dateiendung, ob sie FileType entspricht, wenn nicht, wird gegebenenfalls (Change)
      //die Endung ge�ndert.   Beispiel f�r FileTye: '.pap'
var CurrentEnding: ShortString;
    tempChar: Char;
    i:integer;
begin
  CurrentEnding:='';
  i:=length(path)+1;
  REPEAT
    dec(i);
    tempChar:=Path[i];
    CurrentEnding:=tempChar+CurrentEnding;
  until (tempChar='.') or (i=0);

  //Punkte und Leerstellen am Ende l�schen
  IF (Change=TRUE) and (i>1)
    THEN begin
      While (i>1) and ((Path[i-1]='.') or (Path[i-1]=' ')) do
        begin
          Delete(Path,i-1,1);
          dec(i);
        end;
    end;

  IF CurrentEnding=FileType
    THEN result:=TRUE
    ELSE begin
      result:=FALSE;
      IF (Change=TRUE)
        THEN begin
          IF i>length(FileType) THEN Delete(Path,i,length(path)-i+1);
          Path:=Path+FileType;
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Debug.DialogCanClose(Sender: TObject;
  var CanClose: Boolean);
var Path:String;
begin
  IF Sender=OpenDialog
    THEN begin
      IF FileExists(OpenDialog.FileName)=FALSE
        THEN ShowMessage('Angegebene Datei existiert nicht')
        ELSE begin
          IF (length(Memo.Entries)=0) or (MessageDlg('Alle jetzigen Eintr�ge gehen verloren. Fortfahren?',mtConfirmation,[mbOK,mbCancel],0)=mrOK)
            THEN Memo.LoadFromFile(OpenDialog.FileName);
        end;
    end;
  IF Sender=SaveDialog
    THEN begin
      //Pfad anpassen
      Path:=SaveDialog.FileName;
      CheckFileEnding(Path,'.txt',true);
      SaveDialog.FileName:=Path;
      IF (FileExists(Path)=FALSE) OR ((FileExists(Path)=TRUE) AND (MessageDlg('Angegebene Datei existiert bereits. �berschreiben?',mtConfirmation,[mbOK,mbCancel],0)=mrOK))
        THEN begin
          Memo.SaveToFile(Path);
        end;
    end;
end;


















end.
