unit Unit_Server_Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, math, Unit_TGameBoard, ExtCtrls, Menus, StdCtrls, Spin, Grids, Unit_CustomProc;

type
  TForm_Server = class(TForm)
    SG_GameBoard: TStringGrid;
    La_PinguinNummer: TLabel;
    La_Path: TLabel;
    La_Ausgang: TLabel;
    La_Ziel: TLabel;
    La_CheckPath: TLabel;
    La_Turn: TLabel;
    La_Player: TLabel;
    MainMenu: TMainMenu;
    MM_Datei: TMenuItem;
    MM_Close: TMenuItem;
    MM_Game: TMenuItem;
    MM_NewGame: TMenuItem;
    MM_Options: TMenuItem;
    MM_Save: TMenuItem;
    MM_Open: TMenuItem;
    MM_Ansicht: TMenuItem;
    MM_Entwicklung: TMenuItem;
    MM_Info: TMenuItem;
    MM_About: TMenuItem;
    GB_Entwicklung: TGroupBox;
    GB_Spiel: TGroupBox;
    La_Score_0: TLabel;
    La_Score_1: TLabel;
    Btn_TargetsInDirection: TButton;
    SE_Direction: TSpinEdit;
    La_Direction: TLabel;
    Btn_GetAllTargets: TButton;
    La_Movable: TLabel;
    La_Position: TLabel;
    OpenDialog_Game: TOpenDialog;
    SaveDialog_Game: TSaveDialog;
    Btn_Encode: TButton;
    GB_GameShow: TGroupBox;
    Btn_Next: TButton;
    Btn_Previous: TButton;
    Btn_First: TButton;
    Btn_Last: TButton;
    Btn_Auto: TButton;
    Timer_GameShow: TTimer;
    Btn_Renumber: TButton;
    MM_Undo: TMenuItem;
    MM_Hex: TMenuItem;
    Btn_ImgRefresh: TButton;
    GB_Client: TGroupBox;
    Btn_ClientTurn: TButton;
    Btn_ClientReset: TButton;
    CB_ClientAuto: TCheckBox;
    MM_Help: TMenuItem;
    Btn_ClientTest: TButton;
    MM_GetIntoGame: TMenuItem;
    La_Fish: TLabel;
    MM_Testreihe: TMenuItem;
    MM_LastGameStats: TMenuItem;
    MM_Debug: TMenuItem;
    Btn_Send: TButton;
    MM_TimeStats: TMenuItem;
    MM_Transparency: TMenuItem;
    MM_Calculation: TMenuItem;
    MM_CalcResult: TMenuItem;
    Btn_BoardRestart: TButton;
    MM_ContinueLastGame: TMenuItem;
    Btn_Faktoren: TButton;
    La_ScoreName1: TLabel;
    La_ScoreName0: TLabel;
    procedure Btn_FaktorenClick(Sender: TObject);
    procedure MM_ContinueLastGameClick(Sender: TObject);
    procedure Btn_BoardRestartClick(Sender: TObject);
    procedure MM_CalcResultClick(Sender: TObject);
    procedure La_PlayerDblClick(Sender: TObject);
    procedure MM_TransparencyClick(Sender: TObject);
    procedure MM_TimeStatsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MM_DebugClick(Sender: TObject);
    procedure MM_LastGameStatsClick(Sender: TObject);
    procedure MM_TestreiheClick(Sender: TObject);
    procedure MM_GetIntoGameClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SG_GameBoardSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Btn_ClientTestClick(Sender: TObject);
    procedure MM_HelpClick(Sender: TObject);
    procedure MM_HexClick(Sender: TObject);
    procedure Btn_AutoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Btn_ImgRefreshClick(Sender: TObject);
    procedure MM_AboutClick(Sender: TObject);
    procedure MM_UndoClick(Sender: TObject);
    procedure Btn_RenumberClick(Sender: TObject);
    procedure Timer_GameShowTimer(Sender: TObject);
    procedure Btn_EncodeClick(Sender: TObject);
    procedure MM_OpenClick(Sender: TObject);
    procedure MM_SaveClick(Sender: TObject);
    procedure SaveDialog_GameCanClose(Sender: TObject; var CanClose: Boolean);
    procedure OpenDialog_GameCanClose(Sender: TObject; var CanClose: Boolean);
    procedure MM_OptionsClick(Sender: TObject);
    procedure Btn_GetAllTargetsClick(Sender: TObject);
    procedure Btn_TargetsInDirectionClick(Sender: TObject);
    procedure MM_EntwicklungClick(Sender: TObject);
    procedure MM_CloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NewGame(Sender: TObject);
    procedure GameShow(Sender: TObject); //Buttons zur GameShow
    procedure ClientButtons(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure ClientTimeOut(Sender: TObject; Client: Unit_TGameBoard.TBit; Time: Integer);
  public
    { Public-Deklarationen }
    Started:Boolean; //Gibt an, ob die Form zum ersten Mal schon gezeigt wurde
    procedure LabelRefreshTurn; //Zum Aktualisieren der Labels jede Runde
    procedure LabelRefreshMouseMove; //Zum Aktualisieren der Labels bei Mausbewegung
  end;

var
  Form_Server: TForm_Server;
  Board: TGameBoard;
  AutoShow: Boolean; //L�uft die GameShow automatisch weiter?

Const Version: String = '4.3';
      PublicVersion: Boolean = true;

const clMarkPos: TColor = clGreen;
      clMarkAlt: TColor = clWhite; //Alternative Markierung

implementation

uses  Unit_Server_Options, Unit_Server_About, Unit_Server_Testreihe,
      Unit_Server_Finish, DebugForm, Unit_Server_SendClient, Unit_Server_TimeStats,
      Unit_Server_Help, Unit_CalcThd, Unit_CalcResult, Unit_Faktoren;

{$R *.dfm}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//#######################################
//##########  VISUALISIERUNG  ###########
//#######################################

procedure TForm_Server.LabelRefreshMouseMove;
var Path: TPointArray;
    PathCaption: String;
    i,Player, PinguinNummer: byte;
    Pinguin: TPinguin;
begin
  IF (Board.QuadVisible=TRUE) AND (Board.HexVisible)
    THEN begin
      Path:= Board.GetPath(Board.Ausgang.X,Board.Ausgang.Y,Board.Ziel.X,Board.Ziel.Y);
      PathCaption:='Pfad:';

      IF (length(Path)<>0) AND (Path[0].X<>0) AND (Path[0].Y<>0)
        THEN FOR i:=0 to Length(Path)-1 do PathCaption:=PathCaption
                                  +' ('+IntToStr(Path[i].X)+'|'+IntToStr(Path[i].Y)+')';
      IF La_Path.Caption<>PathCaption THEN La_Path.Caption:=PathCaption;
      Form_Server.La_Ausgang.Caption:='Ausgang: ('+IntToStr(Board.Ausgang.X)+'|'+IntToStr(Board.Ausgang.Y)+')';
      Form_Server.La_Ziel.Caption:='Ziel: ('+IntToStr(Board.Ziel.X)+'|'+IntToStr(Board.Ziel.Y)+')';

      IF (Board.CheckPath(Board.Ausgang.X,Board.Ausgang.Y,Board.Ziel.X,Board.Ziel.Y)=TRUE)
        THEN Form_Server.La_CheckPath.Caption:='Pfad: TRUE'
        ELSE Form_Server.La_CheckPath.Caption:='Pfad: FALSE';

      //Infos zu ausgew�hltem Pinguin
      IF Board.Field[Board.Ausgang.X,Board.Ausgang.Y].Full = TRUE
        THEN begin
          Player:=Board.Field[Board.Ausgang.X,Board.Ausgang.Y].Player;
          PinguinNummer:=Board.Field[Board.Ausgang.X,Board.Ausgang.Y].PinguinNummer;
          Pinguin:=Board.Player[Player].Pinguin[PinguinNummer];
          La_Position.Caption:='Position: ('+IntToStr(Pinguin.Position.X)+'|'+IntToStr(Pinguin.Position.Y)+')';
          La_PinguinNUmmer.Caption:='Nummer: '+IntToStr(PinguinNummer);
          IF Pinguin.movable=TRUE
            THEN La_Movable.Caption:='Movable: TRUE'
            ELSE La_Movable.Caption:='Movable: FALSE';
          La_Fish.Caption:='Fisch: '+IntToStr(Pinguin.Fish);
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.LabelRefreshTurn;
const ScoreLabelSpace = 3;
var ScoreLeft: Integer;
begin
  //Spiel
  La_Turn.Caption:='Runde: '+IntToStr(Board.Turn);
  La_Player.Caption:=Board.Player[Board.CurrentPlayer].Name;
  La_Player.Font.Color:=Board.clPlayer;

  La_ScoreName0.Caption:=Board.Player[0].Name + ':';
  La_ScoreName1.Caption:=Board.Player[1].Name + ':';
  La_Score_0.Hint:='Schollen: '+IntToStr(Board.Player[0].FieldsCollected);
  La_Score_1.Hint:='Schollen: '+IntToStr(Board.Player[1].FieldsCollected);
  La_Score_0.Caption:=IntToStr(Board.Player[0].Score);
  La_Score_1.Caption:=IntToStr(Board.Player[1].Score);
  if La_ScoreName0.Width > La_ScoreName1.Width then
    ScoreLeft := La_ScoreName0.Left + La_ScoreName0.Width + ScoreLabelSpace
  else
    ScoreLeft := La_ScoreName1.Left + La_ScoreName1.Width + ScoreLabelSpace;
  La_Score_0.Left := ScoreLeft;
  La_Score_1.Left := ScoreLeft;

  //Client
  IF (Board.GameShow=FALSE)
    THEN begin
      GB_Client.Visible:=Board.Player[Board.Currentplayer].CPUControlled;
      IF (Board.Turn>1) AND (Board.Player[Board.CurrentPlayer].AutoMove=TRUE)
        THEN Btn_ClientTurn.Enabled:=FALSE
        ELSE Btn_ClientTurn.Enabled:=TRUE;
      CB_ClientAuto.Checked:=Board.Player[Board.CurrentPlayer].AutoMove;
      IF Board.Player[Board.currentplayer].ClientWait=TRUE
        THEN CB_ClientAuto.Hint:='Verz�gerung: '+IntToStr(Board.Player[Board.CurrentPlayer].WaitSleep)+' ms'
        ELSE CB_ClientAuto.Hint:='Verz�gerung: - ';
      Btn_ClientTurn.ShowHint:=Board.QuadVisible;
      Btn_ClientReset.ShowHint:=Board.QuadVisible;
    end
    ELSE GB_Client.Visible:=FALSE;   

  //MainMenu
  IF (Board.Turn>1)
    THEN MM_Undo.Enabled:=TRUE
    ELSE MM_Undo.Enabled:=FALSE;
  IF (Board.GameShow=TRUE)
    THEN begin
      MM_Undo.Enabled:=FALSE;
      MM_Save.Enabled:=FALSE;
    end
    ELSE MM_Save.Enabled:=TRUE;
  MM_GetIntoGame.Visible:=Board.GameShow;
  MM_LastGameStats.Visible:=Form_Finish.FirstShown;

  //Form_TimeStats
  IF Form_TimeStats.CB_Refresh.Checked=TRUE
    THEN Form_TimeStats.RefreshListBox(self);

  //Form anpassen
  if (GB_Client.Visible = false) and (GB_GameShow.Visible = false) then
    self.ClientHeight := GB_Spiel.Top + GB_Spiel.Height + GB_Spiel.Left;
  if (GB_Client.Visible = true) then
    self.ClientHeight := GB_Client.Top + GB_Client.Height + GB_Client.Left;
  if (GB_GameShow.Visible = true) then
    self.ClientHeight := GB_GameShow.Top + GB_GameShow.Height + GB_GameShow.Left;
end;



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//#######################################
//##############  EINGABE  ##############
//#######################################

procedure TForm_Server.La_PlayerDblClick(Sender: TObject);
begin
  self.Enabled:=FALSE;
  Form_Options.Show;
  IF Board.Currentplayer=0
    THEN begin
      Form_Options.Ed_Name0.SetFocus;
      Form_Options.Ed_Name0.SelectAll;
    end
    ELSE begin
      Form_Options.Ed_Name1.SetFocus;
      Form_Options.Ed_Name1.SelectAll;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_TargetsInDirectionClick(Sender: TObject);
var i: integer;
    p: TPoint;
    PArray: TPointArray;
begin
  Board.RefreshPaintBox;
  p:=Board.Ausgang;
  PArray:=Board.Player[Board.Field[p.x,p.y].Player].Pinguin[Board.Field[p.x,p.y].PinguinNummer].GetTargetsInDirection(SE_Direction.Value);
  FOR i:=0 to length(PArray)-1 do
    Board.MarkField(PArray[i].X,PArray[i].Y,clMarkAlt);
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_GetAllTargetsClick(Sender: TObject);
var p: TPoint;
    PArray: TPointArray;
begin
  Board.RefreshPaintBox;
  p:=Board.Ausgang;
  PArray:=Board.Player[Board.Field[p.x,p.y].Player].Pinguin[Board.Field[p.x,p.y].PinguinNummer].GetAllTargets;
  Board.MarkTPointArray(PArray,clMarkAlt);
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_EncodeClick(Sender: TObject);
begin
  SendDebug('BrettspielCode: '+Board.EncodeBoard(0));
  IF Form_Debug.Visible=FALSE THEN ShowMessage('Spielfeldcode in dem Debugfenster ausgegeben.');
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_RenumberClick(Sender: TObject);
begin
  Board.RenumberPinguins;
  IF Board.QuadVisible=TRUE THEN Board.DrawQuad;
  IF Board.HexVisible=TRUE THEN Board.DrawHex;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_ImgRefreshClick(Sender: TObject);
begin
  Board.RefreshPaintBox;
  Board.DrawHex;
  Board.DrawQuad;
  LabelRefreshTurn;
  LabelRefreshMouseMove;
  //Formgr��e anpassen
  IF Board.QuadVisible=TRUE
    THEN begin
      SG_GameBoard.Left:=2*Board.Left + Board.Width;
      GB_Entwicklung.Left:=SG_GameBoard.Left;
      ClientWidth:=SG_GameBoard.Left + SG_GameBoard.Width + Board.Left;
    end
    ELSE begin
      ClientWidth:=2*Board.Left + Board.Width;
    end;
  ClientHeight:=GB_Spiel.Top+GB_Spiel.Height+10;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_BoardRestartClick(Sender: TObject);
var GameSettings: TGameSettings;
    TestSettings: TTestSettings;
    TimeStats: TClientTimeStatsArray;
    SavedTurns: TSavedTurnArr;
begin
  //Einstellungen zwischenspeichern
  GameSettings:=Board.Settings;
  TestSettings:=Board.TestSettings;
  TimeStats:=Board.ClientTimeStats;
  SavedTurns:=Board.SavedTurn;
  //Spielfeld neustarten
  Board.Free;
  Board:=TGameBoard.Create(self,SG_GameBoard);
  //Einstellungen laden
  Board.Settings:=GameSettings;
  Board.TestSettings:=TestSettings;
  Board.ClientTimeStats:=TimeStats;
  //Ereignisse zuweisen
  Board.OnNewTurn:=LabelRefreshTurn;
  Board.OnMouseMove:=LabelRefreshMouseMove;
  Board.OnClientTimeOut:=ClientTimeOut;
  //In das Spiel zur�ckkehren
  Board.ContinueGame(SavedTurns);

  IF Sender<>Calculation
    THEN ShowMessage('Spielfeld erfolgreich neugestartet');
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_FaktorenClick(Sender: TObject);
begin
  if Form_Faktoren.Visible = false then
    Form_Faktoren.Show;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_ClientTestClick(Sender: TObject);
begin
//  IF Form_ClientTest.Visible=FALSE
//    THEN Form_ClientTest.Show
//    ELSE Form_ClientTest.Close;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.SG_GameBoardSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
//  IF Form_Clienttest.CB_StringGrid.Checked=TRUE
//    THEN begin
//      Form_Clienttest.SE_Position_X1.Value:=ACol-1;
//      Form_Clienttest.SE_Position_Y1.Value:=ARow-1;
//    end;
//  Form_Clienttest.ChangeStringGrid(Sender);
  Board.RefreshPaintBox;
  IF Board.Field[ACol-1,ARow-1].OnGameBoard=TRUE
    THEN Board.MarkField(ACol-1,ARow-1,clMarkPos);
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//#######################################
//############  SONSTIGES  ##############
//#######################################

procedure TForm_Server.NewGame(Sender: TObject);
begin
  Timer_GameShow.Enabled:=FALSE;
  GB_GameShow.Visible:=FALSE;
  Btn_ClientTurn.Enabled:=TRUE;
  AutoShow:=FALSE;
  Board.New;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.FormCreate(Sender: TObject);
begin
  DoubleBuffered:=TRUE;
  ClientHeight:=GB_Spiel.Top+GB_Spiel.Height+10;
  Caption:=Caption+' - '+Version;
  Started:=FALSE;
  IF PublicVersion=TRUE
    THEN begin
      Btn_ClientTest.Visible:=FALSE;
      Btn_Faktoren.Visible := false;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.FormShow(Sender: TObject);
begin
  IF Started=FALSE //Wird nur beim ersten Anzeigen ausgef�hrt
    THEN begin
      IF Calculation=nil
        THEN Calculation:=TCalcGame.Create;
      Board:=TGameBoard.Create(self,SG_GameBoard);
      Board.OnNewTurn:=LabelRefreshTurn;
      Board.OnMouseMove:=LabelRefreshMouseMove;
      Board.OnClientTimeOut:=ClientTimeOut;
      NewGame(self);
      Started:=TRUE;
      //Letztes Spiel laden
      IF (Board.AutoContinue=TRUE) AND (FileExists(Board.AutoSavePath))
        THEN begin
          Board.LoadGameFromFile(Board.AutoSavePath);
          Board.ContinueGame(Board.SavedTurn);
        end;
    end;
  MM_DebugClick(self);
  MM_EntwicklungClick(self);
  MM_HexClick(self);
  MM_TransparencyClick(self);
  MM_TimeStatsClick(self);
  Form_Debug.FormShow(self);
end;


//------------------------------------------------------------------------------

procedure TForm_Server.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //Clients beenden
  Board.Player[0].SendToClient(2);
  Board.Player[1].SendToClient(2);
  //Einstellungen speichern
  Board.SaveGameSettingsInFile(Board.Settings,Board.GameSettingsPath);
  //Zeitstatistik
  Board.SaveTimeStatsInFile(Board.ClientTimeStats,Board.ClientTimeStatsPath);
end;

//------------------------------------------------------------------------------

procedure TForm_Server.ClientTimeOut(Sender: TObject; Client: Unit_TGameBoard.TBit; Time: Integer);
begin
  CB_ClientAuto.Checked:=FALSE;
  Board.Player[0].AutoMove:=FALSE;
  Board.Player[1].AutoMove:=FALSE;

  IF MessageDlg('Zeit�berschreitung bei Client '+IntToStr(Client)+': '+IntToStr(Time)
                +' Centisekunden. Soll der Client neu gestartet werden?',mtError,[mbYes,mbNo],0)=mrYes
    THEN begin
      Board.Player[Client].SendToClient(2);
      Board.Player[Client].StartClient;
      Btn_ClientTurn.Enabled:=TRUE;
    end;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------




//#######################################
//#############  MAINMENU  ##############
//#######################################


//######## Mainmenu - Datei ###########

procedure TForm_Server.MM_ContinueLastGameClick(Sender: TObject);
begin
  IF (FileExists(Board.AutoSavePath)=TRUE)
    THEN begin
      IF (Board.Turn<=1) OR (MessageDlg('Um das letzte Spiel zu laden, muss das aktuelle abgebrochen werden. Fortfahren?',mtConfirmation,[mbOK,mbCancel],0)=mrOK)
        THEN begin
          Board.LoadGameFromFile(Board.AutoSavePath);
          Board.ContinueGame(Board.SavedTurn);
        end;
    end
    ELSE ShowMessage('Es konnte kein automatisch gespeichertes Spiel gefunden werden.');
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_CloseClick(Sender: TObject);
begin
  close;
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_SaveClick(Sender: TObject);
begin
  SaveDialog_Game.Execute;
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_OpenClick(Sender: TObject);
begin
  OpenDialog_Game.Execute;
end;
//------------------------------------------------------------------------------
//######## Mainmenu - Ansicht ###########

procedure TForm_Server.MM_DebugClick(Sender: TObject);
begin
  IF Sender=MM_Debug
    THEN begin
      Form_Debug.Visible:=MM_Debug.Checked;
      self.SetFocus;
    end
    ELSE MM_Debug.Checked:=Form_Debug.Visible;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.MM_LastGameStatsClick(Sender: TObject);
begin
  self.Enabled:=FALSE;
  Form_Finish.Btn_SaveGame.Enabled:=FALSE;
  Form_Finish.Btn_NewGame.Enabled:=FALSE;
  Form_Finish.GameStats:=Board.LastGameStats;
  Form_Finish.Show;
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_EntwicklungClick(Sender: TObject);
var Change: Boolean; //Wurde etwas an QuadVisible ge�ndert?
begin
  Change:=FALSE;
  IF Sender=MM_Entwicklung
    THEN begin
      IF MM_Entwicklung.Checked<>Board.QuadVisible
        THEN Change:=TRUE;
      Board.QuadVisible:=MM_Entwicklung.Checked;
    end
    ELSE begin
      IF MM_Entwicklung.Checked<>Board.QuadVisible
        THEN Change:=TRUE;
      MM_Entwicklung.Checked:=Board.QuadVisible;
    end;
  GB_Entwicklung.Visible:=Board.QuadVisible;
  SG_GameBoard.Visible:=Board.QuadVisible;

  //Formgr��e anpassen
  IF Board.QuadVisible=TRUE
    THEN begin
      IF Change=TRUE  THEN Left:=Left - Width div 2;
      SG_GameBoard.Left:=2*Board.Left + Board.Width;
      GB_Entwicklung.Left:=SG_GameBoard.Left;
      ClientWidth:=SG_GameBoard.Left + SG_GameBoard.Width + Board.Left;
    end
    ELSE begin
      ClientWidth:=2*Board.Left + Board.Width;
      IF Change=TRUE THEN Left:=Left + Width div 2;
    end;

end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_HexClick(Sender: TObject);
begin
  IF Sender=MM_Hex
    THEN Board.HexVisible:=MM_Hex.Checked
    ELSE MM_Hex.Checked:=Board.HexVisible;
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_TransparencyClick(Sender: TObject);
begin
  IF Sender=MM_Transparency
    THEN Board.Hextransparency:=MM_Transparency.Checked
    ELSE MM_Transparency.Checked:=Board.Hextransparency;
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_TimeStatsClick(Sender: TObject);
begin
  IF Sender=MM_TimeStats
    THEN begin
      IF Form_TimeStats.Visible=FALSE
        THEN begin
          IF Board<>nil
            THEN begin
              Form_TimeStats.Board:=Board;
              Form_TimeStats.Show(Board.ClientTimeStats);
            end;
        end
        ELSE Form_TimeStats.Close;
    end
    ELSE begin
      MM_TimeStats.Checked:=Form_TimeStats.Visible;
    end;
end;

//------------------------------------------------------------------------------

//######## Mainmenu - Spiel ###########

procedure TForm_Server.MM_GetIntoGameClick(Sender: TObject);
begin
  Board.GetIntoGame;
  Timer_GameShow.Enabled:=FALSE;
  GB_GameShow.Visible:=FALSE;
  AutoShow:=FALSE;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.MM_UndoClick(Sender: TObject);
begin
  Board.LoadStep(Board.Turn-1);
  IF Board.HexVisible=TRUE THEN Board.DrawHex;
  IF Board.QuadVisible=TRUE THEN Board.DrawQuad;
end;

//------------------------------------------------------------------------------
procedure TForm_Server.MM_OptionsClick(Sender: TObject);
begin
  Form_Options.Show;
  self.Enabled:=FALSE;
end;

//------------------------------------------------------------------------------

//######## Mainmenu - Testreihen ###########

procedure TForm_Server.MM_TestreiheClick(Sender: TObject);
begin
  Form_Testreihe.Show;
  self.Enabled:=FALSE;
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_CalcResultClick(Sender: TObject);
begin
  Form_CalcResult.Show;
end;

//------------------------------------------------------------------------------

//######## Mainmenu - ? ###########

procedure TForm_Server.MM_AboutClick(Sender: TObject);
begin
  Form_About.Show;
  self.Enabled:=FALSE;
end;
//------------------------------------------------------------------------------
procedure TForm_Server.MM_HelpClick(Sender: TObject);
begin
  Form_Help.Show;
  self.Enabled:=FALSE;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//#######################################
//########  LADEN & SPEICHERN  ##########
//#######################################

procedure TForm_Server.OpenDialog_GameCanClose(Sender: TObject; var CanClose: Boolean);
  var Buttoncode: word;
begin
  IF (Board.Turn>1) AND (Board.GameShow=FALSE) //Falls gerade ein Spiel l�uft
    THEN Buttoncode:=MessageDlg('Das Anzeigen eines gespeicherten Spiels bricht das aktuell laufende ab. Fortfahren?', mtConfirmation, [mbYes, mbNo],0);
  IF (Buttoncode=mrYes) OR (Board.Turn<=1) OR (Board.GameShow=TRUE)
    THEN begin
      Board.LoadGameFromFile(OpenDialog_Game.FileName);
      IF Board.GameShow=TRUE
        THEN begin
          GB_GameShow.Visible:=TRUE;
          AutoShow:=FALSE;
          GameShow(self);
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.SaveDialog_GameCanClose(Sender: TObject;
  var CanClose: Boolean);
var Path: String;
    buttoncode: Word;
begin
  Path:=SaveDialog_Game.FileName;
  CheckFileEnding(Path,'.pap',true);
  IF FileExists(Path)=TRUE
    THEN begin
      buttoncode:=MessageDlg('Datei '+Path+' �berschreiben?',mtConfirmation,[mbYES,mbNO],0);
      IF Buttoncode=mrNo THEN exit;
    end;
  Board.SaveGameToFile(Path);
  ShowMessage('Spiel erfolgreich gespeichert.');
end;




//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//#######################################
//#############  CLIENT  ################
//#######################################


procedure TForm_Server.ClientButtons(Sender:TObject);
begin
  IF (Board.GameFinished=TRUE)
    THEN begin
      GB_Client.Visible:=FALSE;
      exit;
    end;
  IF Sender=Btn_ClientTurn
    THEN begin
      Btn_ClientTurn.Enabled:=FALSE;
      Board.Player[Board.CurrentPlayer].SendToClient(1);
    end;
  IF Sender=Btn_ClientReset
    THEN Board.Player[Board.CurrentPlayer].SendToClient(3);
  IF Sender=CB_ClientAuto
    THEN begin
      Btn_ClientTurn.Enabled:=TRUE;
      Board.Player[0].AutoMove:=CB_ClientAuto.Checked;
      Board.Player[1].AutoMove:=CB_ClientAuto.Checked;
    end;
  IF Sender=Btn_Send
    THEN begin
      self.Enabled:=FALSE;
      Form_ClientSend.Show;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key=13 THEN ClientButtons(Btn_ClientTurn);
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



//#######################################
//###########  GAMESHOW  ################
//#######################################



procedure TForm_Server.GameShow(Sender: TObject);
begin   
  IF Sender=Btn_Next THEN Board.LoadStep(Board.Turn+1);
  IF Sender=Btn_Previous THEN Board.LoadStep(Board.Turn-1);
  IF Sender=Btn_First THEN Board.LoadStep(1);
  IF Sender=Btn_Last THEN Board.LoadStep(1000);
  IF Sender=Btn_Auto
    THEN begin
        IF AutoShow=TRUE
          THEN begin
            Timer_GameShow.Interval:=Board.Settings.GameShowInterval;
            Timer_GameShow.Enabled:=FALSE;
            AutoShow:=FALSE;
            Btn_Auto.Caption:='play';
          end
          ELSE begin
            Timer_GameShow.Interval:=Board.Settings.GameShowInterval;
            Timer_GameShow.Enabled:=TRUE;
            AutoShow:=TRUE;
            Btn_Auto.Caption:='pause';
          end;
    end;
  IF Board.Turn<=1
    THEN begin
      Btn_Previous.Enabled:=FALSE;
      Btn_First.Enabled:=FALSE;
    end
    ELSE begin
      Btn_Previous.Enabled:=TRUE;
      Btn_First.Enabled:=TRUE;
    end;
  IF Board.Turn>=length(Board.SavedTurn)-1
    THEN begin
      Btn_Next.Enabled:=FALSE;
      Btn_Last.Enabled:=FALSE;
      Btn_Auto.Enabled:=FALSE;
      Btn_Auto.Caption:='play';
      Timer_GameShow.Enabled:=FALSE;
      AutoShow:=FALSE;
    end
    ELSE begin
      Btn_Next.Enabled:=TRUE;
      Btn_Last.Enabled:=TRUE;
      Btn_Auto.Enabled:=TRUE;
    end;
  LabelRefreshTurn;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Timer_GameShowTimer(Sender: TObject);
begin
  IF (Board.GameShow=TRUE ) AND (AutoShow=TRUE)
    THEN begin
      GameShow(Btn_Next);
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Server.Btn_AutoMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Btn_Auto.Hint:='Intervall: '+IntToStr(Unit_Server_Main.Board.GameShowInterval)+' ms';
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



//#######################################
//##############  TEST  #################
//#######################################









end.
